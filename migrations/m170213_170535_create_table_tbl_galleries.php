<?php

use yii\db\Migration;

class m170213_170535_create_table_tbl_galleries extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%tbl_galleries}}', [
            'id' => $this->integer(11)->notNull()->append('AUTO_INCREMENT PRIMARY KEY'),
            'name' => $this->string(255)->notNull(),
            'url_alias' => $this->string(255)->notNull(),
            'cover' => $this->string(255)->comment('Обложка'),
            'vis' => $this->smallInteger(1)->notNull()->defaultValue('1'),
            'priority' => $this->integer(11),
            'updated_at' => $this->dateTime(),
            'created_at' => $this->dateTime(),
        ], $tableOptions);

    }

    public function safeDown()
    {
        echo "m170213_170535_create_table_tbl_galleries cannot be reverted.\n";
        return false;
    }
}
