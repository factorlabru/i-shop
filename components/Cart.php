<?php
namespace app\components;
use Yii;
use yii\base\Component;
use yii\helpers\Json;
use app\modules\cart\models\CartProduct;
use app\modules\catalog\models\Product;
use app\modules\order\models\Order;
use app\modules\order\models\OrderProduct;
class Cart extends Component
{
    public $cookie_name = 'cart_products';


    public function init()
    {
        parent::init();
        //Если, установлена кука, но нет сессии (только запустили браузер, при повторном заходе на сайт).
        if(Yii::$app->request->cookies->has('cart_products') && !Yii::$app->session['cart_products']) {
            $user_id = $this->getCookie();
            $cart_products = CartProduct::find()->where(['user_id'=>$user_id])->one();
            if($cart_products) {
                Yii::$app->session['cart_products'] = Json::decode($cart_products->products);
            }
        }
    }

    /**
     * Добавление товара в корзину.
     *
     * @param $product
     * @param $quantity
     */
    public function add($product, $quantity)
    {
        if(Yii::$app->session['cart_products']) {
            $cart_products = Yii::$app->session['cart_products'];
        }
        $cart_products[$product->id] = [
            'product_id'=>$product->id,
            'quantity'=>$quantity,
            'price'=>$product->price,
        ];
        Yii::$app->session['cart_products'] = $cart_products;
        $this->setCookie();
        $this->saveCartProducts(Yii::$app->session['cart_products']);
    }

    /**
     * Удаление товара из корзины.
     *
     * @param $product - найденный товар (объект).
     */
    public function delete($product)
    {
        if(Yii::$app->session['cart_products']) {
            $cart_products = Yii::$app->session['cart_products'];
        }
        unset($cart_products[$product->id]);
        Yii::$app->session['cart_products'] = $cart_products;
        $this->setCookie();
        $this->saveCartProducts(Yii::$app->session['cart_products']);
    }

    /**
     * Записываем куку, для пользователей которые положили товары в корзину.
     */
    public function setCookie()
    {
        $cookies_set = Yii::$app->response->cookies;
        if(!Yii::$app->request->cookies->has($this->cookie_name)) {
            $cookies_set->add(new \yii\web\Cookie([
                'name' => $this->cookie_name,
                'value' => $this->makeUserId(),
                'expire' => time() + 60 * 60 * 24 * 90,
            ]));
        }
        //setcookie("cart_products", $this->makeUserId(), time() + 60 * 60 * 24, '/');
    }

    /**
     * Получение куки.
     *
     * @return mixed
     */
    public function getCookie()
    {
        $cookies_get = Yii::$app->request->cookies;
        $value = $cookies_get->getValue($this->cookie_name);
        return $value;
    }

    /**
     * Генерация уникалного user_id
     * @return string
     */
    private function makeUserId()
    {
        return md5(Yii::$app->request->userIP.time());
    }

    /**
     * Сохранение добавленных товаров в специальной таблице.
     * Ставится кука и при повторном посещении выбираются товары, которые пользователь добавил в корзину.
     *
     * @param $products
     */
    public function saveCartProducts($products)
    {
        if(Yii::$app->request->cookies->has('cart_products')) {
            $products = Json::encode($products);
            $user_id = $this->getCookie();
            $model = CartProduct::find()->where(['user_id'=>$user_id])->one();
            if(!$model) {
                $model = new CartProduct();
            }
            $model->user_id = $user_id;
            $model->products = $products;
            $model->created_at = date('Y-m-d H:i:s');
            $model->ip = Yii::$app->request->userIP;
            $model->save();
        }
    }

    /**
     * Подсчет количества товаров.
     * @return int
     */
    public function countProducts()
    {
        $quantity = 0;
        if(Yii::$app->session['cart_products']) {
            foreach (Yii::$app->session['cart_products'] as $product) {
                $quantity += $product['quantity'];
            }
        }
        return $quantity;
    }

    /**
     * Подсчет цены всех товаров.
     * @return int
     */
    public function countTotalPrice()
    {
        $price = 0;
        if(Yii::$app->session['cart_products']) {
            foreach (Yii::$app->session['cart_products'] as $product) {
                $price += $product['price'] * $product['quantity'];
            }
        }

        return $price;
    }

    /**
     * Подсчет цены определнного товара.
     *
     * @return int
     */
    public function countProductPrice($product_id)
    {
        $price = 0;
        $cart_products = Yii::$app->session['cart_products'];
        if(isset($cart_products[$product_id])) {
            $price = $cart_products[$product_id]['price'] * $cart_products[$product_id]['quantity'];
        }
        return $price;
    }

    /**
     * Считает кол-во товаров определенного товара.
     *
     * @param $product_id
     * @return int
     */
    public function countProductQuantity($product_id)
    {
        $quantity = 0;
        if(isset(Yii::$app->session['cart_products'][$product_id]['quantity'])) {
            return Yii::$app->session['cart_products'][$product_id]['quantity'];
        }
        return $quantity;
    }

    /**
     * Получение всех товров.
     *
     * @return mixed
     */
    public function getProducts()
    {
        if(Yii::$app->session['cart_products']) {
            $products_arr = [];
            foreach (Yii::$app->session['cart_products'] as $product) {
                $products_arr[] = $product['product_id'];
            }
            $products = Product::find()->where(['in','id', $products_arr])->all();
            return $products;
        }
    }

    /**
     * Проверяет, добавлен ли товар в корзину.
     *
     * @param $product_id
     * @return bool
     */
    public function hasItem($product_id)
    {
        if(isset(Yii::$app->session['cart_products'][$product_id])) {
            return true;
        }
        return false;
    }

    /**
     * Очищает корзину.
     */
    public function clear()
    {
        if (isset(Yii::$app->session['cart_products'])) {
            unset(Yii::$app->session['cart_products']);
        }
        Yii::$app->response->cookies->remove($this->cookie_name);
    }
}