<?php

namespace app\components;

use Yii;
use yii\base\Object;

class Config extends Object
{
    private $params = [];

    public function init()
    {
        $params = Yii::$app->db->createCommand('SELECT name, value FROM tbl_params')->queryAll();

        foreach ($params as $val) {
            $this->params[$val['name']] = $val['value'];

            Yii::$app->params[$val['name']] = $val['value'];
        }
    }

    public function get($name, $default = null)
    {
        return isset($this->params[$name]) ? $this->params[$name] : $default;
    }
}
