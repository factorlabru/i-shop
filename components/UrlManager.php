<?php
namespace app\components;

use yii\web\UrlManager as YiiManager;

class UrlManager extends YiiManager
{
    public function createUrl($params)
    {
        return $this->fixPathSlashes(parent::createUrl($params));
    }

    protected  function fixPathSlashes($url)
    {
        return preg_replace('|\%2F|i', '/', $url);
    }
}