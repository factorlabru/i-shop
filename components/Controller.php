<?php

namespace app\components;

use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller as YiiController;

/**
 * Class Controller
 * @package app\components
 * @property \app\components\View $view
 */

class Controller extends YiiController
{
    /**
     * layout по умолчанию.
     * @var string
     */
    public $layout = '@app/views/layouts/main.php';

    public $current_section;

    /**
     * В этой переменной, хранится id текущей записи (раздел, пост, нововсть и т.д.)
     * Один из вариантов использования admin_bar
     * @int
     */
    public $current_item;

    public function beforeAction($action)
    {
        //Устанавливаем общие мета-теги.
        $this->applyCommonMeta();

        //Выводим html из модуля extended.
        $this->applyCustomHtml();

        return parent::beforeAction($action);
    }


    protected function applyCommonMeta()
    {
        $this->setAllMeta([
            'title' => Yii::$app->params['mainpage_title'],
            'keywords' => Yii::$app->params['meta_keywords'],
            'description' => Yii::$app->params['meta_description'],
        ]);

        if (!empty(Yii::$app->params['google-verification'])) {
            $this->view->registerMetaTag([
                'name' => 'google-site-verification',
                'content' => Yii::$app->params['google-verification']
            ]);
        }

        if (!empty(Yii::$app->params['yandex-verification'])) {
            $this->view->registerMetaTag([
                'name' => 'yandex-verification',
                'content' => Yii::$app->params['yandex-verification']
            ]);
        }
    }

    /**
     * Формирование мета-тегов из модели.
     * @param mixed $model
     */
    public function setMetaByModel($model)
    {
        $trail = '';
        if (!empty(Yii::$app->params['title_trail'])) {
            $trail = ' | ' . Yii::$app->params['title_trail'];
        }
        $name = ArrayHelper::getValue($model, 'name');

        if(!empty($model['h1_tag'])){
            $h1_tag = $model['h1_tag'];
        } else if (!empty($model['meta_title'])) {
            $h1_tag = $model['meta_title'];
        } else {
            $h1_tag = $name;
        }

        $this->setAllMeta([
            'title' => !empty($model['meta_title']) ? $model['meta_title'] : $name . $trail,
            'description' => ArrayHelper::getValue($model, 'meta_description'),
            'keywords' =>  ArrayHelper::getValue($model, 'meta_keywords'),
            'h1_tag' => $h1_tag,
        ]);
    }

    public function setAllMeta(array $meta)
    {
        if (!empty($meta['title'])) {
            $this->view->title = $meta['title'];
        }

/*        if (!empty($meta['h1_tag'])) {
            $this->view->h1_tag = $meta['h1_tag'];
        }*/

        if (!empty($meta['description'])) {
            $this->view->registerMetaTag([
                'name' => 'description',
                'content' => $meta['description']
            ], 'description');
        }

        if (!empty($meta['keywords'])) {
            $this->view->registerMetaTag([
                'name' => 'keywords',
                'content' => $meta['keywords']
            ], 'keywords');
        }
    }

    function fail($code = 404, $message = null)
    {
        if ($code == 404) {
            !is_null($message) || $message = 'Страница не найдена';
            throw new \yii\web\NotFoundHttpException($message);
        }

        throw new \yii\web\HttpException($code, $message);
    }


    /**
     * Принимает либо готовый массив хлебных крошек,
     * либо склеивает несколько таких массивов
     * @param array[]|string[]|string $args
     */
    public function setBreadcrumbs(...$args)
    {
        $last = array_pop($args);
        $breadcrumbs = !empty($args) ? array_merge(...$args) : [];
        $breadcrumbs = array_merge($breadcrumbs, (array)$last);

        $this->view->params['breadcrumbs'] = $breadcrumbs;
    }

    /**
     * Вывод в необходимых местах по сайту свой html-код.
     * Задается в модуле extended.
     */
    protected function applyCustomHtml()
    {
        //todo: сделать кеширование.
        $extended_html = Yii::$app->db->createCommand('
            SELECT position, html 
            FROM tbl_custom_html 
            WHERE vis=1
            ORDER BY priority
        ')->queryAll();

        if ($extended_html) {
            foreach ($extended_html as $item) {
                $this->view->registerHtml($item['html'], $item['position']);
            }
        }
    }
}