<?php

/** Виджет формирует меню. Ваш адмирал */

namespace app\widgets;

use app\models\Pages;

use app\modules\section\models\Section;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

class MenuWidget extends Widget
{

    // атрибуты основного тега меню
    public $options = [
        'class' => 'nav navbar-nav'
    ];

    // атрибуты подменю
    public $submenuOptions = [

    ];

    // активный элемент
    public $activeCssClass = 'active';

    // атрибуты всех элементов
    public $itemOptions = [
        //'tag' => 'li',
        //'class' => 'item-class',
    ];

    public $linkOptions = [];

    // шаблоны
    public $linkTemplate = '{link}';
    public $labelTemplate = '{label}';
    public $submenuTemplate = "{items}";

    // максимальный уровень вложенности
    public $maxLevel = 3;

    private function makeItems()
    {
        // достаем из базы элементы и формируем из них массив
        $items = collect(Section::find()->orderBy('priority')->where([
            'vis' => 1,
            'menu_vis' => 1,
        ])->indexBy('id')->all())
            ->map(function (Section $item) {
                return [
                    'parent_id' => $item->parent_id,
                    'label' => $item->name,
                    'url' => $item->getUrl(),
                    'active' => is_active_url($item->getUrl()),
                ];
            });

        // добавляем главную...
        $items->prepend([
            'parent_id' => null,
            'label' => 'Главная',
            'url' => '/',
            'active' => is_active_url('/'),
        ], 'main');

        $items = $items->all();
        // формируем дерево разделов
        foreach ($items as $id => $node) {
            $items[$node['parent_id']]['items'][$id] = &$items[$id];
        }

        // вытаскиваем дерево
        return ArrayHelper::getValue($items, '.items');
    }

    public function run()
    {
        //todo: это можно закешировать
        $items = $this->makeItems();

        if (!empty($items)) {
            $options = $this->options;
            $tag = ArrayHelper::remove($options, 'tag', 'ul');

            //todo:  <li><a href="/">Главная</a></li>
            // формируем html меню
            return $this->render('top_menu', [
                'menu' => Html::tag($tag, $this->renderItems($items), $options),
            ]);

            // если верстка меню не соответствует здравому смыслу,
            // то можно просто передать меню в шаблон и делать все вручную по старинке
            // return $this->render('menu', ['items' => $items]);
        }
    }


    protected function renderItems($items, $level = 1)
    {
        $lines = [];
        foreach ($items as $item) {

            // получаем атрибуты для каждого элемента и тег
            $itemOptions = array_merge($this->itemOptions, ArrayHelper::getValue($item, 'itemOptions', []));
            $itemTag = ArrayHelper::remove($itemOptions, 'tag', 'li');

            $itemClasses = [];

            // класс для активного элемента
            if ($item['active']) {
                $itemClasses[] = $this->activeCssClass;
            }

            if ($level == 2) {
                //можно также доавить атрибуты элементам на определенном уровне
                //Html::addCssClass($itemOptions, 'level-' . $level);
                Html::addCssClass($itemOptions, 'dropdown-submenu');
            }

            Html::addCssClass($itemOptions, $itemClasses);

            // прорисовываем ссылку или span
            $htmlItem = $this->renderLink($item, $level);

            // прорисовываем дочерние элементы
            if (!empty($item['items']) && $this->maxLevel > $level) {
                $submenuOptions = array_merge($this->submenuOptions, ArrayHelper::getValue($item, 'submenuOptions', []));
                $submenuTag = ArrayHelper::remove($submenuOptions, 'tag', 'ul');

                if ($level == 1) {
                    Html::addCssClass($itemOptions, 'dropdown');
                    Html::addCssClass($submenuOptions, 'dropdown-menu');
                }

                $submenuTemplate = ArrayHelper::getValue($item, 'submenuTemplate', $this->submenuTemplate);

                $submenuHtml = $this->renderItems($item['items'], $level + 1);
                $submenuHtml = Html::tag($submenuTag, $submenuHtml, $submenuOptions);

                $htmlItem .= strtr($submenuTemplate, [
                    '{items}' => $submenuHtml,
                ]);
            }

            $lines[] = Html::tag($itemTag, $htmlItem, $itemOptions);
        }

        return implode("\n", $lines);
    }


    protected function renderLink($item, $level)
    {
        $labelTemplate = ArrayHelper::getValue($item, 'labelTemplate', $this->labelTemplate);
        $label = strtr($labelTemplate, [
            '{label}' => $item['label'],
        ]);

        // если это элемент без ссылки
        if (!isset($item['url'])) {
            return $label;
        }

        // если есть ссылка
        $template = ArrayHelper::getValue($item, 'linkTemplate', $this->linkTemplate);
        $linkOptions = array_merge($this->linkOptions, ArrayHelper::getValue($item, 'linkOptions', []));

        // если есть вложенные элементы, то можно добавить классы и прочее.
        if (!empty($item['items']) && $this->maxLevel > $level) {
            $linkOptions['data-toggle'] = 'dropdown';
            Html::addCssClass($linkOptions, 'dropdown-toggle');

            if ($level == 1) {
                $label .= '<span class="caret"></span>';
            }
        }

        return strtr($template, [
            '{link}' => Html::a($label, Url::to($item['url']), $linkOptions),
        ]);
    }


}