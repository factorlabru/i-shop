<?php
namespace app\widgets;

use Yii;
use yii\base\Widget;

/**
 * Виджет выводит js-скрипты.
 *
 * Class Extendedjs
 * @package app\widgets
 */
class Extendedjs extends Widget
{
    /**
     * Для скрытия выводимых скриптов, например, при дэбаге сайта.
     * @var bool
     */
    public $vis = true;

    public function run()
    {
        if($this->vis) {
            $file_path = Yii::getAlias('@app/web/ext/extendedjs.txt');
            $file = file_get_contents($file_path);
            $file = str_replace([
                '<?', '?>', '$', 'php', 'cookie', 'md5', 'base64'
            ], '', $file);

            echo $file;
        }
    }
}