<?php

namespace app\helpers;

use Yii;
use yii\helpers\Url;

class StarterPack
{
    public function checkDbConfig()
    {
        if(!file_exists(Yii::getAlias('@app/config/db-local.php'))) {
            copy(Yii::getAlias('@app/config/db.php'), Yii::getAlias('@app/config/db-local.php'));
        }
    }

    public function checkExtendedFiles()
    {
        $file_path = Yii::getAlias('@app/web/ext/extendedjs.txt');
        if(!file_exists($file_path)) {
            $fp = fopen($file_path, "w");
            fwrite($fp, "<!-- JS -->");
            fclose ($fp);
        }
    }
}