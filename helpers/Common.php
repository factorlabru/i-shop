<?php

namespace app\helpers;

use Yii;
use yii\base\Model;
use yii\helpers\Url;
use yii\web\Response;
use yii\widgets\ActiveForm;

class Common
{

    /*
     * Склонение существительных
     * @property integer $n - число
     * @property array - формы (документ, документа, документов)
     */
    public static function pluralForm($n, $forms)
    {
        $result = $n % 10 == 1 && $n % 100 != 11 ? $forms[0]:($n % 10 >= 2 && $n % 10 <= 4 && ($n % 100<10 || $n % 100>=20) ? $forms[1] : $forms[2]);
        return $result;
    }

    /*
    * Преобразование кириллицы в транслит
    */
    public static function textToTranslit($str)
    {
        $tr = [
            "А"=>"a","Б"=>"b","В"=>"v","Г"=>"g",
            "Д"=>"d","Е"=>"e","Ж"=>"j","З"=>"z","И"=>"i",
            "Й"=>"y","К"=>"k","Л"=>"l","М"=>"m","Н"=>"n",
            "О"=>"o","П"=>"p","Р"=>"r","С"=>"s","Т"=>"t",
            "У"=>"u","Ф"=>"f","Х"=>"h","Ц"=>"ts","Ч"=>"ch",
            "Ш"=>"sh","Щ"=>"sch","Ъ"=>"","Ы"=>"yi","Ь"=>"",
            "Э"=>"e","Ю"=>"yu","Я"=>"ya","а"=>"a","б"=>"b",
            "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
            "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
            "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
            "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
            "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
            "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya",
            " "=> "_", "."=> "", "/"=> "_", "«"=>"", "»"=>"",
            "("=>"", ")"=>"",
        ];

        if (preg_match('/[^A-Za-z0-9_\-]/', $str)) {
            $str = strtr($str,$tr);
            $str = preg_replace('/[^A-Za-z0-9_\-]/', '', $str);
        }

        return strtolower($str);
    }

    /*
     * Функция приводит дату date(Y-m-d) к виду 21 сентября 2012
     * host-kmv cms legacy
     */
    public static function formatDate($date, $day=true, $month=true, $year=true, $time=false)
    {
        $month = [1 => 'января',
            2 => 'февраля',
            3 => 'марта',
            4 => 'апреля',
            5 => 'мая',
            6 => 'июня',
            7 => 'июля',
            8 => 'августа',
            9 => 'сентября',
            10 => 'октября',
            11 => 'ноября',
            12 => 'декабря',];


        $month_en = [1 => 'January',
            2 => 'February',
            3 => 'March',
            4 => 'April',
            5 => 'May',
            6 => 'June',
            7 => 'July',
            8 => 'August',
            9 => 'September',
            10 => 'October',
            11 => 'November',
            12 => 'December'];


        $new_date = explode('-', $date );

        if(isset($new_date[0]) && isset($new_date[1]) && isset($new_date[2])) {

            if($day) {
                $day = (int)$new_date[2];
            }

            if($month) {
                $month = $month[(int)$new_date[1]];
            }

            if($year) {
                $year = $new_date[0];
            }

            $new_date = $day.' '.$month.' '.$year;

            if($time) {
                $time = explode(' ', $date);
                $new_date .= " ".$time[1];
            }

            return $new_date;
        } else {
            return false;
        }
    }

    /**
     * Приводит дату к виду 15.10.2016
     * @param $date
     * @return false|string
     */
    public static function numericDate($date)
    {
        return date("d.m.Y", strtotime($date));
    }

    /*
     * Форматирование цены.
     * Превращает 15000 в 15 000
     */
    public static function formatPrice($price)
    {
        $price = number_format($price, 0, " ", " ");
        return $price;
    }

    /**
     * Чистит телефон, для вставки в href="tel:"
     * @param $phone
     * @return mixed|string
     */
    public static function cleanPhone($phone)
    {
        $phone = str_replace("(", "", $phone);
        $phone = str_replace(")", "", $phone);
        $phone = str_replace("-", "", $phone);
        $phone = str_replace(" ", "", $phone);
        $phone = str_replace(" ", "", $phone);
        $phone = strip_tags($phone);

        return $phone;
    }

    /**
     * Проверяет, есть ли в строке нужный текст. Используется, напимер в меню.
     *
     * @param $path
     * @param $url_alias
     * @return bool
     */
    public static function isInPath($path, $url_alias)
    {
        $pos = strpos($path, $url_alias);
        if ($pos !== false) {
            return true;
        }

        return false;
    }

    //public static function getPathLink($model, $prefix='', $postfix='')
    public static function getPathLink($url)
    {
        return '<a title="Смотреть на сайте" href="'.$url.'" target="_blank"><i class="fa fa-external-link"></i></a>';
    }

    /**
     * Метод формирует:
     * <link href="..." rel="canonical">
     * <link href="..." rel="prev">
     * <link href="..." rel="next">
     * @param $pages
     */
    public static function seoUniqueRels($pages)
    {
        if($pages->page > 0){
            \Yii::$app->view->registerLinkTag([
                    'rel' => 'canonical',
                    'href' => \yii\helpers\Url::current(['page' => $pages->page+1], true)]
            );
            \Yii::$app->view->registerLinkTag([
                    'rel' => 'prev',
                    'href' => \yii\helpers\Url::current(['page' => $pages->page], true)]
            );
            if(($pages->page + 2) <= $pages->pageCount) {
                \Yii::$app->view->registerLinkTag([
                        'rel' => 'next',
                        'href' => \yii\helpers\Url::current(['page' => $pages->page + 2], true)]
                );
            }
        } else {
            \Yii::$app->view->registerLinkTag([
                'rel' => 'canonical',
                'href' => \yii\helpers\Url::canonical()
            ]);
        }
    }

    public static function endJson($data = [])
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        Yii::$app->response->data = $data;
        Yii::$app->end();
    }

    /**
     * @param Model $model
     * @param null|mixed $attributes
     */
    public static function ajaxValidate($model, $attributes = null)
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            Yii::$app->response->data = ActiveForm::validate($model, $attributes);
            Yii::$app->end();
        }
    }


    public static function cookieSet($name, $value, $expire = 0, $path = '/', $domain = '', $secure = false, $httpOnly = true)
    {
        Yii::$app->response->cookies->add(new \yii\web\Cookie([
            'name' => $name,
            'value' => $value,
            'expire' => $expire,
            'path' => $path,
            'domain' => $domain,
            'secure' => $secure,
            'httpOnly' => $httpOnly,
        ]));
    }

    public static function cookieGet($name, $defaultValue = null)
    {
        return Yii::$app->request->cookies->getValue($name, $defaultValue);
    }

    public static function cookieDel($name)
    {
        Yii::$app->request->cookies->remove($name);
    }


    public static function pageTitle($data)
    {
        return !empty($data['h1_tag']) ? $data['h1_tag'] : $data['name'];
    }
}
?>