CKEDITOR.plugins.addExternal('codemirror', '/ext/ckeditor/plugins/codemirror/');
CKEDITOR.plugins.addExternal('backup', '/ext/ckeditor/plugins/backup/');

// разрешим вкладывать некоторые блочные элементы, в ссылку и span
CKEDITOR.dtd.a.div = 1;
CKEDITOR.dtd.a.p = 1;
CKEDITOR.dtd.a.section = 1;
CKEDITOR.dtd.span.div = 1;

// вернул пока эту строку.
CKEDITOR.dtd.$removeEmpty = {};

CKEDITOR.editorConfig = function( config ) {
    config.language = 'ru';

    // true отключает суровую обработку контента
    config.allowedContent = true;

    // эта регулярка позволяет использовать пустые теги
    config.protectedSource.push(/<[a-z]*[a-z\s\=\"\']*><\/[\s\S][^/]*?>/g);

    config.extraPlugins = 'codemirror,backup';

    // false отключает автоматическое добавление параграфа, как корневого элемента
    config.autoParagraph = false;
};