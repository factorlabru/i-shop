jQuery(document).ready(function() {

    jQuery(document).on('click', '.product-page__quantity .plus-button, .product-page__quantity .minus-button', function(ev) {
        ev.preventDefault;
        var product_id = $(this).data('product_id'),
            quantity = $('input[data-product_id='+product_id+']').val(),
            value = jQuery("input[data-product_id="+product_id+"].cart-quantity__field").val();
        if($(this).hasClass('plus-button')) {
            value++;
            value = isNaN(value) ? 0 : value;

            jQuery("input[data-product_id="+product_id+"].cart-quantity__field").val( value ).change();
        } else {
            value--;

            value = isNaN(value) ? 0 : value;

            if(value > 0) {
                jQuery("input[data-product_id="+product_id+"].cart-quantity__field").val( value ).change();
            }
        }
        return false;
    });

    /**
     * Добавление тоавара в корзину.
     */
    jQuery(document).on('click', '.add-cart_button', function() {
        var product_id = jQuery(this).data('product_id'),
            token = jQuery("#token").val(),
            quantity = jQuery('input[data-product_id='+product_id+']').val();

        $(this).text('Перейти в корзину')
            .attr('href', '/cart/')
            .removeClass('add-cart_button');

        $('.product-page__quantity').remove();

        jQuery.ajax({
            type: 'POST',
            url: "/cart/add/",
            data: {
                product_id: jQuery(this).data('product_id'),
                quantity: quantity,
                _csrf:token
            },
            success: function (data) {
                if(data) {
                    data = jQuery.parseJSON(data);
                    jQuery('.cart-total_quantity').html(data.total_count);
                    jQuery('.cart-total_price').html(data.total_price);
                }
            }
        });
        return false;
    });

    /**
     * Нажатие на плюс в корзине.
     */
    jQuery(document).on('click', '.cart-container .plus-button', function(ev) {
        ev.preventDefault;
        var product_id = $(this).data('product_id'),
            token = jQuery("#token").val(),
            quantity = $('input[data-product_id='+product_id+']').val(),
            value = jQuery("input[data-quantity="+product_id+"]").val();

        value++;
        value = isNaN(value) ? 0 : value;
        if (value <= 599) {
            jQuery("input[data-quantity="+product_id+"]").val( value ).change();
            jQuery.ajax({
                type: 'POST',
                url: "/cart/recount/",
                data: {product_id: product_id, quantity: jQuery('input[data-product_id='+product_id+']').val(), _csrf: token},
                success: function (data) {
                    if (data) {
                        console.log('aaa');

                        data = jQuery.parseJSON(data);
                        jQuery('.cart-total_quantity').html(data.total_count);
                        jQuery('span.cart-price__quantity[data-product_id='+product_id+']').html(data.new_price);
                        jQuery('.cart-total_price').html(data.total_price);
                        jQuery('.cart-page__total').html(data.total_price);
                    }
                }
            });
        }
        return false;
    });

    /**
     * Нажатие на минус в корзине.
     */
    jQuery(document).on('click', '.minus-button', function(ev) {
        ev.preventDefault();

        var product_id = $(this).data('product_id'),
            token = jQuery("#token").val(),
            quantity = jQuery('input[data-product_id='+product_id+']').val(),
            value = jQuery('input[data-product_id='+product_id+']').val();

        value--;
        value = isNaN(value) ? 0 : value;

        if (value >= 1) {
            jQuery("input[data-quantity="+product_id+"]").val( value ).change();
            jQuery.ajax({
                type: 'POST',
                url: "/cart/recount/",
                data: {product_id: product_id, quantity: jQuery('input[data-product_id='+product_id+']').val(), _csrf: token},
                success: function (data) {
                    if (data) {
                        data = jQuery.parseJSON(data);
                        jQuery('.cart-total_quantity').html(data.total_count);
                        jQuery('span.cart-price__quantity[data-product_id='+product_id+']').html(data.new_price);
                        jQuery('.cart-total_price').html(data.total_price);
                        jQuery('.cart-page__total').html(data.total_price);
                    }
                }
            });
        }
        return false;
    });

    /**
     * Удаление товара из корзины.
     */
    jQuery(document).on('click', '.remove-from-cart__link', function(ev) {
        ev.preventDefault;

        var product_id = $(this).data('product_id'),
            token = jQuery("#token").val();
        jQuery('tr[data-product_id='+product_id+']').remove();

        jQuery.ajax({
            type: 'POST',
            url: "/cart/delete/",
            data: {
                product_id: $(this).data('product_id'), _csrf:token
            },
            success: function (data) {
                if(data) {
                    data = jQuery.parseJSON(data);
                    if(!jQuery('.cart-product__item').length) {
                        jQuery('.cart-container').html('<div class="empty-cart__text">Вы еще не добавили товары в корзину.</div>');
                    }
                    jQuery('.cart-products-count').html(data.total_count);
                    jQuery('.cart-total_price').html(data.total_price);
                }
            }
        });
        return false;
    });

    jQuery(document).on('keydown', '.inputNum', function(event) {
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 ||
            (event.keyCode == 65 && event.ctrlKey === true) ||
            (event.keyCode >= 35 && event.keyCode <= 39)) {

            return;
        }
        else {
            if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault();
            }
        }
    });
});