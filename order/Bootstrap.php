<?php
namespace app\modules\order;

use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules(
            [
                '<path:([\w_\/-]+\/)?order>' => 'order/order/index',
                '<path:([\w_\/-]+\/)?order>/<_a:[\w-]+>' => 'order/order/<_a>',
            ]
        );
    }
}