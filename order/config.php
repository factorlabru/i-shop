<?php

namespace app\modules\order;

return [
    'params' => [
        '_bcModule' => 'Заказ',
        'viewed'=>true,
        'model'=>'\app\modules\order\models\Order',
    ],
];