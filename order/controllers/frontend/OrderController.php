<?php
namespace app\modules\order\controllers\frontend;
use app\modules\order\models\Order;
use app\components\Controller;
use Yii;
class OrderController extends Controller
{
    public function actionComplete()
    {
        $this->setMeta(['title'=>'Сообщение отправлено']);

        Yii::$app->cart->clear();

        return $this->render('complete');
    }
}