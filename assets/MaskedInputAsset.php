<?php
namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

class MaskedInputAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '/web';

    public $js = [
        'js/libs/jquery.inputmask.bundle.min',
    ];
    public $jsOptions = [
        'position' => View::POS_END,
    ];
}