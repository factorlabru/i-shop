<?php
namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

class FormHelperAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '/web';

    public $js = [
        'js/libs/ajax.form.helper.js',
    ];
    public $jsOptions = [
        'position' => View::POS_END,
    ];
}