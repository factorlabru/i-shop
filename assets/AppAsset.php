<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/site.css',

        'ext/extended.css', // этот файл должен быть всегда в конце
    ];

    //тут подключаются js
    public $js = [
        'js/libs/csrf.autoadd.js',
        'js/libs/cart.js',
        //'js/code.js',
    ];

    public $depends = [
        'yii\bootstrap\BootstrapPluginAsset',
        //'yii\web\YiiAsset',
        //'yii\web\JqueryAsset',
    ];
}
