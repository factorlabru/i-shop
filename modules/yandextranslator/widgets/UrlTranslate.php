<?php
namespace app\modules\yandextranslator\widgets;

use cebe\markdown\inline\UrlLinkTrait;
use Yii;
use yii\base\Widget;

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

class UrlTranslate extends Widget
{

    public $request_url = ['/admin/yandextranslator/default/ajax-url-translate'];
    public $source_id;
    public $target_id;

    public function run()
    {
        $this->registerJs();

        $translate_icon = Html::tag('i', '', [
            'class' => 'fa fa-language',
        ]);
        $arrow_icon = Html::tag('i', '', [
            'class' => 'fa fa-long-arrow-down',
        ]);
        $link = Html::a($translate_icon . ' ' . $arrow_icon, '#', [
            'class' => 'js-translate-url-alias',
            'data-source-id' => $this->source_id,
            'data-target-id' => $this->target_id,
            'data-request-url' => Url::to($this->request_url),
            'style' => 'font-size:20px;',
            'title' => 'Перевести Url Alias',
        ]);

        $html = Html::tag('div', $link, [
            'class' => 'control-group',
            'style' => 'width:65%; text-align:center'
        ]);

        return $html;

    }

    private function registerJs()
    {
        $JS = /** @lang JavaScript */
            <<<JS
                !function($){
                    $(function(){
                        var that = $('.js-translate-url-alias');
                        var source_id = that.data('source-id');
                        var target_id = that.data('target-id');
                        that.on('click', function(){
                            $.ajax({
                                url: that.data('request-url'),
                                data: {
                                    text: $('#' + source_id).val()
                                },
                                success: function(data){
                                    if(data.code === 200){
                                        $('#' + target_id).val(data.slug);
                                    }
                                }
                            });
                
                            return false;
                        })
                    });
                }(jQuery);
JS;
            Yii::$app->controller->view->registerJs($JS, View::POS_END);
    }
}
