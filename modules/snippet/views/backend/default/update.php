<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\snippet\models\backend\Snippet */

$this->title = 'Редактирование сниппета';
$this->params['breadcrumbs'][] = ['label' => 'Блоки', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<section class="content">
    <div class="box">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</section>
