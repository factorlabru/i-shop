<?php

namespace app\modules\sidebarblock\models;

use Yii;
use app\modules\section\models\Section;

/**
 * This is the model class for table "tbl_section_sidebar_blocks".
 *
 * @property integer $id
 * @property integer $section_id
 * @property integer $sidebar_block_id
 */
class SectionSidebarBlocks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_section_sidebar_blocks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['section_id', 'sidebar_block_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'section_id' => 'Section ID',
            'sidebar_block_id' => 'Sidebar Block ID',
        ];
    }
}
