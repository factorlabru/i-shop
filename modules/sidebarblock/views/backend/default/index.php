<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\admin\widgets\grid\LinkColumn;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\sidebarblock\models\SidebarBlockSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Боковые блоки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sidebar-block-index content">
    <div class="box">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <div class="button-controls">
            <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Добавить запись', ['create'], ['class' => 'btn btn-success']) ?>
        </div>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'class' => LinkColumn::className(),
                    'attribute' => 'name',
                ],

                ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}{link}'],
            ],
        ]); ?>
    </div>
</div>