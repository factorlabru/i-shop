<?php

namespace app\modules\sitemap;

/**
 * sitemap module definition class
 */
class Module extends \yii\base\Module
{
    public $module_name = 'Карта сайта';
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        \Yii::configure($this, require(__DIR__ . '/config.php'));

        // custom initialization code goes here
    }
}