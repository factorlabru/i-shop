<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\modules\section\models\Section;

$this->title = 'История записей раздела "'.$model->name.'"';
$this->params['breadcrumbs'][] = 'История записей';
?>
<div class="content">
    <div class="box">
        <?php if($model->sectionHistory) {?>
            <table class="table">
                <tr>
                    <th>Название</th>
                    <th>Дата изменения</th>
                    <th></th>
                </tr>
                <?php foreach($model->sectionHistory as $item) {?>
                    <tr>
                        <td><a href="<?=Url::to(['/admin/section/timemachine/restore', 'id'=>$item->id]);?>"><?=$item->name?></a></td>
                        <td><?=$item->updated_at?></td>
                        <td><a href="<?=Url::to(['/admin/section/timemachine/restore', 'id'=>$item->id]);?>">Смотреть</a></td>
                    </tr>
                <?php }?>
            </table>
        <?php } else {
            echo 'Записи не найдены. Call the doc Brown.';
        }?>
    </div>
</div>

