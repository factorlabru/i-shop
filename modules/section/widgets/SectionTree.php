<?php
namespace app\modules\section\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use app\modules\section\models\Section;

class SectionTree extends Widget
{

    public function run()
    {
        $items = [];
        $data = [];
        $sections = Section::find()->roots()->all();

        $id = Yii::$app->request->get('id');

        //кешируем результат получения дерева разделов
        /*$data = Yii::$app->cache->getOrSet('sidebarsections', function () use ($sections) {
            //рекурсивно получаем дерево разделов
            return $result = $this->getTree($sections);
        });*/

        $data = Yii::$app->cache->getOrSet('sidebarsections', function () use ($sections) {
            //рекурсивно получаем дерево разделов
            return $result = $this->getTree($sections);
        }, $duration=3600);

        $items = array_merge($items, $data);

        return $this->render('section_tree', ['items'=>$items]);
    }

    private function getTree($sections)
    {
        $data = [];

        foreach ($sections as $section) {

            $name = '<span class="sections-tree-item tree-item__text" data-id="'.$section->id.'">'.$section->name.'</span>';
            $delete_icon = '<i title="Удалить подраздел" class="glyphicon glyphicon-trash delete-sections-tree__item" data-id="'.$section->id.'"></i>';
            $add_icon = '<i title="Добавить подраздел" class="fa fa-plus-square add-sections-tree__item" data-id="'.$section->id.'"></i>';

            $name .= ' &nbsp;'.$delete_icon;
            $name .= ' &nbsp;&nbsp;'.$add_icon;

            if($section->childrenAll) {
                $data[$section->id] = [
                    'text' => $name,
                    'href' => '/admin/section/'.$section->id.'/update/',
                    'icon'=>'glyphicon glyphicon-folder-close',
                    'selectedIcon'=>'glyphicon glyphicon-folder-open',
                ];
                $data[$section->id]['nodes'] = $this->getTree($section->childrenAll);
            } else {
                $data[$section->id] = [
                    'text' => $name,
                    'href' => '/admin/section/'.$section->id.'/update/',
                    'icon'=>'glyphicon glyphicon-folder-close',
                    'selectedIcon'=>'glyphicon glyphicon-folder-open',
                ];
            }
        }

        return $data;
    }
}