<?php

namespace app\modules\section\models;

use app\behaviors\UploadImageBehavior;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use Yii;
use app\behaviors\AdjacencyListBehavior;
use yii\db\ActiveRecord;
use paulzi\adjacencyList\AdjacencyListQueryTrait;
use app\modules\section\models\SectionQuery;
use app\modules\section\models\SectionsHistory;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\Url;

/**
 * This is the model class for table "tbl_sections".
 *
 * @mixin AdjacencyListBehavior
 * @mixin UploadImageBehavior
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $name
 * @property string $url_alias
 * @property string $content
 * @property string $img
 * @property integer $priority
 * @property integer $vis
 * @property integer $menu_vis
 * @property integer $child_vis
 * @property string $external_link
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $h1_tag
 * @property string $updated_at
 * @property string $created_at
 */
class Section extends \yii\db\ActiveRecord
{
    public $selected_blocks;
    public $url_path;

    public function behaviors()
    {
        return [
            'log' => [
                'class' => 'app\behaviors\LogBehavior',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'slug' => [
                'class' => 'app\behaviors\Slug',
                'in_attribute' => 'name',
                'out_attribute' => 'url_alias',
                'translit' => true
            ],
            'saveRelations' => [
                'class'     => SaveRelationsBehavior::className(),
                'relations' => ['sidebarblocks']
            ],
            'image' => [
                'class' => UploadImageBehavior::className(),
                'attribute' => 'img',
                'scenarios' => ['create', 'update'],
                'extensions' => 'png, jpg, jpeg, gif',
                'path' => '@webroot/content/section',
                'url' => '@web/content/section',
                'generate_new_name'=>true,
                'thumbs' => [
                    'thumb' => [
                        'width' => 335,
                        'height'=>210,
                        'quality' => 90,
                        'mode'=>\Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND
                    ],
                ],
            ],
            [
                'class' => \app\behaviors\AdjacencyListBehavior::className(),
                'sortable'=>['sortAttribute'=>'priority']
            ],
            'TimeMachine' => [
                'class' => 'app\behaviors\TimeMachineBehavior',
                'history_model'=>new \app\modules\section\models\SectionsHistory(),
            ],
            'PriorityBehavior' => [
                'class' => 'app\behaviors\PriorityBehavior',
                'order_attribute'=>'priority',
            ],
        ];
    }

    public static function find()
    {
        return new SectionQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_sections';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'trim'],
            [['parent_id', 'menu_vis', 'priority', 'vis', 'child_vis'], 'integer'],
            [['content'], 'string'],
            [['sidebarblocks'], 'safe'],
            [['updated_at', 'created_at'], 'safe'],
            [['name', 'url_alias', 'external_link', 'meta_title', 'meta_description', 'meta_keywords', 'h1_tag'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Родительский раздел',
            'name' => 'Название',
            'url_alias' => 'Url Alias',
            'content' => 'Контент',
            'img' => 'Картинка',
            'priority' => 'Приоритет',
            'vis' => 'Показывать',
            'menu_vis' => 'Показывать в меню',
            'child_vis' => 'Показывать дочерние разделы',
            'external_link' => 'Внешняя ссылка',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
            'h1_tag' => 'H1 Tag',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Список разделов.
     */
    public static function treeList()
    {
        return self::makeTree(self::find()->roots()->all());
    }

    protected static function makeTree($items, $level = 0)
    {
        if ($level > 20) throw new \LogicException();

        return array_reduce($items, function ($list, $item) use ($level) {
            $list[$item['id']] = str_repeat('¦ -', $level) . $item['name'];
            $sublist = self::makeTree($item->children, $level + 1);
            return array_replace($list, $sublist);
        }, []);
    }

    public function getSection()
    {
        return $this->hasOne(Section::className(), ['id' => 'parent_id']);
    }

    public function getSectionHistory()
    {
        return $this->hasMany(SectionsHistory::className(), ['model_id' => 'id'])->orderBy(['priority'=>SORT_ASC]);
    }


    /**
     * @param $path
     * @return Section|null
     */
    public static function getByPath($path)
    {
        /** @var Section $section */
        $section = (new Section())->findByPath($path);

        return $section;
    }

    public function getSidebarblocks()
    {
        return $this->hasMany(
            \app\modules\sidebarblock\models\SidebarBlock::className(),
            ['id' => 'sidebar_block_id']
        )->viaTable(
            'tbl_section_sidebar_blocks',
            ['section_id' => 'id']
        );
    }

    public function getSelectedBlocksIds()
    {
        return $this->hasMany(\app\modules\sidebarblock\models\SectionSidebarBlocks::className(), ['section_id' => 'id']);
    }

    public function getUrl()
    {
        return Url::toRoute([
            '/section/section/index',
            'path' => $this->getUrlPath()
        ]);
    }
}