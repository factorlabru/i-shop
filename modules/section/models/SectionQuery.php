<?php
namespace app\modules\section\models;
use paulzi\adjacencyList\AdjacencyListQueryTrait;

class SectionQuery extends \yii\db\ActiveQuery
{
    use AdjacencyListQueryTrait;
}