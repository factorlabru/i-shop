<?php

namespace app\modules\section\controllers\backend;

use Yii;
use app\modules\section\models\Section;
use app\modules\section\models\SectionsHistory;
use app\modules\section\models\SectionSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\admin\components\AdminController;

/**
 * DefaultController implements the CRUD actions for Section model.
 */
class TimemachineController extends AdminController
{
    /**
     * Lists all Section models.
     * @return mixed
     */
    public function actionIndex($id)
    {
        //$id = Yii::$app->request->get('section_id');
        $model = Section::findOne($id);
        if(!$model) {
            throw new NotFoundHttpException('Страница не найдена.');
        }

        return $this->render('index', ['model'=>$model]);
    }

    protected function findModel($id)
    {
        if (($model = SectionsHistory::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Страница не найдена.');
        }
    }


    public function actionRestore($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            $section = Section::findOne($model->model_id);
            $section->setAttributes($model->attributes);
            $section->save();

            \Yii::$app->getSession()->setFlash('restore', 'Данные восстановлены');

            return $this->redirect(['/admin/section/default/update', 'id'=>$model->model_id]);
        }

        return $this->render('restore', [
            'model' => $model,
        ]);
    }
}
