<?php

namespace app\modules\section;

/**
 * section module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\section\controllers';

    public $module_name = 'Разделы';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        //\Yii::configure($this, require(__DIR__ . '/config.php'));

        // custom initialization code goes here
    }
}
