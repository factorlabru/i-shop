<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Редактирование региона';
$this->params['breadcrumbs'][] = ['label' => 'Регионы', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="category-update content">
    <div class="box">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>