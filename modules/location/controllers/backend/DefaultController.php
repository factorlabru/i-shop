<?php

namespace app\modules\location\controllers\backend;

use Yii;

use yii\web\Controller;
use app\modules\location\models\Region;
use app\modules\location\models\RegionSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\admin\components\AdminController;

/**
 * Default controller for the `location` module
 */
class DefaultController extends AdminController
{

    /**
     * Lists all Region models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->redirect('/admin/location/region');

        /*$searchModel = new RegionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);*/
    }
}
