<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\logs\models\LogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Логи';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-index content">
    <div class="box">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a('<i class="glyphicon glyphicon-trash"></i> Удалить логи', ['clear'], ['class' => 'btn btn-danger']) ?>
        </p>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'module',
                'model',
                'record_id',
                [
                    'attribute'=>'action',
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'action',
                        ['create'=>'Создание', 'update'=>'Редактирование', 'delete'=>'Удаление'],
                        ['class'=>'form-control','prompt' => 'Все']
                    ),
                    'value'=>function($model) {
                        return !is_array($model->getActions($model->action)) ? $model->getActions($model->action) : '';
                    },
                ],
                //'attribute',
                [
                    'attribute'=>'user_id',
                    'filter' => Html::activeDropDownList(
                        $searchModel,
                        'user_id',
                        yii\helpers\ArrayHelper::map(\app\modules\admin\models\backend\User::find()->all(), 'id', 'username'),
                        ['class'=>'form-control','prompt' => 'Все']
                    ),
                    'value'=>function($model) {
                        return isset($model->user) ? Html::encode($model->user->username) : $model->user->id;
                    },
                ],
                'ip',
                [
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'created_at',
                        'pluginOptions' => ['format' => 'yyyy-mm-dd']
                    ]),
                    'attribute' => 'created_at',
                    //'format' => ['date', 'php:Y-m-d H:i:s'],
                ],

                //['class' => 'yii\grid\ActionColumn', 'template' => '{view}{link}'],
            ],
        ]); ?>
    </div>
</div>
