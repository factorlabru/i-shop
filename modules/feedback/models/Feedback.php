<?php

namespace app\modules\feedback\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "tbl_feedback".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $message
 * @property string $ip
 * @property integer $viewed
 * @property string $updated_at
 * @property string $created_at
 */
class Feedback extends \yii\db\ActiveRecord
{
    public $verifyCode;
    public $current_room;

    public function behaviors()
    {
        return [
            'log' => [
                'class' => 'app\behaviors\LogBehavior',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_feedback';
    }

    public function beforeSave($insert)
    {
        if($this->isNewRecord){
            $this->ip = Yii::$app->request->userIP;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'message'], 'required'],
            [['message'], 'string'],
            [['viewed'], 'integer'],
            ['email', 'email'],
            [['updated_at', 'created_at'], 'safe'],
            [['name', 'email', 'ip'], 'string', 'max' => 255],
            [
                'verifyCode',
                'captcha',
                'captchaAction' => '/feedback/feedback/captcha',
                'skipOnEmpty'=>!Yii::$app->user->isGuest
            ],
            [['phone'], 'string', 'max' => 55],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ФИО',
            'email' => 'Email',
            'phone' => 'Телефон',
            'message' => 'Сообщение',
            'viewed' => 'Запись просмотрена',
            'ip' => 'ip',
            'updated_at' => 'Updated At',
            'created_at' => 'Дата создания',
        ];
    }

    public function sendMail($model, $mail_to, $mail_from='', $subject='Обратная связь с сайта')
    {
        $mails = explode(',', $mail_to);

        $mail_from = $mail_from ? $mail_from : 'noreply@'.$_SERVER['SERVER_NAME'];

        foreach ($mails as $mail) {
            Yii::$app->mailer->compose('@modules/feedback/mails/letter', ['model' => $model])
                ->setFrom($mail_from)
                ->setTo($mail)
                ->setSubject($subject.' '.$_SERVER['SERVER_NAME'])
                ->send();
        }
    }
}