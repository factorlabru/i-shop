<?php
namespace app\modules\feedback\controllers\frontend;

use app\components\Controller;
use app\helpers\Mail;
use app\modules\section\models\Section;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use app\modules\feedback\models\Feedback;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use app\components\NumericCaptcha;

class FeedbackController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'app\components\NumericCaptcha',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


    public function actionIndex($path)
    {
        $section = Section::getByPath($path) or $this->fail();

        $this->current_section = $section->id;

        $model = new Feedback();

        if($model->load(Yii::$app->request->post()) && $model->save()) {

            $mail = (new \app\helpers\Mail())->sendMail($model,
                Yii::$app->params['admin_email'],
                '@modules/feedback/mails/letter',
                'Обратная связь с сайта');

            return $this->redirect(['/feedback/complete']);
        }

        $this->setMetaByModel($section);
        $this->setBreadcrumbs($section->makeBreadcrumbs());

        return $this->render('index', [
            'model'=>$model,
            'section'=>$section,
        ]);
    }

    public function actionComplete()
    {
        $this->setAllMeta(['title'=>'Сообщение отправлено']);

        return $this->render('complete');
    }
}