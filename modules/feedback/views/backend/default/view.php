<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\booking\models\Booking */

$this->title = 'Сообщение обратной связи';
$this->params['breadcrumbs'][] = ['label' => 'Обратная связь', 'url' => ['index']];
$this->params['breadcrumbs'][] = "Сообщение обратной связи";
?>
<div class="booking-view content">

    <b>Дата создания:</b><br />
    <?php echo Html::encode(\app\helpers\Common::formatDate($model->created_at))?><br /><br />

    <b>Имя:</b><br />
    <?php echo Html::encode($model->name)?><br /><br />

    <?php if($model->phone) { ?>
        <b>Телефон:</b><br />
        <?php echo Html::encode($model->phone)?><br /><br />
    <?php }?>

    <b>Адрес электронной почты:</b><br />
    <?php echo Html::encode($model->email)?><br /><br />

    <b>Сообщение:</b><br />
    <?php echo Html::encode($model->message)?><br /><br />

    <b>IP:</b><br />
    <?php echo Html::encode($model->ip)?><br /><br />

</div>
