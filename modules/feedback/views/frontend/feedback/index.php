<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
?>

<div  class="col-md-8">
    <h1><?= $this->h1_tag ?></h1>

    <div class="rowIn">
        <div class="form">
            <?php $form = ActiveForm::begin([
                'id'=>'booking',
                'fieldConfig' => [
                    'template' => "{input}{error}",
                    'options' => [
                        //'tag'=>false
                    ]
                ]
            ]);?>

                <?= $form->errorSummary($model); ?>

                <div class="form-group">
                    <label>ФИО *</label>
                    <?=$form->field($model, 'name')->textInput(['class'=>'form-control', 'placeholder'=>'ФИО']);?>
                </div>

                <div class="form-group">
                    <label>Email *</label>
                    <?=$form->field($model, 'email')->textInput(['class'=>'form-control', 'placeholder'=>'Ваш email']);?>
                </div>

                <div class="form-group">
                    <label>Сообщение *</label>
                    <?=$form->field($model, 'message')->textArea(['class'=>'form-control']);?>
                </div>

                <div class="form-group">
                    <label>Проверочный код *</label>
                    <?=$form->field($model, 'verifyCode')->widget(Captcha::className(), [
                        'captchaAction' => '/feedback/feedback/captcha',
                        'template' => '<div class="row"><div class="col-lg-6">{input}</div><div class="col-lg-3">{image}</div></div>',
                    ]) ?>
                </div>

                <button type="submit" class="btn btn-default">Отправить</button>
            <?php ActiveForm::end()?>
        </div>
    </div>
</div>