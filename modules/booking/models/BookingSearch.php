<?php

namespace app\modules\booking\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\booking\models\Booking;

/**
 * BookingSearch represents the model behind the search form about `app\modules\booking\models\Booking`.
 */
class BookingSearch extends Booking
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'days', 'adult', 'children', 'room_id', 'viewed'], 'integer'],
            [['name', 'phone', 'email', 'begin_date', 'end_date', 'additional_info', 'ip', 'updated_at', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Booking::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['created_at'=>SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'days' => $this->days,
            'begin_date' => $this->begin_date,
            'end_date' => $this->end_date,
            'adult' => $this->adult,
            'children' => $this->children,
            'room_id' => $this->room_id,
            'viewed' => $this->viewed,
            'updated_at' => $this->updated_at,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'additional_info', $this->additional_info])
            ->andFilterWhere(['like', 'ip', $this->ip]);

        return $dataProvider;
    }
}
