<?php

namespace app\modules\booking\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use app\modules\rooms\models\Room;

/**
 * This is the model class for table "tbl_booking".
 *
 * @property integer $id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property integer $days
 * @property string $begin_date
 * @property string $end_date
 * @property integer $adult
 * @property integer $children
 * @property string $additional_info
 * @property integer $room_id
 * @property string $ip
 * @property integer $viewed
 * @property string $updated_at
 * @property string $created_at
 */
class Booking extends \yii\db\ActiveRecord
{
    public $verifyCode;
    public $current_room;

    public function behaviors()
    {
        return [
            'log' => [
                'class' => 'app\behaviors\LogBehavior',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function beforeValidate()
    {
        if($this->isNewRecord){
            $this->ip = Yii::$app->request->userIP;
        }

        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_booking';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phone', 'email', 'begin_date', 'end_date'], 'required'],
            [['days', 'adult', 'children', 'room_id', 'viewed'], 'integer'],
            [['begin_date', 'end_date', 'updated_at', 'created_at'], 'safe'],
            [['additional_info'], 'string'],
            ['email', 'email'],
            [
                'verifyCode',
                'captcha',
                'captchaAction' => '/booking/booking/captcha',
                'skipOnEmpty'=>!Yii::$app->user->isGuest
            ],
            [['name', 'phone', 'email'], 'string', 'max' => 255],
            [['ip'], 'string', 'max' => 55],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ФИО',
            'phone' => 'Телефон',
            'email' => 'Email',
            'days' => 'Количество дней',
            'begin_date' => 'Дата заезда',
            'end_date' => 'Дата выезда',
            'adult' => 'Взрослые',
            'children' => 'Дети',
            'additional_info' => 'Дополнительная информация',
            'viewed' => 'Запись просмотрена',
            'room_id' => 'Номер',
            'ip' => 'Ip',
            'updated_at' => 'Updated At',
            'created_at' => 'Дата создания',
        ];
    }

    public function getRoom()
    {
        return $this->hasOne(Room::className(), ['id' => 'room_id']);
    }

    public function sendMail($model, $mail_to, $mail_from='', $subject='Заявка на бронирование с сайта')
    {
        $mail_from = $mail_from ? $mail_from : 'noreply@'.$_SERVER['SERVER_NAME'];

        Yii::$app->mailer->compose('@modules/booking/mails/letter', ['model' => $model])
            ->setFrom($mail_from)
            ->setTo($mail_to)
            ->setSubject($subject.' '.$_SERVER['SERVER_NAME'])
            ->send();
    }

    /**
     * Привеодит дату в формат для записи в БД.
     * @param $date
     * @param string $delimiter
     * @return bool|string
     */
    public function formatDate($date, $delimiter='.')
    {
        if($date) {
            $pieces = explode($delimiter, $date);

            if(isset($pieces[1]) && $pieces[2]) {
                return $pieces[2]."-".$pieces[1]."-".$pieces[0];
            }
        }

        return false;
    }
}