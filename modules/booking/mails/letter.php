<?php
use yii\helpers\Html;
use app\helpers\Common;
?>

<div style="margin:0;padding:0; ">
    <table style="border-collapse:collapse;" cellpadding="0" cellspacing="0" width="690px" align="center">
        <tbody>
        <tr bgcolor="#bdbbbc">
            <td align="left" valign="middle" style="padding-left:25px;padding-top: 20px;padding-bottom: 20px">
                <span style="font-size:24px;color:#000;"><b>Заявка на бронирование с сайта <?=$_SERVER['SERVER_NAME']?></b></span>
            </td>
        </tr>
        <tr bgcolor="#fffff">
            <td height="30">&nbsp;</td>
        </tr>
        <tr bgcolor="#fffff;">
            <td style="padding-left: 25px;">
                <table>
                    <tr>
                        <td style="padding-bottom: 15px">
                            <b style="text-transform: uppercase">Дата отправки письма:</b>
                        </td>
                        <td>
                            <?=date('d-m-Y H:i:s');?>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-bottom: 15px">
                            <b style="text-transform: uppercase">ФИО:</b>
                        </td>
                        <td>
                            <?=Html::encode($model->name)?>
                        </td>
                    </tr>

                    <?php if($model->phone) {?>
                        <tr>
                            <td style="padding-bottom: 15px">
                                <b style="text-transform: uppercase">Телефон:</b>
                            </td>
                            <td>
                                <?=Html::encode($model->phone)?>
                            </td>
                        </tr>
                    <?php }?>

                    <?php if($model->email) {?>
                        <tr>
                            <td style="padding-bottom: 15px">
                                <b style="text-transform: uppercase">Email:</b>
                            </td>
                            <td>
                                <?=Html::encode($model->email)?>
                            </td>
                        </tr>
                    <?php }?>

                    <?php if($model->begin_date) {?>
                        <tr>
                            <td style="padding-bottom: 15px">
                                <b style="text-transform: uppercase">Дата заезда:</b>
                            </td>
                            <td>
                                <?=Common::numericDate($model->begin_date)?>
                            </td>
                        </tr>
                    <?php }?>


                    <?php if($model->end_date) {?>
                        <tr>
                            <td style="padding-bottom: 15px">
                                <b style="text-transform: uppercase">Дата выезда:</b>
                            </td>
                            <td>
                                <?=Common::numericDate($model->end_date)?>
                            </td>
                        </tr>
                    <?php }?>

                    <?php if($model->children) {?>
                        <tr>
                            <td style="padding-bottom: 15px">
                                <b style="text-transform: uppercase">Дети от 4-х до 12 лет:</b>
                            </td>
                            <td>
                                <?=Html::encode($model->children)?>
                            </td>
                        </tr>
                    <?php }?>


                    <?php if($model->room) {?>
                        <tr>
                            <td style="padding-bottom: 15px">
                                <b style="text-transform: uppercase">Номер:</b>
                            </td>
                            <td>
                                <?=Html::encode($model->room->name)?>
                            </td>
                        </tr>
                    <?php }?>

                    <tr>
                        <td style="padding-bottom: 15px;" colspan="2">
                            <b style="text-transform: uppercase">Дополнительная информация:</b>
                        </td>
                    </tr>

                    <tr>
                        <td style="padding-bottom: 15px;" colspan="2">
                            <?=Html::encode($model->additional_info);?>
                        </td>
                    </tr>

                    <tr>
                        <td style="padding-bottom: 15px">
                            <b style="text-transform: uppercase">IP:</b>
                        </td>
                        <td>
                            <?=Html::encode($model->ip)?>
                        </td>
                    </tr>

                    <tr>
                        <td style="padding-bottom: 15px;" colspan="2">
                            <a href="<?=$_SERVER['SERVER_NAME']?>/admin/booking/"
                               style="background: #393874;
                                        height: 40px;
                                        line-height: 38px;
                                        text-align: center;
                                        padding: 10px 20px;
                                        color: #fff;
                                        text-decoration: none;
                                        border-radius: 3px;
                                    ">
                                В панель управления
                            </a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr bgcolor="#fffff">
            <td height="30">&nbsp;</td>
        </tr>
        <tr>
            <td style="padding-left: 25px; padding-top: 20px; padding-bottom: 5px">
                <span>Письмо отправлено с адреса <?=$_SERVER['SERVER_NAME']?>.</span>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 25px; padding-bottom: 25px;">
                <span>Использованный адрес &mdash; <?=\Yii::$app->params['admin_email']?></span>
            </td>
        </tr>
        <tr>
            <td height="30">&nbsp;</td>
        </tr>
        </tbody>
    </table>
</div>