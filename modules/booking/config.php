<?php

namespace app\modules\booking;

return [
    'params' => [
        'viewed'=>true,
        'model'=>'\app\modules\booking\models\Booking',
    ],
];