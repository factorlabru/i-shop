<?php
namespace app\modules\booking;
use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules(
            [
                '<path:([\w_\/-]+\/)?(booking|reservation|bronirovanie)>' => 'booking/booking/index',
                '<path:([\w_\/-]+\/)?(booking|reservation|bronirovanie)>/<_a:[\w-]+>' => 'booking/booking/<_a>',
            ]
        );
    }
}