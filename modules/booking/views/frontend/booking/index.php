<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\captcha\Captcha;
?>


<div class="col-md-8">
    <?php $form = ActiveForm::begin([
        'id'=>'booking',
        'fieldConfig' => [
            'template' => "{input}{error}",
            'options' => [
                //'tag'=>false
            ]
        ]
    ]);?>

    <h1>Бронирование путевок</h1>

        <?= $form->errorSummary($model); ?>

        <div class="booking-field__container">

            <div class="form-group">
                <label>ФИО *</label>
                <?=$form->field($model, 'name')->textInput(['class'=>'form-control', 'placeholder'=>'ФИО']);?>
            </div>

            <div class="form-group">
                <label>Телефон *</label>
                <?=$form->field($model, 'phone')->textInput(['class'=>'form-control', 'placeholder'=>'+7 XXX XXX-XX-XX']);?>
            </div>

            <div class="form-group">
                <label>Email *</label>
                <?=$form->field($model, 'email')->textInput(['class'=>'form-control', 'placeholder'=>'Ваш email']);?>
            </div>

            <div class="form-group">
                <label>Дата заезда *</label>
                <?=$form->field($model, 'begin_date')->textInput(['class'=>'form-control inputDate datepicker']);?>
            </div>

            <div class="form-group">
                <label>Дата выезда *</label>
                <?=$form->field($model, 'end_date')->textInput(['class'=>'form-control inputDate datepicker']);?>
            </div>

            <div class="form-group">
                <label>Категория номера</label>
                <div>
                    <?= $form->field($model, 'room_id')->dropDownList(ArrayHelper::map($rooms, 'id', 'name'), ['class'=>'form-select rooms-select']); ?>
                </div>
            </div>

            <div class="form-group room-holder">
                <?php $url = Url::toRoute([
                        '/rooms/room/view',
                        'url_alias'=>$model->current_room->url_alias,
                        'path'=>'rooms'
                ]);?>

                <div class="flatInfo">
                    <div class="pic">
                        <a target="_blank" href="<?=$url?>">
                            <img src="<?=$model->current_room->mainImg('thumb');?>">
                        </a>
                    </div>

                    <div class="text">
                        <div class="title">
                            <a target="_blank" href="<?=$url?>"><?=$model->current_room->name?></a>
                        </div>
                        <p></p>
                        <p><?=$model->current_room->description?></p>
                        <p></p>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label>Дети от 4-х до 12 лет </label>
                <?=$form->field($model, 'children')->textInput(['class'=>'form-control inputNum']);?>
            </div>

            <div class="fRow">
                <label>Комментарий</label>
                <?=$form->field($model, 'additional_info')->textArea(['class'=>'form-control']);?>
            </div>

            <div class="form-group">
                <label>Проверочный код *</label>
                <?=$form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'captchaAction' => '/booking/captcha',
                    'template' => '<div class="row"><div class="col-lg-6">{input}</div><div class="col-lg-3">{image}</div></div>',
                ]) ?>
            </div>

            <button type="submit" class="btn btn-default">Отправить</button>
        </div>

    <?php ActiveForm::end()?>
</div>


<?php
$token = Yii::$app->request->getCsrfToken();

$script = <<< JS

    $(document).on('change', '#booking-room_id', function(event) {
         var room_id = $(this).val();
         
        $.ajax({
            type: 'POST',
            url: '/booking/ajax-load-room/',
            data: {room_id:room_id},
            success: function (data) {
                if(data) {
                    data = $.parseJSON(data);
                    if(data.result) {
                        $('.room-holder').html(data.html);
                    }
                }
            }
        });

        return false;
    });
JS;

$this->registerJs($script, yii\web\View::POS_READY);
?>