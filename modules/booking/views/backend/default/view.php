<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\booking\models\Booking */

$this->title = 'Заявка на бронирование';
$this->params['breadcrumbs'][] = ['label' => 'Бронирование', 'url' => ['index']];
$this->params['breadcrumbs'][] = "Заявка на бронирование";
?>
<div class="booking-view content">

    <b>Дата создания:</b><br />
    <?php echo Html::encode(\app\helpers\Common::formatDate($model->created_at))?><br /><br />

    <b>Имя:</b><br />
    <?php echo Html::encode($model->name)?><br /><br />

    <b>Телефон:</b><br />
    <?php echo Html::encode($model->phone)?><br /><br />

    <b>Адрес электронной почты:</b><br />
    <?php echo Html::encode($model->email)?><br /><br />

    <?php if($model->days) {?>
        <b>Количество дней:</b><br />
        <?php echo Html::encode($model->days)?><br /><br />
    <?php }?>

    <b>Дата заезда:</b><br />
    <?php echo Html::encode($model->begin_date)?><br /><br />

    <b>Дата выезда:</b><br />
    <?php echo Html::encode($model->end_date)?><br /><br />

    <?php if($model->adult) {?>
        <b>Количество взрослых:</b><br />
        <?php echo Html::encode($model->adult)?><br /><br />
    <?php }?>

    <?php if($model->children) {?>
        <b>Дети:</b><br />
        <?php echo Html::encode($model->children)?><br /><br />
    <?php }?>

    <?php if($model->room) {?>
        <b>Номер:</b><br />
        <?php echo Html::encode($model->room->name)?><br /><br />
    <?php }?>

    <b>Дополнительная информация:</b><br />
    <?php echo Html::encode($model->additional_info)?><br /><br />

    <b>IP:</b><br />
    <?php echo Html::encode($model->ip)?><br /><br />

</div>
