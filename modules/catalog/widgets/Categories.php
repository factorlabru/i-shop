<?php
namespace app\modules\catalog\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use app\modules\catalog\models\Category;

/**
 * Виджет выводит категории.
 *
 * Class Categories
 * @package app\modules\catalog\widgets
 */
class Categories extends Widget
{
    /**
     * @var string
     */
    public $view = 'categories';

    /**
     * @var int
     *
     * Количество выводимых записей.
     */
    public $limit = 3;

    /**
     * @var string
     *
     * Тип сортировки.
     */
    public $order = 'priority';


    public function run()
    {
        $categories = Category::find()->where(['IS','parent_id', (new \yii\db\Expression('Null'))])->orWhere(['parent_id'=>'0'])->all();

        return $this->render($this->view, ['categories'=>$categories]);
    }

    /**
     * Вариант выборки меню каталога без AR.
     * @return array
     */
    private function getCatalogMenu()
    {
        $query = "SELECT
                       *
                 FROM
                       tbl_categories
                 WHERE
                       vis='1'
                 ORDER BY
                       priority, id";
        $categories = Yii::$app->db->createCommand($query)->queryAll();
        $main_categories = [];
        $children_categories = [];
        foreach($categories as $category) {
            if($category['parent_id']) {
                $children_categories[$category['parent_id']][$category['id']] = ["id"=>$category['id'],
                    "name"=>$category['name'],
                    "parent_id"=>$category['parent_id'],
                    "url_alias"=>$category['url_alias']];
            } else {
                $main_categories[$category['id']] = ["id"=>$category['id'],
                    "name"=>$category['name'],
                    "url_alias"=>$category['url_alias']];
            }
        }
        return ['main_categories'=>$main_categories, 'children_sections'=>$children_categories];
    }
}