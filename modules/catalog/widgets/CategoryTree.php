<?php
namespace app\modules\catalog\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;
use app\modules\catalog\models\Category;

/**
 * Виджет выводит дерево категорий.
 *
 * Class CategoryTree
 * @package app\modules\catalog\widgets
 */
class CategoryTree extends Widget
{

    public function run()
    {
        $items = [];

        $categories = Category::find()->roots()->all();

        $id = Yii::$app->request->get('id');

        //кешируем результат получения дерева категорий
        $data = Yii::$app->cache->getOrSet('sidebarcategories', function () use ($categories) {
            return $result = $this->getTree($categories);
        }, $duration=3600);

        $items = array_merge($items, $data);

        return $this->render('categories_tree', ['items'=>$items]);
    }

    private function getTree($arr)
    {
        $data = [];

        foreach ($arr as $item) {

            $name = '<span class="categories-tree-item tree-item__text" data-id="'.$item->id.'">'.$item->name.'</span>';
            //$delete_icon = '<i class="glyphicon glyphicon-trash delete-categories-tree__item" data-id="'.$item->id.'"></i>';

            $add_icon = '<i title="Добавить подкатегоию" class="fa fa-plus-square add-categories-tree__item" data-id="'.$item->id.'"></i>';
            $show_products_icon = '<i title="Смотреть товары категории" class="fa fa-cart-arrow-down show-products-categories-tree__item" data-id="'.$item->id.'"></i>';

            //$name .= ' '.$delete_icon;
            $name .= ' &nbsp;'.$show_products_icon;
            $name .= ' &nbsp;&nbsp;'.$add_icon;

            if($item->childrenAll) {
                $data[$item->id] = [
                    'text' => $name,
                    'href' => '/admin/section/'.$item->id.'/update/',
                    'icon'=>'glyphicon glyphicon-folder-close',
                    'selectedIcon'=>'glyphicon glyphicon-folder-open',
                ];
                $data[$item->id]['nodes'] = $this->getTree($item->childrenAll);
            } else {
                $data[$item->id] = [
                    'text' => $name,
                    'href' => '/admin/section/'.$item->id.'/update/',
                    'icon'=>'glyphicon glyphicon-folder-close',
                    'selectedIcon'=>'glyphicon glyphicon-folder-open',
                ];
            }
        }

        return $data;
    }
}