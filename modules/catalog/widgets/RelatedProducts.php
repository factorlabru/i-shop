<?php
namespace app\modules\catalog\widgets;

use yii\base\Widget;
use app\modules\catalog\models\Product;

/**
 * Виджет выводит похожие товары.
 *
 * Class RelatedProducts
 * @package app\modules\catalog\widgets
 */
class RelatedProducts extends Widget
{
    /**
     * @var int
     */
    public $product_id;

    /**
     * @var int
     *
     * Категория.
     */
    public $category_id;

    /**
     * @var string
     */
    public $view='related_products';

    /**
     * @var int
     *
     * Количество выводимых записей.
     */
    public $limit = 3;

    /**
     * @var string
     *
     * Тип сортировки.
     */
    public $order = 'RAND()';

    public function run()
    {
        $products = Product::find()
            ->with('category')
            ->where('vis=1 AND category_id=:category_id AND id!=:id',
                ['category_id'=>$this->category_id, 'id'=>$this->product_id, ]
            )
            ->orderBy($this->order)
            ->limit($this->limit)
            ->all();

        return $this->render($this->view, ['products'=>$products]);
    }
}