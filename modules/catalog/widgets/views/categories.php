<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>

<?php
if($categories) {
    function outputTree(array $subtree, $depth) {

        foreach ($subtree as $key => $category) {
            echo str_repeat("-", $depth*2);
            if ($category->childrenAll) {
                echo Html::a($category->name, $category->getUrl()
                    )."<br>";
                outputTree($category->childrenAll, $depth+1);
            } else {
                echo Html::a($category->name, $category->getUrl())."<br>";
            }
        }
    }

    outputTree($categories, 0);
}
?>