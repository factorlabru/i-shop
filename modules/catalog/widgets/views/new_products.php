<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>

<?php if($products) {?>

    <?php foreach($products as $product) {?>
        <div class="col-md-3">
            <?php $url = Url::to([
                'path'=>'catalog',
                '/catalog/catalog/product',
                'category'=>$product->category->getUrlPath(),
                'product_url'=>$product->url_alias,
            ]);?>

            <a href="<?= $url ?>">
                <img src="<?=$product->mainImg();?>" alt="">
            </a>

            <?=Html::a($product->name, $url);?>
        </div>
    <?php }?>

<?php }?>
