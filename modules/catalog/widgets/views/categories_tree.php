<?php
use yii\helpers\Html;
?>

<div style="width: 310px">
    <?php
    use execut\widget\TreeView;


    $onSelect = new \yii\web\JsExpression(<<<JS
function (undefined, item) {
   //location.href = item.href;
    console.log(item);
}

JS
    );

    $script = <<< JS

        $(document).on('click', '.categories-tree-item.tree-item__text', function(event) {
            event.preventDefault;
            
            var id = $(this).data('id');
            location.href = '/admin/catalog/category/'+id+'/update/';
            return false;
         });

        $(document).on('click', '.list-group-item .delete-categories-tree__item', function(event) {
            event.preventDefault;
            
            var id = $(this).data('id'),
                token = $('meta[name=csrf-token]').prop('content');
   
           if (confirm("Удалить категорию?")) {
                $.ajax({
                    type: 'POST',
                    url: "/admin/catalog/category/'+id+'/delete/",
                    data: {id: id,_csrf:token},
                    success: function (data) {
                        console.log('Delete data');
                    }
                });    
           }
      
            return false;
         });
        
        $(document).on('click', '.list-group-item .add-categories-tree__item', function(event) {
            event.preventDefault;
            
            var id = $(this).data('id');
            location.href = '/admin/catalog/category/create/?id='+id;
            return false;
         });
        
         $(document).on('click', '.list-group-item .show-products-categories-tree__item', function(event) {
            event.preventDefault;
            
            var id = $(this).data('id');
            location.href = '/admin/catalog/product/?category_id='+id;
            return false;
         });

JS;

    $this->registerJs($script, yii\web\View::POS_READY);

    echo Html::beginTag('div', ['class'=>'categories-tree__container']);
    echo TreeView::widget([
        'data' => $items,
        'size' => TreeView::SIZE_MIDDLE,
        'header' => 'Каталог',
        'searchOptions' => [
            'inputOptions' => [
                'placeholder' => 'Поиск'
            ],
        ],
        'clientOptions' => [
            //'onNodeSelected' => $onSelect,
            'selectedBackColor' => '#00a65a',
            'borderColor' => '#222d32',
            'backColor'=>'#222d32',
            'onhoverColor'=>'#00a65a',
            'showTags'=>true
        ],
    ]);
    echo Html::endTag('div');
    ?>
</div>

<style>
    .tree-view-wrapper .tree-heading-container {
        color: #fff !important;
    }
    .execut-tree-filter-input .close{
        display: none;
    }
    .row.tree-header input[type=text]{
        width: 100%;
    }
</style>