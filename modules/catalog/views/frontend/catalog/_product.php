<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>

<div class="col-md-3">
    <?php
    /*$url = Url::to([
        'catalog/product',
        'path'=>'catalog',
        'category'=>$product->category->getUrlPath(),
        'product_url'=>$product->url_alias,
    ]);*/
    $url = $product->getUrl();
    ?>
    <div class="clear"></div>
    <a href="<?= $url ?>">
        <img src="<?=$product->mainImg();?>" alt="">
    </a>
    <div class="clear"></div>
    <?=Html::a($product->name, $url);?>
</div>
