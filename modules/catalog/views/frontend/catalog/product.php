<?php
use yii\helpers\Html;
use app\modules\catalog\widgets\RelatedProducts;
?>

<div class="col-md-12">
    <h1><?=$product->name?></h1>
    <div class="col-md-7">
        <?php if($product->mainImg()) {
            echo Html::img($product->mainImg('thumb_big'), ['class' => 'img-thumbnail']);

            $imgs = $product->images;
            foreach($imgs as $item) {
                echo Html::img($item->img(), ['width'=>150, 'class'=>'img-thumbnail']);
            }
        }?>
    </div>

    <div class="col-md-5">
        <?=$product->description?>

        <div class="col-md-12">
            <?=$product->price?> руб.
        </div>


        <?php if(!\Yii::$app->cart->hasItem($product->id)) {?>
            <div class="product-page__quantity">
                <span class="quantity-button quantity-down minus-button" data-product_id="<?=$product->id?>">-</span>

                &nbsp;&nbsp;&nbsp;
                <input type="text"
                       class="form-control m-mini cart-quantity__field inputNum"
                       name="cart_quantity"
                       value="1"
                       data-product_id="<?=$product->id?>"
                       maxlength="6">
                &nbsp;&nbsp;&nbsp;

                <span class="quantity-button quantity-up plus-button" data-product_id="<?=$product->id?>">+</span>
            </div>

            <br>
            <a href="#" class="btn btn-lg btn-success add-cart_button" data-product_id="<?=$product->id?>">
                <i class="glyphicon glyphicon-shopping-cart"></i>
                Добавить в корзину
            </a>
        <?php } else {?>
            <a href="/cart/" class="btn btn-lg btn-success" data-product_id="<?=$product->id?>">
                <i class="glyphicon glyphicon-shopping-cart"></i>
                Перейти в корзину
            </a>
        <?php }?>
    </div>

    <div class="clear"></div>
    <br>
    <div class="col-md-12">
        <?=$product->features?>
    </div>

    <div class="clear"></div>
    <?=RelatedProducts::widget([
        'product_id'=>$product->id,
        'category_id'=>$product->category_id,
    ]);?>
</div>