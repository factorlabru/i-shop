<?php
use app\modules\catalog\widgets\ProductsByParam;
use yii\helpers\Url;

//$this->registerLinkTag(['rel' => 'canonical', 'href' => Url::canonical()]);
?>

<div class="col-md-9">
    <h1><?= $this->h1_tag ?></h1>

    <?php if($products) {
        foreach($products as $product) {
            echo $this->render('_product', ['product'=>$product]);
        }
    } else {
        echo "Товары не найдены.";
    }?>

    <div class="clear"></div>
    <div class="pagination">
        <?php
        echo \app\widgets\LinkPager::widget([
            'pagination' => $pages,
            'nextPageLabel'=>'вперед →',
            'prevPageLabel'=>'← назад'
        ]); ?>
    </div>

</div>


<div class="col-md-3">
    <h2>Категории</h2>

    <?= \app\modules\catalog\widgets\Categories::widget()?>
</div>