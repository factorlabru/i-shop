<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\catalog\models\Product */

$this->title = 'Редактирование товара "'.Html::encode($model->name).'"';
$this->params['breadcrumbs'][] = ['label' => \Yii::$app->getModule(\Yii::$app->controller->module->id)->module_name, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-update content">
    <div class="box">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
