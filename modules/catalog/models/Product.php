<?php

namespace app\modules\catalog\models;

use app\behaviors\UploadManyImageBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;


/**
 * This is the model class for table "{{%products}}".
 *
 * @mixin UploadManyImageBehavior
 *
 * @property integer $id
 * @property string $name
 * @property string $url_alias
 * @property integer $price
 * @property integer $old_price
 * @property string $code
 * @property string $description
 * @property string $features
 * @property integer $vis
 * @property integer $new
 * @property integer $brand_id
 * @property integer $category_id
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $updated_at
 * @property string $created_at
 */
class Product extends \yii\db\ActiveRecord
{
    public $file_loader;
    const PAGE_SIZE = 2;

    public function behaviors()
    {
        return [
            'log' => [
                'class' => 'app\behaviors\LogBehavior',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'slug' => [
                'class' => 'app\behaviors\Slug',
                'in_attribute' => 'name',
                'out_attribute' => 'url_alias',
                'translit' => true
            ],
            [
                'class' => UploadManyImageBehavior::className(),
                'attribute' => 'file_loader',
                'behavior_name' => 'image',
                'relation' => 'images',
            ],

        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%products}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'category_id'], 'required'],
            [['name'], 'trim'],
            [['price', 'old_price', 'new', 'vis', 'special', 'category_id'], 'integer'],
            [['description', 'features'], 'string'],
            [['updated_at', 'created_at'], 'safe'],
            [['name', 'url_alias', 'meta_title', 'meta_description', 'meta_keywords'], 'string', 'max' => 255],
            ['file_loader', 'image', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg, gif', 'maxFiles' => 5],
            [['code'], 'string', 'max' => 25],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'url_alias' => 'Url Alias',
            'price' => 'Цена',
            'old_price' => 'Старая цена',
            'code' => 'Артикул',
            'description' => 'Описание',
            'features' => 'Характеристики',
            'new' => 'Новинка',
            'special' => 'Спецпредложение',
            'vis' => 'Показывать',
            'category_id' => 'Категория',
            'brand_id' => 'Бренд',
            'file_loader' => 'Фото товара',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function getBrand()
    {
        return $this->hasOne(Brand::className(), ['id' => 'brand_id']);
    }

    public function getImages()
    {
        return $this->hasMany(ProductImage::className(), ['product_id' => 'id'])->orderBy(['priority' => SORT_ASC]);
    }

    public function getUrl()
    {
        //$catalog_section = Category::getCatalogSection();

        $url = \yii\helpers\Url::toRoute([
            '/catalog/catalog/product',
            //'path' => $catalog_section->getUrlPath(),
            'path' => 'catalog',
            'category' => $this->category->getUrlPath(),
            'product_url' => $this->url_alias
        ]);

        return $url;
    }
}
