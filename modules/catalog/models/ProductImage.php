<?php

namespace app\modules\catalog\models;

use app\behaviors\UploadImageBehavior;
use Yii;

/**
 * This is the model class for table "{{%product_images}}".
 *
 * @mixin UploadImageBehavior
 *
 * @property integer $id
 * @property string $img
 * @property integer $product_id
 * @property integer $priority
 */
class ProductImage extends \yii\db\ActiveRecord
{

    public function behaviors()
    {
        return [
            'image' => [
                'class' => UploadImageBehavior::className(),
                'attribute' => 'img',
                'scenarios' => ['default'],
                'thumbs' => [
                    'thumb' => [
                        'width' => 150,
                        'height' => 90,
                        'quality' => 90,
                        //'mode'=>\Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND
                    ],
                    'thumb_big' => [
                        'width' => 590,
                        'height' => 410,
                        'quality' => 90,
                        //'mode'=>\Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND
                    ],
                ],
                'generate_new_name' => true,
                'path' => '@webroot/content/catalog',
                'url' => '@web/content/catalog',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_images}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'priority'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'img' => 'Img',
            'product_id' => 'Product ID',
            'priority' => 'Priority',
        ];
    }
}
