<?php
namespace app\modules\catalog\models;
use paulzi\adjacencyList\AdjacencyListQueryTrait;

class CategoryQuery extends \yii\db\ActiveQuery
{
    use AdjacencyListQueryTrait;
}