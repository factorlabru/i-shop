<?php

namespace app\modules\catalog\controllers\backend;

use yii\web\Controller;
use app\modules\admin\controllers\console\BackController;
use app\modules\admin\components\AdminController;

/**
 * Default controller for the `catalog` module
 */
class DefaultController extends AdminController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $this->redirect('/admin/catalog/product');
        return $this->render('index');
    }
}
