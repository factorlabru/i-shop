<?php

namespace app\modules\catalog\controllers\frontend;

use app\helpers\Common;
use app\modules\section\models\Section;
use app\modules\catalog\models\Product;
use app\modules\catalog\models\Category;
use app\components\Controller;
use yii\data\Pagination;

class CatalogController extends Controller
{
    public function actionIndex($path)
    {
        $section = Section::getByPath($path) or $this->fail();
        $this->current_section = $section->id;

        $query = Product::find()->with('category')
            ->where(['vis' => 1]);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(),
            'pageSize' => Product::PAGE_SIZE, 'defaultPageSize' => Product::PAGE_SIZE]);

        $products = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        Common::seoUniqueRels($pages);

        $this->setMetaByModel($section);
        $this->setBreadcrumbs($section->makeBreadcrumbs());

        return $this->render('index', [
            'products' => $products,
            'pages' => $pages,
        ]);
    }

    public function actionCategory($path, $category)
    {
        $section = Section::getByPath($path) or $this->fail();
        $category = Category::getByPath($category) or $this->fail();

        $this->current_section = $section->id;
        $this->current_item = $category->id;

        $ids = $category->getDescendantsIds(null, true);
        array_push($ids, $category->id);

        $query = Product::find()->with('category')
            ->where(['vis' => 1])
            ->andWhere(['in', 'category_id', $ids]);

        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(),
            'pageSize' => Product::PAGE_SIZE, 'defaultPageSize' => Product::PAGE_SIZE]);

        $products = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        Common::seoUniqueRels($pages);

        $this->setMetaByModel($category);

        $this->setBreadcrumbs(
            $section->makeBreadcrumbs($with_last=true),
            $category->makeBreadcrumbs()
        );

        return $this->render('products', [
            'products' => $products,
            'pages' => $pages,
            'category' => $category,
        ]);
    }

    public function actionProduct($path, $category, $product_url)
    {
        $section = Section::getByPath($path) or $this->fail();
        $category = Category::getByPath($category) or $this->fail();

        $product = Product::find()
            ->where(['url_alias' => $product_url, 'category_id' => $category->id, 'vis' => 1])
            ->one() or $this->fail();

        $this->current_section = $section->id;
        $this->current_item = $product->id;

        $this->setBreadcrumbs(
            $section->makeBreadcrumbs($with_last=true),
            $category->makeBreadcrumbs($with_last=true),
            $product->name
        );

        $this->setMetaByModel($product);

        return $this->render('product', [
            'product' => $product,
            'category' => $category,
        ]);
    }
}