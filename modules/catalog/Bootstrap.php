<?php
namespace app\modules\catalog;
use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules(
            [
                [
                    'pattern' => '<path:([\w_\/-]+\/)?catalog>/<category:[\w_\/-]+>/<product_url:[A-Za-z0-9 -_.]+>',
                    'route' => 'catalog/catalog/product',
                    'suffix' => '.html',
                ],
                '<path:([\w_\/-]+\/)?catalog>/page/<page:[0-9]+>'=>'catalog/catalog/index',
                '<path:([\w_\/-]+\/)?catalog>/<category:[\w_\/-]+>/page/<page:[0-9]+>'=>'catalog/catalog/category',
                '<path:([\w_\/-]+\/)?catalog>/<category:[\w_\/-]+>'=>'catalog/catalog/category',
                '<path:([\w_\/-]+\/)?catalog>' => 'catalog/catalog/index',
            ]
        );
    }
}