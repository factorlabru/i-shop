<?php
namespace app\modules\news;
use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules(
            [
                '<path:([\w_\/-]+\/)?(news|novosti)>/page/<page:[0-9]+>'=>'news/news/index',
                [
                    'pattern' => '<path:([\w_\/-]+\/)?(news|novosti)>/<url_alias:[-a-zA-Z0-9_]+>',
                    'route' => 'news/news/view',
                    'suffix' => '.html',
                ],
                '<path:([\w_\/-]+\/)?(news|novosti)>' => 'news/news/index',
            ]
        );
    }
}