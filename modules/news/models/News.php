<?php

namespace app\modules\news\models;

use app\behaviors\UploadImageBehavior;
use app\modules\section\models\Section;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use Yii;

/**
 * This is the model class for table "tbl_news".
 *
 * @mixin UploadImageBehavior
 *
 * @property integer $id
 * @property string $name
 * @property string $url_alias
 * @property string $annotation
 * @property string $content
 * @property string $img
 * @property integer $vis
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $h1_tag
 * @property string $updated_at
 * @property string $created_at
 */
class News extends \yii\db\ActiveRecord
{
    const PAGE_SIZE = 2;
    public static $_news_section;

    public function behaviors()
    {
        return [
            'log' => [
                'class' => 'app\behaviors\LogBehavior',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'slug' => [
                'class' => 'app\behaviors\Slug',
                'in_attribute' => 'name',
                'out_attribute' => 'url_alias',
                'translit' => true
            ],
            'image' => [
                'class' => UploadImageBehavior::className(),
                'attribute' => 'img',
                'scenarios' => ['create', 'update'],
                'extensions' => 'png, jpg, jpeg, gif',
                'path' => '@webroot/content/news',
                'url' => '@web/content/news',
                'generate_new_name'=>true,
                'thumbs' => [
                    'thumb' => [
                        'width' => 335,
                        'height'=>210,
                        'quality' => 90,
                        'mode'=>\Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'trim'],
            [['content', 'annotation'], 'string'],
            [['vis'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            //['img', 'file'],
            [['name', 'url_alias', 'meta_title', 'meta_description', 'meta_keywords', 'h1_tag'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'url_alias' => 'Url Alias',
            'annotation' => 'Аннотация',
            'content' => 'Контент',
            'img' => 'Картинка',
            'vis' => 'Показывать',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
            'h1_tag' => 'H1 Tag',
            'updated_at' => 'Дата обновления',
            'created_at' => 'Дата создания',
        ];
    }

    /**
     * @return mixed|Section
     */
    public static function getNewsSection()
    {
        if(!self::$_news_section){
            self::$_news_section = Section::find()->where([
                'url_alias' => 'news'
            ])->one();
        }

        return self::$_news_section;
    }

    public function getUrl()
    {
        $news_section = self::getNewsSection();

        $url = \yii\helpers\Url::toRoute([
            '/news/news/view',
            'path' => $news_section->getUrlPath(),
            'url_alias'=>$this->url_alias
        ]);

        return $url;
    }
}
