<?php

namespace app\modules\news\controllers\frontend;

use app\modules\news\models\News;
use app\components\Controller;
use yii\data\Pagination;


/**
 * News controller for the `news` module
 */
class NewsController extends Controller
{
    public function actionIndex($path)
    {
        $section = \app\modules\section\models\Section::getByPath($path);

        if(!$section) {
            throw new \yii\web\NotFoundHttpException('Страница не найдена.');
        }

        $this->current_section = $section->id;

        $query = News::find()->where(['vis'=>1])->orderBy('created_at DESC');
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(),
            'pageSize'=>News::PAGE_SIZE, 'defaultPageSize'=>News::PAGE_SIZE]);

        $news = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();


        $this->setMetaByModel($section);
        $this->setBreadcrumbs($section->makeBreadcrumbs());

        return $this->render('index', [
            'news' => $news,
            'pages' => $pages,
            'section' => $section,
        ]);
    }

    public function actionView($path, $url_alias)
    {
        $section = \app\modules\section\models\Section::getByPath($path);

        if(!$section) {
            throw new \yii\web\NotFoundHttpException('Страница не найдена.');
        }

        $model = News::find()->where('url_alias=:url_alias AND vis=1', [':url_alias'=> $url_alias])->one();

        if(!$model) {
            throw new \yii\web\NotFoundHttpException('Страница не найдена.');
        }

        $this->current_section = $section->id;
        $this->current_item = $model->id;

        $this->setMetaByModel($model);
        $this->setBreadcrumbs($section->makeBreadcrumbs(true), $model->name);

        return $this->render('view', [
            'model'=>$model,
        ]);
    }
}
