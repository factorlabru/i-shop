<?php
namespace app\modules\cart;
use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules(
            [
                'cart'=>'cart/cart/index',
                '<path:([\w_\/-]+\/)?cart>/<_a:[\w-]+>'=>'cart/cart/<_a>',
            ]
        );
    }
}