<?php
$total = 0;
?>

<table class="cart-table" border="1">
    <tr>
        <th></th>
        <th>Название</th>
        <th>Количество</th>
        <th>Цена</th>
        <th>Стоимость</th>
        <th></th>
    </tr>

    <?php foreach ($products as $product) {?>
        <?php
        $url = $product->getUrl();
        $total += $product->price * Yii::$app->cart->countProductQuantity($product->id);
        ?>
        <tr class="cart-product__item" data-product_id="<?=$product->id?>">
            <td>
                <a href="<?=$url;?>">
                    <img src="<?=$product->mainImg();?>">
                </a>
            </td>
            <td>
                <a href="<?=$url?>"><?=$product->name?></a>
            </td>
            <td>
                <div class="product-page__quantity">
                    <span class="quantity-button quantity-down minus-button" data-product_id="<?=$product->id?>">-</span>

                    &nbsp;
                    <input type="text"
                           class="form-control m-mini cart-quantity__field inputNum"
                           name="cart_quantity"
                           value="<?=Yii::$app->cart->countProductQuantity($product->id);?>"
                           data-product_id="<?=$product->id?>"
                           maxlength="6">
                    &nbsp;&nbsp;&nbsp;

                    <span class="quantity-button quantity-up plus-button" data-product_id="<?=$product->id?>">+</span>
                </div>
            </td>
            <td><?=$product->price?></span> руб.</td>
            <td><span class="cart-price__quantity"  data-product_id="<?=$product->id?>"><?=Yii::$app->cart->countProductPrice($product->id);?></span> руб.</td>
            <td>
                <a href="#" class="remove-from-cart__link" data-product_id="<?=$product->id?>">Удалить</a>
            </td>
        </tr>
    <?php }?>
    <tr>
        <td colspan="6" align="right">
            Итого: <span class="cart-page__total"><?=$total?></span> руб.
        </td>
    </tr>
</table>

<hr>
<div style="text-align: right">
    <a href="/order/checkout/" class="btn btn-lg btn-success">
        Оформить заказ
    </a>
</div>