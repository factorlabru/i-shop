<?php
namespace app\modules\promo\widgets;

use yii\base\Widget;
use yii\helpers\Html;


class Promo extends Widget
{
    /**
     * @var string
     */
    public $view = 'promo';

    /**
     * @var int
     *
     * Количество выводимых записей.
     */
    public $limit = 3;

    /**
     * @var string
     *
     * Тип сортировки.
     */
    public $order = 'created_at DESC';

    public function run()
    {
        $news = \app\modules\promo\models\Promo::find(["vis='1'"])
            ->limit((int)$this->limit)
            ->orderBy($this->order)
            ->all();

        return $this->render($this->view, ['news'=>$news]);
    }
}