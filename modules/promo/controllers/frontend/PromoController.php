<?php

namespace app\modules\promo\controllers\frontend;

use app\modules\promo\models\Promo;
use app\components\Controller;
use app\modules\section\models\Section;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;


/**
 * Promo controller for the `promo` module
 */
class PromoController extends Controller
{
    public function actionIndex($path)
    {
        $section = Section::getByPath($path) or $this->fail();

        $this->current_section = $section->id;

        $query = Promo::find()->where(['vis'=>1]);
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(),
            'pageSize'=>Promo::PAGE_SIZE, 'defaultPageSize'=>Promo::PAGE_SIZE]);

        $promo = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        $this->setMetaByModel($section);
        $this->setBreadcrumbs($section->makeBreadcrumbs());

        return $this->render('index', [
            'promo' => $promo,
            'pages' => $pages,
            'section'=>$section,
        ]);
    }

    public function actionView($path, $url_alias)
    {
        $section = Section::getByPath($path);

        if(!$section) {
            throw new \yii\web\NotFoundHttpException('Страница не найдена.');
        }

        $model = Promo::find()->where('url_alias=:url_alias AND vis=1', [':url_alias'=> $url_alias])->one();

        if(!$model) {
            throw new \yii\web\NotFoundHttpException();
        }

        $this->current_section = $section->id;
        $this->current_item = $model->id;

        $this->setMetaByModel($model);
        $this->setBreadcrumbs($section->makeBreadcrumbs(), $model->name);

        return $this->render('view', [
            'model'=>$model,
        ]);
    }
}