<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\widgets\CKEditor as CKEditor;
use app\modules\admin\widgets\ImageRender;
use kartik\date\DatePicker;
use yii\bootstrap\Alert;

/* @var $this yii\web\View */
/* @var $model app\modules\news\models\News */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-form content">

    <?php $form = ActiveForm::begin(); ?>

    <?php if(Yii::$app->session->getFlash('save')) {
        echo Alert::widget([
            'options' => ['class' => 'alert-success',],
            'body' => Yii::$app->session->getFlash('save'),
        ]);
    }?>

    <?= $form->field($model, 'vis')->checkbox() ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php if(Yii::$app->moduleIsActive('yandextranslator')) {
        echo \app\modules\yandextranslator\widgets\UrlTranslate::widget([
            'source_id' => Html::getInputId($model, 'name'),
            'target_id' => Html::getInputId($model, 'url_alias'),
        ]);
    } ?>

    <?= $form->field($model, 'url_alias')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= \app\modules\admin\widgets\CKEditorWidget::widget([
        'form'=>$form,
        'model'=>$model,
        'attr'=>'content',
        'textarea_id'=>'promo-content',
        'gallery_widget'=>false,
    ]); ?>

    <?= $form->field($model, 'img')->fileInput(['accept' => 'image/*']) ?>

    <?php if($model->img) {
        echo ImageRender::widget([
            'id'=>$model->id,
            'attribute'=>'img',
            'img_url'=>$model->img(),
        ]);
    }?>

    <hr>
    <?php
    echo $form->field($model, 'created_at')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'Дата создания акции'],
        'value' => date('Y-m-d'),
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-m-dd'
        ]
    ]);?>

    <?= $this->render('@admin_layouts/chunks/_meta_form', [
        'model' => $model,
        'form' => $form,
    ]); ?>

    <?= $this->render('@admin_layouts/chunks/_save_buttons', [
        'model' => $model,
        'form' => $form,
    ]); ?>

    <?php ActiveForm::end(); ?>
</div>