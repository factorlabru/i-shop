<?php
use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\promo\models\Promo */

$this->title = 'Создание акции';
$this->params['breadcrumbs'][] = ['label' => \Yii::$app->getModule(\Yii::$app->controller->module->id)->module_name, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promo-create content">
    <div class="box">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>