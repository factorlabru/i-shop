<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
?>

<div class="cart-grid-body col-xs-12 col-md-8">
    <h1>Оформить заказ</h1>

    <?php $form = ActiveForm::begin([
        'id'=>'order-form',
        'options'=>['class'=>'order-form__class'],
        'fieldConfig' => [
            'template' => "{input}{error}",
        ]
    ]);?>
    <?= $form->errorSummary($order); ?>

    <div class="form-group">
        <label>ФИО *</label>
        <?=$form->field($order, 'name')->textInput(['class'=>'form-control', 'placeholder'=>'ФИО', 'required'=>true]);?>
    </div>

    <div class="form-group">
        <label>Email *</label>
        <?=$form->field($order, 'email')->textInput(['class'=>'form-control', 'placeholder'=>'Ваш email', 'required'=>true]);?>
    </div>

    <div class="form-group">
        <label>Телефон *</label>
        <?=$form->field($order, 'phone')->textInput(['class'=>'form-control', 'placeholder'=>'Ваш телефон', 'required'=>true]);?>
    </div>

    <div class="form-group">
        <label>Адрес</label>
        <?=$form->field($order, 'message')->textArea(['class'=>'form-control']);?>
    </div>

    <div class="form-group">
        <label>Сообщение</label>
        <?=$form->field($order, 'message')->textArea(['class'=>'form-control']);?>
    </div>

    <div class="button-block">
        <button type="submit" class="btn btn-primary">Заказать</button>
        <?php ActiveForm::end()?>
    </div>

</div>
