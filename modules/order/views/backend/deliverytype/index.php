<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\admin\widgets\grid\LinkColumn;
use mcms\xeditable\XEditableColumn;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\order\models\DeliveryTypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Типы доставки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="delivery-type-index content">

    <div class="box">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Добавить запись', ['create'], ['class' => 'btn btn-success']) ?>
        </p>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'class' => LinkColumn::className(),
                    'attribute' => 'name',
                ],
                ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}{link}'],
            ],
        ]); ?>
    </div>
</div>
