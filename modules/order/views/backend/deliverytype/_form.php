<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\order\models\DeliveryType */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="delivery-type-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?//= Html::submitButton('Сохранить и закрыть', ['name' => 'save_close', 'value'=>'save_close', 'class'=>'btn btn-primary']) ?>
        <?= Html::submitButton('Сохранить и продолжить', ['name'=>'save_continue',  'value'=>'save_continue', 'class'=>'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
