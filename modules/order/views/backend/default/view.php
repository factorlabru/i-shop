<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model app\modules\order\models\Order */
$this->title = 'Заказ №'.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-view content">
    <b>Дата создания:</b><br />
    <?php echo Html::encode(\app\helpers\Common::formatDate($model->created_at))?><br /><br />
    <b>Имя:</b><br />
    <?php echo Html::encode($model->name)?><br /><br />
    <?php if($model->phone) { ?>
        <b>Телефон:</b><br />
        <?php echo Html::encode($model->phone)?><br /><br />
    <?php }?>
    <?php if($model->email) { ?>
        <b>Email:</b><br />
        <?php echo Html::encode($model->email)?><br /><br />
    <?php }?>
    <?php if($model->message) { ?>
        <b>Сообщение:</b><br />
        <?php echo Html::encode($model->message)?><br /><br />
    <?php }?>
</div>