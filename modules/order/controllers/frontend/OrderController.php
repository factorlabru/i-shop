<?php

namespace app\modules\order\controllers\frontend;

use Yii;
use app\modules\order\models\Order;
use app\modules\order\models\OrderSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\components\Controller;

/**
 * Order controller for the `order` module
 */
class OrderController extends Controller
{
    public function actionCheckout()
    {
        $this->setAllMeta(['title'=>'Оформление заказа']);

        $products = Yii::$app->cart->getProducts();

        if(!$products) {
            return $this->redirect('/cart/');
        }

        $order = new Order();
        if($order->load(Yii::$app->request->post()) ) {
            $order->total = Yii::$app->cart->countTotalPrice();
            if($order->save()) {

                $mail = (new \app\helpers\Mail())->sendMail($order,
                    Yii::$app->params['admin_email'],
                    '@modules/order/mails/letter',
                    'Заказ с сайта');

                $order->saveProducts($order->id, Yii::$app->cart->getProducts());
                return $this->redirect(['/order/complete']);
            }
        }

        return $this->render('checkout', [
            'products'=>$products,
            'order'=>$order,
        ]);
    }

    public function actionComplete()
    {
        return $this->render('complete');
    }
}