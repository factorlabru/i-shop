<?php
namespace app\modules\order\models;
use Yii;
/**
 * This is the model class for table "tbl_order_products".
 *
 * @property integer $product_id
 * @property integer $order_id
 * @property integer $total
 * @property integer $quantity
 * @property string $name
 * @property integer $price
 * @property string $created_at
 */
class OrderProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_order_products';
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'order_id'], 'required'],
            [['product_id', 'order_id', 'total', 'price', 'quantity'], 'integer'],
            [['created_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'order_id' => 'Order ID',
            'total' => 'Total',
            'quantity' => 'Quantity',
            'name' => 'Name',
            'price' => 'Price',
            'created_at' => 'Created At',
        ];
    }
}