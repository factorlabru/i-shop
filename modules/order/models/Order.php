<?php

namespace app\modules\order\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "tbl_orders".
 *
 * @property integer $id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $message
 * @property integer $viewed
 * @property string $updated_at
 * @property string $created_at
 */
class Order extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            'log' => [
                'class' => 'app\behaviors\LogBehavior',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['message'], 'string'],
            [['viewed'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['name', 'phone', 'email'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ФИО',
            'phone' => 'Телефон',
            'email' => 'Email',
            'viewed' => 'Запись просмотрена',
            'message' => 'Сообщение',
            'updated_at' => 'Updated At',
            'created_at' => 'Дата заказа',
        ];
    }

    /**
     * Сохранение товаров.
     * @param $order_id
     * @param $products
     * @param $cart_products
     */
    public function saveProducts($order_id, $products)
    {
        foreach($products as $product) {
            $model = new OrderProduct();
            $model->order_id = $order_id;
            $model->product_id = $product['id'];
            $model->price = $product['price'];
            $model->quantity = Yii::$app->session['cart_products'][$product['id']]['quantity'];
            if($model->validate()) {
                $model->save();
            }
        }
    }

    public function getProducts()
    {
        return $this->hasMany(OrderProduct::className(), ['order_id' => 'id']);
    }
}
