<?php

namespace app\modules\testimonials;

return [
    'params' => [
        'viewed'=>true,
        'model'=>'\app\modules\testimonials\models\Testimonial',
    ],
];