<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use kartik\date\DatePicker;
//use kartik\editable\Editable;
use kartik\grid\EditableColumn;
use dosamigos\editable\Editable;



/* @var $this yii\web\View */
/* @var $searchModel app\modules\testimonials\models\TestimonialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

/*$this->title = 'Testimonials';
$this->params['breadcrumbs'][] = $this->title;*/
?>
<div class="testimonial-index content">

    <h1><?= Html::encode('Отзывы') ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= Editable::widget( [
        'name' => 'country_code',
        'value' => '',
        'url' => 'site/test',
        'type' => 'select2',
        'mode' => 'pop',
        'clientOptions' => [
            'pk' => 2,
            'placement' => 'right',
            'select2' => [
                'width' => '124px'
            ],
            'source' => [
                ['id' => 'gb', 'text' => 'Great Britain'],
                ['id' => 'es', 'text' => 'Spain'],
            ],
        ]
    ]);?>

    <p>
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Добавить отзыв', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <style>
        /*.kv-editable-reset {
            display: none !important;
        }*/
    </style>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'export'=>false,
        'pjax'=>true,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            'email:email',
            'phone',
            [
                'pageSummary'=>true,
                'class'=>'kartik\grid\EditableColumn',
                'attribute'=>'moderation',
                'editableOptions'=> function ($model, $key, $index) {
                    return [
                        'formOptions' => ['action' => ['/admin/testimonials/default/ajax-editable']],
                        'submitButton'=>['icon'=>'<i class="glyphicon glyphicon-ok"></i>'],
                        'resetButton'=>['icon'=>'<i class="glyphicon glyphicon-remove"></i>'],
                        'inputType'=>\kartik\editable\Editable::INPUT_DROPDOWN_LIST,
                        'data'=>[1=>'Да', 2=>'Нет'],

                    ];
                },
                'value' => function ($model){
                    return $model->moderation==1 ? 'Да' : 'Нет';
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'moderation',
                    ['1' => 'Да', '0' => 'Нет'],
                    ['class'=>'form-control','prompt' => 'Все']
                ),
            ],
            /*[
                'attribute' => 'moderation',
                'format' => 'html',
                'value' => function ($model){
                    return $model->moderation==1 ? 'Да' : 'Нет';
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'moderation',
                    ['1' => 'Да', '0' => 'Нет'],
                    ['class'=>'form-control','prompt' => 'Все']
                ),
            ],*/
            [
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'pluginOptions' => ['format' => 'yyyy-mm-dd']
                ]),
                'attribute' => 'created_at',
                'format' => ['date', 'php:Y-m-d'],
            ],

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}{link}'],
        ],
    ]); ?>
</div>


if (Yii::$app->request->post('hasEditable')) {
\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

$model = $this->findModel($_POST['editableKey']);
$model->moderation = $_POST['Testimonial'][$_POST['editableIndex']][$_POST['editableAttribute']];

if($model->validate()) {
$model->save();

return ['output'=>'aaa', 'message'=>''];
} else {
return ['output'=>'', 'message'=>''];
}
}