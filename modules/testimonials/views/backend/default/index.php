<?php

use app\modules\admin\helpers\XEditableHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;
use yii\widgets\Pjax;
use app\modules\admin\widgets\grid\LinkColumn;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\testimonials\models\TestimonialSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \Yii::$app->getModule(\Yii::$app->controller->module->id)->module_name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="testimonial-index content">

    <div class="box">
    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <div>
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Добавить запись', ['create'], ['class' => 'btn btn-success']) ?>

        <?=\app\modules\admin\widgets\GridMultiDelete::widget()?>
    </div>

    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\CheckboxColumn'],
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => LinkColumn::className(),
                'attribute' => 'name',
            ],
            'email',
            'phone',
            [
                'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'created_at',
                    'pluginOptions' => ['format' => 'yyyy-mm-dd']
                ]),
                'attribute' => 'created_at',
                'format' => ['date', 'php:Y-m-d'],
            ],
            [
                'attribute' => 'viewed',
                'value'=>function($model) {
                    return $model->viewed == 1 ? 'Да' : 'Нет';
                },
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'viewed',
                    [1=>'Да', 0=>'Нет'],
                    ['class'=>'form-control','prompt' => 'Все']
                ),
            ],

            XEditableHelper::buildYesNo('vis'),

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}{link}'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
    </div>
</div>