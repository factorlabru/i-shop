<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\testimonials\models\Testimonial */

$this->title = 'Редактирование отзыва';
$this->params['breadcrumbs'][] = ['label' => \Yii::$app->getModule(\Yii::$app->controller->module->id)->module_name, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="testimonial-update content">
    <div class="box">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>