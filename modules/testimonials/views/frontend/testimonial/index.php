<?php
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\Url;
use app\helpers\Common;
?>

<h1><?= $this->h1_tag ?></h1>

<div class="col-sm-8">
    <?php if($testimonials) { ?>

        <?php foreach($testimonials as $testimonial) {?>

            <?php echo Html::beginTag('div', ['class'=>'testimonial-item']);?>

            <h2>
               <?=$testimonial['name']?>
            </h2>
            <span><?=Common::formatDate($testimonial['created_at'])?></span> <br><br>

            <div class="entry">
                <?=Html::encode($testimonial['message'])?>
            </div>
            <div class="clearfix"></div>

            <?php echo Html::endTag('div');?>

        <?php }?>

        <div class="pagination">
            <?php
            echo \app\widgets\LinkPager::widget([
                'pagination' => $pages,
                'nextPageLabel'=>'вперед →',
                'prevPageLabel'=>'← назад'
            ]); ?>
        </div>
    <?php }?>
</div>

<div  class="col-sm-4">
    <div class="sidebar-module">
        <a href="/testimonials/create" type="button" class="btn btn-lg btn-success">Добавить отзыв</a>
    </div>
</div>