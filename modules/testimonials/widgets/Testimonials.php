<?php
namespace app\modules\testimonials\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use app\modules\testimonials\models\Testimonial;

/**
 * Виджет вывводит отзывы, исходя из заданных параметров.
 *
 * Class TestimonialsWidget
 * @package common\widgets
 */
class Testimonials extends Widget
{
    /**
     * @var string
     */
    public $view='testimonials';

    /**
     * @var int
     *
     * Количество выводимых записей.
     */
    public $limit = 3;

    /**
     * @var string
     *
     * Тип сортировки.
     */
    public $order = 'created_at DESC';

    public function run()
    {
        $testimonials = Testimonial::find()
            ->where(['vis'=>1])
            ->limit((int)$this->limit)
            ->orderBy($this->order)
            ->all();

        return $this->render($this->view, ['testimonials'=>$testimonials]);
    }
}