<?php

namespace app\modules\testimonials\controllers\frontend;

use app\modules\section\models\Section;

use app\components\Controller;
use Yii;
use app\modules\testimonials\models\Testimonial;
use yii\data\Pagination;



/**
 * Testimonial controller for the `testimonia;s` module
 */
class TestimonialController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                //'class' => 'yii\captcha\CaptchaAction',
                'class' => 'app\components\NumericCaptcha',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex($path)
    {
        $section = Section::getByPath($path) or $this->fail();

        $this->current_section = $section->id;

        $query = Testimonial::find()->where('vis = 1');
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(),
            'pageSize'=>Testimonial::PAGE_SIZE, 'defaultPageSize'=>Testimonial::PAGE_SIZE]);

        $testimonials = $query->offset($pages->offset)
            ->orderBy('created_at DESC')
            ->limit($pages->limit)
            ->all();

        $this->setMetaByModel($section);
        $this->setBreadcrumbs($section->makeBreadcrumbs());

        return $this->render('index', [
            'testimonials' => $testimonials,
            'pages' => $pages,
        ]);
    }

    public function actionCreate()
    {
        $model = new Testimonial();

        if($model->load(Yii::$app->request->post())) {
            $model->vis = 0;
            if($model->save()) {
                $mail = (new \app\helpers\Mail())->sendMail($model,
                    Yii::$app->params['admin_email'],
                    '@modules/testimonials/mails/letter',
                    'Отзыв с сайта');

                return $this->redirect(['/testimonials/complete']);
            }
        }

        $this->setAllMeta(['title'=>'Создание отзыва']);

        return $this->render('create', [
            'model'=>$model,
        ]);
    }

    public function actionComplete()
    {
        $this->setAllMeta(['title'=>'Сообщение отправлено']);

        return $this->render('complete');
    }
}