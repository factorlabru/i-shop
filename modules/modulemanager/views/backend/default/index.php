<?php

use app\modules\admin\helpers\XEditableHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;
use yii\widgets\Pjax;
use app\modules\admin\widgets\grid\LinkColumn;
use mcms\xeditable\XEditableColumn;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\promo\models\PromoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Менеджер модулей';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promo-index content">
    <div class="box">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <div>
            <?=\app\modules\admin\widgets\GridMultiDelete::widget()?>
        </div>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'class' => \app\modules\admin\widgets\grid\sortable\SortableColumn::className(),
                    'url' => \yii\helpers\Url::toRoute(['ajax-sorting'])
                ],
                ['class' => 'yii\grid\CheckboxColumn'],
                ['class' => 'yii\grid\SerialColumn'],
                XEditableHelper::buildEditField('name', 'textarea'),
                'module',
                XEditableHelper::buildEditField('icon', 'textarea'),
                XEditableHelper::buildYesNo('menu_vis'),
                XEditableHelper::buildYesNo('active'),
                //XEditableHelper::buildYesNo('bootstrap'),

                ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}{link}'],
            ],
        ]); ?>
    </div>
</div>