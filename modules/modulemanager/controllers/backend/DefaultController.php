<?php

namespace app\modules\modulemanager\controllers\backend;

use app\modules\admin\actions\AjaxDeleteGridItemsAction;
use app\modules\admin\actions\AjaxEditableAction;
use app\modules\admin\widgets\grid\sortable\AjaxGridSortableAction;
use app\modules\modulemanager\models\Module;
use app\modules\modulemanager\models\ModuleSearch;
use Yii;

use yii\web\NotFoundHttpException;
use app\modules\admin\components\AdminController;

/**
 * DefaultController implements the CRUD actions for Module model.
 */
class DefaultController extends AdminController
{


    public function actions()
    {
        $model_name = Module::className();

        return [
            'ajax-editable' => [
                'class' => AjaxEditableAction::className(),
                'model' => $model_name,
            ],

            'ajax-delete-grid-items' => [
                'class' => AjaxDeleteGridItemsAction::className(),
                'model' => $model_name,
            ],

            'ajax-sorting' => [
                'class' =>  AjaxGridSortableAction::className(),
                'modelClass' => $model_name,
            ],

        ];

    }

    /**
     * Lists all Module models.
     * @return mixed
     */
    public function actionIndex()
    {
        collect(Yii::$app->modules_config) // получаем модули которые есть в папке modules
            ->forget(collect(Module::find()->asArray()->all()) // убираем записи, которые уже есть в базе
                ->pluck('module')
                ->all()
            )
            ->keys() // берем только ключи оставшихся записей (которые являются именами модулей)
            ->each(function ($module_name) { // по каждому ключу создаем новый модуль в базе
                (new Module([
                    'module' => $module_name,
                ]))
                    ->loadDefaultValues()
                    ->save();
            });

        $searchModel = new ModuleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Module model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Module model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Module();
        $model->loadDefaultValues();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->getSession()->setFlash('save', 'Данные сохранены');

            return $this->moduleRedirect($model->id);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Module model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->getSession()->setFlash('save', 'Данные сохранены');

            return $this->moduleRedirect($model->id);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Module model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Module model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Module the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Module::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Страница не найдена.');
        }
    }
}