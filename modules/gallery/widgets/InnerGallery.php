<?php
namespace app\modules\gallery\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use app\modules\gallery\models\Gallery;

/**
 * Виджет для вывода галерей в контенте.
 *
 * Class InnerGallery
 * @package app\modules\gallery\widgets
 */
class InnerGallery extends Widget
{
    public $gallery_id;

    public function run()
    {
        $gallery = Gallery::find()
            ->where(['id'=>$this->gallery_id, 'vis'=>1])
            ->one();

        if($gallery) {
            return $this->render('inner_gallery', ['gallery'=>$gallery]);
        }
    }
}

