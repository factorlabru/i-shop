<?php
use yii\helpers\Html;
?>

<?php if($gallery->images) {?>
    <ul>
        <?php foreach($gallery->images as $image) {?>
            <li>
                <a href="<?=$gallery->getImagePath().'thumb_big_'.$image->img?>"
                   class="fancybox" rel="gal<?=$gallery->id?>">
                    <img src="<?=$gallery->getImagePath().'thumb_'.$image->img?>" alt="">
                </a>
            </li>
        <?php }?>
    </ul>
<?php }?>