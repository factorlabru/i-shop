<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\gallery\models\Gallery */

$this->title = 'Редактирование галереи';
$this->params['breadcrumbs'][] = ['label' => 'Галереи', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="gallery-update content">
    <div class="box">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
