<?php

namespace app\modules\gallery\models;

use app\behaviors\UploadManyImageBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "tbl_galleries".
 *
 * @mixin UploadManyImageBehavior
 *
 * @property integer $id
 * @property string $name
 * @property string $url_alias
 * @property string $cover
 * @property integer $vis
 * @property integer $priority
 * @property string $updated_at
 * @property string $created_at
 */
class Gallery extends \yii\db\ActiveRecord
{
    public $file_loader;

    public function behaviors()
    {
        return [
            'log' => [
                'class' => 'app\behaviors\LogBehavior',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'slug' => [
                'class' => 'app\behaviors\Slug',
                'in_attribute' => 'name',
                'out_attribute' => 'url_alias',
                'translit' => true
            ],
            'PriorityBehavior' => [
                'class' => 'app\behaviors\PriorityBehavior',
            ],
            [
                'class' => UploadManyImageBehavior::className(),
                'attribute' => 'file_loader',
                'behavior_name' => 'image',
                'relation' => 'images',
            ],

        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_galleries';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'url_alias'], 'required'],
            [['name'], 'trim'],
            [['vis', 'priority'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            ['file_loader', 'image', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg, gif', 'maxFiles' => 10],
            [['name', 'url_alias'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'url_alias' => 'Url Alias',
            'cover' => 'Обложка',
            'vis' => 'Показывать',
            'file_loader' => 'Фото галереи',
            'priority' => 'Приоритет',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

    public function getImages()
    {
        return $this->hasMany(GalleryImage::className(), ['gallery_id' => 'id'])->orderBy(['priority'=>SORT_ASC]);
    }
}
