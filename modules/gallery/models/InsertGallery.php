<?php
namespace app\modules\gallery\models;

class InsertGallery
{
    public static function parseData($model, $attributes = ['content'])
    {
        foreach($attributes as $attribute){
            static::parseAttribute($model, $attribute);
        }
        return $model;
    }

    private static function parseAttribute($model, $attribute)
    {
        preg_match_all("/<img(.*?) id=\"inner-gallery(.*?)\"(.*?)\/>/is", $model->$attribute, $matches);
        if(!empty($matches[2])) {
            foreach($matches[2] as $key => $gallery_id){
                $gallery = \app\modules\gallery\widgets\InnerGallery::widget(['gallery_id'=>$gallery_id]);
                $model->$attribute = preg_replace("/<img id=\"inner-gallery".$gallery_id."\"(.*?)\/>/is", $gallery, $model->$attribute);
            }
        }
        return $model;
    }
}