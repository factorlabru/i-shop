<?php

namespace app\modules\user\controllers\backend;

use Yii;
use app\modules\admin\components\AdminController;
use app\modules\admin\models\backend\User;
use app\modules\admin\models\backend\LoginForm;

/**
 * Default controller for the `user` module
 */

class DefaultController extends AdminController
{

    public function actionIndex()
    {
        $this->setbcAction('Список пользователей');
        $model = new User();
        return $this->render('index',['model'=>$model]);
    }

    public function actionCreate()
    {
        $this->setbcAction('Добавление пользователя');
        $model = new User(['scenario' => 'create']);
        if ($model->load($_POST) && $model->save()) {
            Yii::$app->authManager->revokeAll($model->id);
            $role = Yii::$app->authManager->getRole($model->role_auth);
            if ($role) {
                Yii::$app->authManager->assign($role, $model->id);
            }
            $this->goToIndex();
        }
        return $this->render('create',['model'=>$model]);
    }

    public function actionView()
    {
        $this->setbcAction('Профиль пользователя');
        $id = $_GET['id'];
        $model = User::findOne(['id' => $id]);
        return $this->render('view',['model'=>$model]);
    }

    public function actionUpdate()
    {
        $this->setbcAction('Редактирование профиля пользователя');
        $id = $_GET['id'];
        $model = User::findOne(['id' => $id]);
        if ($model->load($_POST) && $model->save()) {
            Yii::$app->authManager->revokeAll($model->id);
            $role = Yii::$app->authManager->getRole($model->role_auth);
            if ($role) {
                Yii::$app->authManager->assign($role, $model->id);
            }
            $this->goToIndex();
        }
        return $this->render('update',['model'=>$model]);
    }

    public function actionDelete()
    {
        $id = $_GET['id'];
        if (!empty($id) && ($id != 1)) {
            $model = User::findOne(['id' => $id]);
            if (!empty($model)) {
                $model->delete();
            }
        }
        $this->goToIndex();
    }

    public function actionLogin()
    {
        $this->setbcAction('Вход в систему');
        $this->layout = 'blank';
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }
    

    public function beforeAction($action)
    {
        if (($action->id == 'login') || ($action->id == 'logout')) {
            return true;
        } else {
            return parent::beforeAction($action);
        }
    }

}
