<?php
namespace app\modules\user\controllers\backend;

use Yii;
use app\modules\admin\components\AdminController;
use app\modules\admin\models\backend\User;
use app\modules\user\views\backend\access\RuleForm;

class RulesController extends AdminController
{
	public $_bcController = 'Права доступа';

	public function actionIndex()
	{
		$this->setbcAction('Системные правила доступа');
		$model = new User();
		return $this->render('index',['model'=>$model]);
	}
	
	public function actionCreate()
	{
		$this->setbcAction('Добавление правила');
		$model = new RuleForm;
		if ($model->load($_POST) && $model->validate()) {
			$role = Yii::$app->authManager->createPermission($model->role);
			$role->description = $model->description;
			Yii::$app->authManager->add($role);
			$this->goToIndex();
		}
		return $this->render('create',['model'=>$model]);
	}
	
	public function actionUpdate()
	{
		$this->setbcAction('Редактирование правила');
		$id = $_GET['id'];
		$permission = Yii::$app->authManager->getPermission($id);
		$model = new RuleForm;
		$model->role = $permission->name;
		$model->description = $permission->description;

		if ($model->load($_POST) && $model->validate()) {
			$permission->name = $model->role;
			$permission->description = $model->description;
			Yii::$app->authManager->update($id, $permission);
			$this->goToIndex();
		}
		return $this->render('update', ['model'=>$model]);
	}
	
	public function actionDelete()
	{
		if (!empty($_GET['id'])) {
            $id = $_GET['id'];

			$model = Yii::$app->authManager->getPermission($id);
			if (!empty($model)) {
				Yii::$app->authManager->remove($model);
			}
		}
		$this->goToIndex();
	}
}