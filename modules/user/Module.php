<?php

namespace app\modules\user;

use Yii;
/**
 * user module definition class
 */
class Module extends \app\modules\admin\Module
{

    public $module_name = 'Пользователи';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        \Yii::configure($this, require(__DIR__ . '/config.php'));

        // custom initialization code goes here
    }
}
