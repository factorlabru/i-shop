<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Редактирование правила: ' . ' ' . $permit->description;
$this->params['breadcrumbs'][] = ['label' => 'Правила доступа', 'url' => ['permission']];
$this->params['breadcrumbs'][] = 'Редактирование правила';
?>
<div class="news-index content">
    <div class="links-form box">

        <?php if (!empty($error)) { ?>
            <div class="error-summary">
                <?php echo implode('<br>', $error); ?>
            </div>
        <?php } ?>

        <?php $form = ActiveForm::begin(); ?>

        <div class="form-group">
            <?= Html::label('Текстовое описание'); ?>
            <?= Html::textInput('description', $permit->description, ['class'=>'form-control']); ?>
        </div>

        <div class="form-group">
            <?= Html::label('Разрешенный доступ'); ?>
            <?= Html::textInput('name', $permit->name, ['class'=>'form-control']); ?>
        </div>

        <?= $this->render('@admin_layouts/chunks/_save_buttons', [
            //'model' => $model,
            'form' => $form,
        ]); ?>

        <?php ActiveForm::end(); ?>

    </div>
</div>