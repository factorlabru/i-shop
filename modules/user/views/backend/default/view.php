<?php
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = $this->context->getbcAction();
$statuses = $model->getStatuses();
?>

<section class="content">
    <p><b>Имя пользователя:</b> <?=Html::encode($model->username);?></p>
    <p><b>Email:</b> <?=$model->email;?></p>
    <p><b>Статус:</b> <?=$statuses[$model->status];?></p>

    <div class="well">
        <div class="control-group">
            <b>Последний вход: </b> <?=Html::encode($model->last_sign_in)?>
        </div><br>
        <div class="control-group">
            <b>Текущий IP: </b> <?=Html::encode($model->current_sign_in_ip)?>
        </div><br>
        <div class="control-group">
            <b>Последний вход был с IP: </b> <?=Html::encode($model->last_sign_in_ip)?>
        </div>
    </div>
    <a href="/admin/user/" class="btn btn-success">Назад</a> &nbsp;
    <a href="/admin/user/<?=$model->id?>/update/" class="btn btn-success">Редактировать</a>
</section>
