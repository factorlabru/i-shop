<?php

use yii\grid\GridView;
use yii\helpers\Html;
use app\modules\admin\widgets\grid\LinkColumn;


/* @var $this yii\web\View */

$this->title = 'Пользователи';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Main content -->
<section class="content">
    <div class="box">

        <?php if ($this->context->_enableCreate) { ?>
            <div>
                <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Добавить запись', ['create'], ['class' => 'btn btn-success']) ?>
            </div>
        <?php } ?>

        <?= GridView::widget([
            'dataProvider' => $model->search(Yii::$app->request->get()),
            'filterModel' => $model,
            'columns' => [
                'id',
                [
                    'class' => LinkColumn::className(),
                    'attribute' => 'username',
                ],
                'email',
                [
                    'attribute' => 'role_auth',
                    'value'=>function($model) {
                        return isset(Yii::$app->authManager->getRoles()[$model->role_auth]) ? Yii::$app->authManager->getRoles()[$model->role_auth]->description : '';
                    },
                ],
                [
                    'value'=>function($model) {
                        return $model->status == 10 ? 'Активен' : 'Не активен';
                    },

                    'attribute' => 'status',
                    'format' => 'raw',
                    'filter' => Html::activeDropDownList(
                        $model,
                        'status',
                        ['10' => 'Активен', '0' => 'Не активен'],
                        ['class'=>'form-control','prompt' => 'Все']
                    ),
                ],

                ['class' => 'yii\grid\ActionColumn',
                    'visibleButtons' => [
                        'delete' => function ($model, $key, $index) {return ($model->id != 1);}
                    ]],
            ],
        ]) ?>
    </div>
</section>
<!-- /.content -->
