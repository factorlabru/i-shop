<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

?>

<?php $form = ActiveForm::begin(); ?>
<?= $form->field($model, 'status')->dropDownList($model->getStatuses()); ?>

<?= $form->field($model, 'role_auth')->dropDownList(ArrayHelper::map(Yii::$app->authManager->getRoles(),'name','description')) ?>

<?= $form->field($model, 'username') ?>

<?= $form->field($model, 'email') ?>

<?= $form->field($model, 'password_field', [
    'template' => '{label}
                <div class="col-sm-12 nopadding inline-input">
                    {input} <a href="#" class="generate-password__link"><i class="fa fa-cogs" aria-hidden="true"></i></a>
                    {error}{hint}
                </div>'
])->passwordInput(['maxlength' => true, 'style'=>'display:inline'])?>

<?php if(!$model->isNewRecord) { ?>
    <?= $form->field($model, 'invalidate_all_sessions')->checkbox() ?>
<?php } ?>

<?= $this->render('@admin_layouts/chunks/_save_buttons', [
    'model' => $model,
    'form' => $form,
]); ?>

<?php ActiveForm::end(); ?>