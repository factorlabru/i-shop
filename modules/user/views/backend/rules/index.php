<?php
use yii\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Права роли';
$this->params['breadcrumbs'][] = $this->title;
?>
 
<!-- Main content -->
<section class="content">
    <div class="box">

        <?php if ($this->context->_enableCreate) { ?>
            <div>
                <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Добавить запись', ['create'], ['class' => 'btn btn-success']) ?>
            </div>
        <?php } ?>

        <?= GridView::widget([
            'dataProvider' => new ArrayDataProvider([
                'allModels' => Yii::$app->authManager->getPermissions(),
                'sort' => [
                    'attributes' => ['id', 'username', 'email'],
                ],
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]),
            'filterModel' => $model,
            'columns' => [
                'name:text:Правило',
                'description:text:Описание',
                [
                    'header'=>'Дата создания',
                    'attribute' => 'createdAt',
                    'format' => ['date', 'php:d-m-Y']
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}',
                    'visibleButtons' => [
                        'update' => function ($model, $key, $index) {return ($model->name != 'admin');},
                        'delete' => function ($model, $key, $index) {return ($model->name != 'admin');}
                    ],
                ],
            ],
        ]) ?>
    </div>
</section>
<!-- /.content -->
