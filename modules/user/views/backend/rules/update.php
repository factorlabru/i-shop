<?php
use yii\widgets\Breadcrumbs;
use app\widgets\Alert;
use app\modules\admin\models\backend\User;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Редактирование правила';
$this->params['breadcrumbs'][] = ['label' => 'Редактирование правила', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- Main content -->
<section class="content">
    <div class="box">
        <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'role') ?>
        <?= $form->field($model, 'description') ?>
        <?= Html::submitButton(Yii::t('app', 'Отправить'), ['class' => 'btn btn-primary']) ?>
        <?php ActiveForm::end(); ?>
    </div>
</section>
<!-- /.content -->
