<?php
namespace app\modules\callback;
use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules(
            [
                '<path:([\w_\/-]+\/)?callback>/<_a:[\w-]+>' => 'callback/callback/<_a>',
            ]
        );
    }
}