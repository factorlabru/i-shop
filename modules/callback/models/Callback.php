<?php

namespace app\modules\callback\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "tbl_callbacks".
 *
 * @property integer $id
 * @property string $name
 * @property string $phone
 * @property string $ip
 * @property string $updated_at
 * @property string $created_at
 */
class Callback extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            'log' => [
                'class' => 'app\behaviors\LogBehavior',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if($this->isNewRecord){
            $this->ip = Yii::$app->request->userIP;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_callbacks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'phone'], 'required'],
            [['updated_at', 'created_at'], 'safe'],
            [['name', 'phone'], 'string', 'max' => 255],
            [['ip'], 'string', 'max' => 55],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'phone' => 'Телефон',
            'viewed' => 'Запись просмотрена',
            'ip' => 'Ip',
            'updated_at' => 'Updated At',
            'created_at' => 'Дата создания',
        ];
    }

    public function sendMail($model, $mail_to, $mail_from='', $subject='Обратный звонок')
    {
        $mail_from = $mail_from ? $mail_from : 'noreply@'.$_SERVER['SERVER_NAME'];

        Yii::$app->mailer->compose('@modules/callback/mails/letter', ['model' => $model])
            ->setFrom($mail_from)
            ->setTo($mail_to)
            ->setSubject($subject.' '.$_SERVER['SERVER_NAME'])
            ->send();
    }
}
