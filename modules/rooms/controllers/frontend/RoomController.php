<?php

namespace app\modules\rooms\controllers\frontend;

use app\modules\rooms\models\Room;
use app\components\Controller;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;


/**
 * Room controller for the `rooms` module
 */
class RoomController extends Controller
{
    public function actionIndex($path)
    {
        $section = \app\modules\section\models\Section::getByPath($path);

        $section or $this->fail();
        $this->current_section = $section->id;

        $query = Room::find();
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(),
            'pageSize'=>20, 'defaultPageSize'=>20]);

        $rooms = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        $this->setMetaByModel($section);
        $this->setBreadcrumbs($section->makeBreadcrumbs());

        return $this->render('index', [
            'rooms' => $rooms,
            'pages' => $pages,
        ]);
    }

    public function actionView($path, $url_alias)
    {
        $section = \app\modules\section\models\Section::getByPath($path);

        if(!$section) {
            throw new \yii\web\NotFoundHttpException('Страница не найдена.');
        }

        $model = Room::find()->where('url_alias=:url_alias AND vis=1', [':url_alias'=> $url_alias])->one();

        if(!$model) {
            throw new \yii\web\NotFoundHttpException();
        }

        $this->current_section = $section->id;
        $this->current_item = $model->id;

        $this->setMetaByModel($model);
        $this->setBreadcrumbs(
            $section->makeBreadcrumbs(true),
            $model->name
        );

        return $this->render('view', [
            'model'=>$model,
        ]);
    }
}