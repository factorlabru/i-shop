<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<h2><?=$title;?></h2>

<?php if($rooms) {?>
    <?php foreach($rooms as $room) {?>
        <div class="col-md-4">
            <h3><?=$room->name?></h3>
            <?php if($room->mainImg('thumb')) {
                $img = Html::img($room->mainImg('thumb'), ['width'=>300]);
                echo Html::a($img, [
                    'room/view',
                    'path'=>Yii::$app->request->get('path'),
                    'url_alias'=>$room->url_alias
                ]);
            } ?>
        </div>
    <?php }?>
<?php }?>