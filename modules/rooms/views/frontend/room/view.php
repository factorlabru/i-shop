<?php
use yii\helpers\Html;
use app\modules\rooms\widgets\Rooms;
?>

<h1><?=\app\helpers\Common::pageTitle($model)?></h1>

<div class="row">
    <div class="col-md-7">
        <?php if($img = $model->mainImg('thumb_big')) {
            echo Html::img($img, ['class' => 'img-thumbnail']);

            foreach($model->images as $item) {
                echo Html::img($item->img(), ['width'=>150, 'class'=>'img-thumbnail']);
            }
        }?>
    </div>


    <div class="col-md-5">
        <?=$model->description?>
    </div>

    <div class="clear"></div>
    <br>
    <div class="col-md-12">
        <?=$model->content?>
    </div>

    <?=Rooms::widget([
        'order'=>'RAND()',
        'not_id'=>$model->id,
    ]);?>
</div>