<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\rooms\models\Room */
$this->title = 'Редактирование';
$this->params['breadcrumbs'][] = ['label' => \Yii::$app->getModule(\Yii::$app->controller->module->id)->module_name, 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="room-update content">
    <div class="box">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
