<?php

use app\modules\admin\helpers\XEditableHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\admin\widgets\grid\LinkColumn;
use mcms\xeditable\XEditableColumn;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\rooms\models\RoomSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = \Yii::$app->getModule(\Yii::$app->controller->module->id)->module_name;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="room-index content">
    <div class="box">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <div>
            <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Добавить запись', ['create'], ['class' => 'btn btn-success']) ?>
            <?=\app\modules\admin\widgets\GridMultiDelete::widget()?>
        </div>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\CheckboxColumn'],
                ['class' => 'yii\grid\SerialColumn'],
                [
                    'class' => LinkColumn::className(),
                    'attribute' => 'name',
                ],
                'url_alias',
                'price_single',
                XEditableHelper::buildEditPriority(),
                XEditableHelper::buildYesNo('vis'),
                ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}{link}'],
            ],
        ]); ?>
    </div>
</div>
