<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\widgets\CKEditor as CKEditor;
use app\modules\admin\widgets\ImageBlocks;
use yii\bootstrap\Alert;

/* @var $this yii\web\View */
/* @var $model app\modules\rooms\models\Room */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="room-form content">

    <?php $form = ActiveForm::begin(); ?>

    <?php if(Yii::$app->session->getFlash('save')) {
        echo Alert::widget([
            'options' => ['class' => 'alert-success',],
            'body' => Yii::$app->session->getFlash('save'),
        ]);
    }?>

    <div class="col-md-9 nopadding">
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?php if(Yii::$app->moduleIsActive('yandextranslator')) {
            echo \app\modules\yandextranslator\widgets\UrlTranslate::widget([
                'source_id' => Html::getInputId($model, 'name'),
                'target_id' => Html::getInputId($model, 'url_alias'),
            ]);
        } ?>

        <?= $form->field($model, 'url_alias')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'price_single')->textInput(['class'=>'form-control m-mini']) ?>

        <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
    </div>

    <div class="clear"></div>

    <?= \app\modules\admin\widgets\CKEditorWidget::widget([
        'form'=>$form,
        'model'=>$model,
        'attr'=>'content',
        'textarea_id'=>'room-content',
        'gallery_widget'=>false,
    ]); ?>

    <?= $form->field($model, 'priority')->textInput(['class'=>'form-control m-mini']) ?>

    <?= $form->field($model, 'file_loader[]')->fileInput(['multiple' => true, 'accept' => 'image/*']) ?>

    <?=ImageBlocks::widget([
        'data'=>$model->images,
    ]);?>

    <?= $this->render('@admin_layouts/chunks/_meta_form', [
        'model' => $model,
        'form' => $form,
    ]); ?>

    <?= $this->render('@admin_layouts/chunks/_save_buttons', [
        'model' => $model,
        'form' => $form,
    ]); ?>

    <?php ActiveForm::end(); ?>

</div>
