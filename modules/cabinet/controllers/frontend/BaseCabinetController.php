<?php

namespace app\modules\cabinet\controllers\frontend;

use app\components\Controller;
use Yii;
use yii\filters\AccessControl;


class BaseCabinetController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'controllers' => ['cabinet/auth'],
                        'actions' => [
                            'login',
                            'reg',
                            'forget',
                            'reset',
                            'forget-sent',
                            'reset-success',
                            'reg-activation',
                        ],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'controllers' => ['cabinet/networks'],
                        'actions' => [
                            'attach',
                            'auth',
                        ],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
                'denyCallback' => function($rule, $action) {
                    return Yii::$app->response->redirect(['cabinet/login']);
                },
            ],
        ];
    }
}
