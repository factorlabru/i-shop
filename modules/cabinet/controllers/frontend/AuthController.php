<?php

namespace app\modules\cabinet\controllers\frontend;

use app\modules\admin\models\backend\User;
use app\modules\cabinet\forms\RegForm;
use Yii;
use yii\base\Module;

use app\modules\cabinet\services\UserService;
use app\modules\cabinet\exceptions\NotFoundException;
use app\modules\cabinet\forms\ForgetForm;
use app\modules\cabinet\forms\LoginForm;
use app\modules\cabinet\forms\ResetForm;

use yii\web\NotFoundHttpException;


class AuthController extends BaseCabinetController
{
    private $user_service;

    public function __construct($id, Module $module, UserService $user_service, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->user_service = $user_service;
    }


    public function actionReg()
    {
        $form = new RegForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                //чтобы влючить верификацию email просто присваивайте статус User::UNACTIVE в этом методе
                //все остальное уже сделано.
                $user = $this->user_service->signUpByForm($form);

                if ($user->status == User::STATUS_ACTIVE) {
                    Yii::$app->user->login($user, 3600 * 24 * 30);
                    return $this->redirect(['cabinet/index']);
                }

                return $this->redirect(['reg-activation']);

            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                //Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $this->setMetaByModel(['name' => 'Регистрация',]);
        $this->setBreadcrumbs($this->view->h1_tag);

        return $this->render('reg', [
            'model' => $form
        ]);
    }

    public function actionRegActivation()
    {
        $this->setMetaByModel(['name' => 'Активация аккаунта',]);
        $this->setBreadcrumbs($this->view->h1_tag);

        return $this->render('reg_activation');
    }

    public function actionLogin()
    {
        $form = new LoginForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate() && $this->user_service->checkLoginForm($form)) {
            try {
                $user = $this->user_service->auth($form);
                Yii::$app->user->login($user, $form->rememberMe ? 3600 * 24 * 30 : 0);
                return $this->redirect(['cabinet/index']);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                //Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $this->setMetaByModel(['name' => 'Авторизация',]);
        $this->setBreadcrumbs($this->view->h1_tag);

        return $this->render('login', [
            'model' => $form
        ]);
    }

    public function actionForget()
    {
        $form = new ForgetForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate() && $this->user_service->checkForgetForm($form)) {
            try {
                $this->user_service->requestForget($form);
                $this->redirect(['forget-sent']);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                //Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $this->setMetaByModel(['name' => 'Восстановление пароля',]);
        $this->setBreadcrumbs($this->view->h1_tag);

        return $this->render('forget', [
            'model' => $form
        ]);
    }

    public function actionEmailVerification($token)
    {
        try {
            $user = $this->user_service->activateByEmailToken($token);

            //ну или можно сразу авторизировать:
            //Yii::$app->user->login($user, 3600 * 24 * 30);
            //return $this->redirect(['cabinet/index']);
        } catch (NotFoundException $e){
            Yii::$app->errorHandler->logException($e);
            throw new NotFoundHttpException('Токен не найден', 0, $e);
        }

        $this->setMetaByModel(['name' => 'E-mail подтвержден!',]);
        $this->setBreadcrumbs($this->view->h1_tag);

        return $this->render('email_verification_success');
    }

    public function actionReset($token)
    {
        //проверяем есть ли такой токен
        $this->user_service->hasUserWithResetToken($token) or $this->fail();

        $form = new ResetForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->user_service->resetPassword($form, $token);
                $this->redirect(['reset-success']);
            } catch (NotFoundException $e) {
                Yii::$app->errorHandler->logException($e);
                throw new NotFoundHttpException(null, 0, $e);
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                //Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $this->setMetaByModel(['name' => 'Установка нового пароля',]);
        $this->setBreadcrumbs($this->view->h1_tag);

        return $this->render('reset', [
            'model' => $form
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();
        $this->redirect('/');
    }

    public function actionForgetSent()
    {
        $this->setMetaByModel(['name' => 'Сброс пароля',]);
        $this->setBreadcrumbs($this->view->h1_tag);

        return $this->render('forget_sent');
    }

    public function actionResetSuccess()
    {
        $this->setMetaByModel(['name' => 'Пароль успешно установлен',]);
        $this->setBreadcrumbs($this->view->h1_tag);

        return $this->render('reset_success');
    }
}
