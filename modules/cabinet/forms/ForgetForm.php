<?php

namespace app\modules\cabinet\forms;

use yii\base\Model;

class ForgetForm extends Model
{
    public $username;

    public function rules()
    {
        return [
            [['username'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Логин',
        ];
    }

}