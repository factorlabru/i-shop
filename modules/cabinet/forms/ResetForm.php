<?php

namespace app\modules\cabinet\forms;

use yii\base\Model;

class ResetForm extends Model
{
    public $new_password;
    public $repeat_password;
    public $token;


    public function rules()
    {
        return [
            [['new_password'], 'required'],
            [['repeat_password'], 'required'],
            [['repeat_password'], 'compare',
                'operator' => '==',
                'compareAttribute' => 'new_password',
                'message' => 'Пароли не совпадают',
            ],
        ];
    }

}