<?php

namespace app\modules\cabinet\forms;

use app\modules\admin\models\backend\User;
use yii\base\Model;

class RegForm extends Model
{
    public $username;
    public $email;
    public $new_password;
    public $repeat_password;

    public $agree = 1;

    public function rules()
    {
        return [
            ['agree', 'required', 'requiredValue' => 1, 'message' => 'Примите условия'],

            [['username', 'email', 'repeat_password', 'new_password'], 'required'],

            [['email'], 'unique', 'targetAttribute' => 'email', 'targetClass' => User::className()],
            [['username'], 'unique', 'targetAttribute' => 'username', 'targetClass' => User::className()],

            [['repeat_password'], 'compare',
                'operator' => '==',
                'compareAttribute' => 'new_password',
                'message' => 'Пароли не совпадают',
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Логин',
            'new_password' => 'Пароль',
            'repeat_password' => 'Повтор пароля',

            'agree' => 'Принять условия',
        ];
    }
}