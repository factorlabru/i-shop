<?php

/**
 * @var $model User
 */

use app\modules\admin\models\backend\User;
use yii\helpers\Html;
use app\helpers\Common;
?>

<div style="margin:0;padding:0; ">
    <table style="border-collapse:collapse;" cellpadding="0" cellspacing="0" width="690px" align="center">
        <tbody>
        <tr bgcolor="#bdbbbc">
            <td align="left" valign="middle" style="padding-left:25px;padding-top: 20px;padding-bottom: 20px">
                <span style="font-size:24px;color:#000;"><b>Активация аккаунта на сайте: <?=$_SERVER['SERVER_NAME']?></b></span>
            </td>
        </tr>
        <tr bgcolor="#fffff">
            <td height="30" h>&nbsp;</td>
        </tr>
        <tr bgcolor="#fffff;">
            <td style="padding-left: 25px;">

                <table>
                    <tr>
                        <td style="padding-bottom: 15px">
                            <b style="text-transform: uppercase">Дата отправки письма:</b>
                        </td>
                        <td  style="padding-bottom: 15px">
                            <?=date('d-m-Y H:i:s');?>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-bottom: 15px">
                            <b style="text-transform: uppercase">Ссылка для активации аккаунта:</b>
                        </td>
                        <td>
                            <a target="_blank" rel="noopener" href="<?= $link ?>"><?= $link ?></a>
                        </td>
                    </tr>


                </table>
            </td>
        </tr>
        <tr bgcolor="#fffff">
            <td height="30">&nbsp;</td>
        </tr>
        <tr>
            <td style="padding-left: 25px; padding-top: 20px; padding-bottom: 5px">
                <span>Письмо отправлено с сайта <?=$_SERVER['SERVER_NAME']?>.</span>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 25px">
                <span>Использованный адрес &mdash; <?= $model->email ?></span>
            </td>
        </tr>
        <tr>
            <td height="30">&nbsp;</td>
        </tr>
        </tbody>
    </table>
</div>