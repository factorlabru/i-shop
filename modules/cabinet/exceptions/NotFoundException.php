<?php

namespace app\modules\cabinet\exceptions;

use DomainException;

class NotFoundException extends DomainException
{

}