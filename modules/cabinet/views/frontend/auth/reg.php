<?php

use yii\widgets\ActiveForm;
?>

<div class="login_form__wrapper">
    <div class="container">
        <div class="login_form_content">

            <h1><?= $this->h1_tag ?></h1>

            <div class="row">
                <div class="left_block">
                    <?php $form = ActiveForm::begin([
                        'id' => 'reg-form',
                        'enableClientValidation' => false,
                        'enableAjaxValidation' => false,
                        'fieldConfig' => [
                            'template' => "{input}{error}{label}",
                            'options' => [
                                'class' => 'form-group'
                                //'tag'=>false
                            ],
                            'errorOptions' => ['class' => 'error-message'],
                        ]
                    ]); ?>

                    <?= $form->field($model, 'username')
                        ->textInput([
                            "autofocus"=>true,
                        ])?>

                    <?= $form->field($model, 'email')
                        ->textInput()?>

                    <?= $form->field($model, 'new_password')
                        ->passwordInput([
                            "class"=>"form-control",
                            "id"=>"user_password"
                        ])?>

                    <?= $form->field($model, 'repeat_password')
                        ->passwordInput([
                            "class"=>"form-control",
                            "id"=>"user_password"
                        ])?>

                    <span>Я даю согласие на обработку моих <a target="_blank" href="/useragreement/">персональных данных</a>.</span>
                    <?= $form->field($model, 'agree')
                        ->checkbox()?>

                    <div class="form-group">
                        <input type="submit" class="js_submit_button" value="Регистрация" >
                    </div>

                    <?php ActiveForm::end(); ?>

                    <?= yii\authclient\widgets\AuthChoice::widget([
                        'baseAuthUrl' => ['networks/auth']
                    ]); ?>

                </div>

                <?=$this->render('_sidebar');?>

            </div>
        </div>
    </div>
</div>