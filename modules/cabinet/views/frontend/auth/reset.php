<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;
use yii\helpers\Url;
use app\helpers\Common;
?>

<section id="content" class="page-content page-cms page-cms-1">

    <h1><?= $this->h1_tag ?></h1>


    <div class="panel-body">
        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'enableClientValidation' => false,
            'enableAjaxValidation' => false,
            'fieldConfig' => [
                'template' => '{input}',
            ],
        ]); ?>

        <div class="form-group field-loginform-username required">
            <div>
                <?= $form->field($model, 'new_password')
                    ->textInput([
                        "autofocus"=>true,
                        "class"=>"form-control",
                        'placeholder'=>'Пароль',
                        "size"=>"20",
                    ])?>
            </div>
            <div class="errors"><?= Html::error($model, 'new_password'); ?></div>
        </div>

        <div class="form-group field-loginform-username required">
            <div>
                <?= $form->field($model, 'repeat_password')
                    ->textInput([
                        "autofocus"=>true,
                        "class"=>"form-control",
                        'placeholder'=>'Повтор пароля',
                        "size"=>"20",
                    ])?>
            </div>
            <div class="errors"><?= Html::error($model, 'repeat_password'); ?></div>
        </div>


        <div class="col-md-6 no-right-padding">
            <button type="submit" class="btn btn-lg btn-primary btn-block">Продолжить</button>
        </div>
        <div class="clearfix"></div>

        <?php ActiveForm::end(); ?>
    </div>

</section>