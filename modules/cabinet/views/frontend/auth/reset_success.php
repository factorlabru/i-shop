
<main class="inner_page">
    <div class="container">
        <div class="title_block">
            <h1><?= $this->h1_tag ?></h1>
        </div>

        <div class="inner_content__wrapper row">

            <div class="inner_content_block col-sm-12 col-xs-12">

                <div class="lead_block">

                    <p class="lead">
                        Вы можете войти в свой аккаунт по ссылке:
                        <a href="<?= \yii\helpers\Url::to(['login']) ?>" class="btn btn-primary">Вход</a>
                    </p>

                </div>

            </div>

        </div>


    </div>

</main>

