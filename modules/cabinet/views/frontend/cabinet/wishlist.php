<section id="content" class="page-content page-cms page-cms-1">
    <div class="row">
        <div class="col-md-8">
            <header class="page-header">
                <h1><?= $this->h1_tag ?></h1>
            </header>

            <div class="row">
                <div class="col-md-12">

                    <?php foreach($products as $one) { ?>
                        <div class="row js_product_container" data-product_id="<?= $one->id ?>">
                            <a href="<?= $one->getUrl() ?>">
                                <img src="<?= $one->mainImg() ?>" alt="">
                                <h2><?= $one->name ?></h2>
                            </a>
                            <a href="#" class="js_delete_from_wishlist">Удалит из избранного</a>
                        </div>
                    <?php } ?>

                </div>
            </div>

        </div>

        <?=$this->render('_sidebar');?>

    </div>
</section>