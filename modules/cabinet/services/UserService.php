<?php

namespace app\modules\cabinet\services;

use app\modules\cabinet\exceptions\NotFoundException;
use app\modules\cabinet\forms\ForgetForm;
use app\modules\cabinet\forms\LoginForm;
use app\modules\cabinet\forms\RegForm;
use app\modules\cabinet\forms\ResetForm;
use app\modules\admin\models\backend\User;
use app\modules\cabinet\repositories\UserRepository;
use Yii;
use yii\base\Object;
use yii\helpers\Url;

class UserService
{
    private $user_repository;

    public function __construct(UserRepository $user_repository)
    {
        $this->user_repository = $user_repository;
    }

    public function checkLoginForm(LoginForm $form)
    {
        /** @var User $user */
        $user = $this->user_repository->findByUsernameOrEmail($form->username);
        if (!$user || !$user->validatePassword($form->password)) {
            $form->addError('password', 'Неправильный логин или пароль');
            return false;
        }

        return true;
    }

    public function checkForgetForm(ForgetForm $form)
    {
        /** @var User $user */
        $user = $this->user_repository->findByUsernameOrEmail($form->username);
        if (!$user) {
            $form->addError('username', 'Пользователь не найден');
        }

        // чтобы запросы не отправляли слишком часто, проверяем есть ли у пользователя валидный токен
        if ($user->hasValidResetToken()) {
            //$form->addError('username', 'Запрос на восстановление уже был отправлен. Проверьте папку спам.');
        }

        return !$form->hasErrors();
    }

    public function signUpByForm(RegForm $form)
    {
        $user = new User();
        $user->created_at = date('Y-m-d H:i:s');
        $user->role_auth = User::ROLE_CLIENT;

        $user->status = User::STATUS_ACTIVE;
        $user->email_verification_token = Yii::$app->security->generateRandomString();

        $user->setAttributes($form->getAttributes());
        $user->setPassword($form->new_password);
        $user->generateAuthKey();

        $this->user_repository->save($user);
        $this->applyRole($user);

        if($user->status == User::STATUS_UNACTIVE){
            $mail_from = 'noreply@'.$_SERVER['SERVER_NAME'];
            $link = Url::to([
                'auth/email-verification',
                'token' => $user->email_verification_token
            ], 1);

            Yii::$app->mailer->compose('@modules/cabinet/mails/email-verification', [
                'model' => $user,
                'link' => $link,
            ])
                ->setFrom($mail_from)
                ->setTo($user->email)
                ->setSubject('Активация аккаунта.' . ' ' . $_SERVER['SERVER_NAME'])
                ->send();
        }

        return $user;
    }

    public function activateByEmailToken($token)
    {
        /** @var User $user */
        if(!$user = $this->user_repository->findByEmailVerificationToken($token)){
            throw new NotFoundException();
        }

        $user->status = User::STATUS_ACTIVE;
        $this->user_repository->save($user);

        return $user;
    }

    public function auth(LoginForm $form)
    {
        /** @var User $user */
        $user = $this->user_repository->findByUsernameOrEmail($form->username);

        if (!$user || !$user->validatePassword($form->password)) {
            throw new \DomainException('Undefined user or password.');
        }

        return $user;
    }

    public function requestForget(ForgetForm $form)
    {
        /** @var User $user */
        $user = $this->user_repository->findByUsernameOrEmail($form->username);
        $user->generatePasswordResetToken();
        $this->user_repository->save($user);

        $mail_from = 'noreply@'.$_SERVER['SERVER_NAME'];
        $link = Url::to([
            'auth/reset',
            'token' => $user->password_reset_token
        ], 1);

        Yii::$app->mailer->compose('@modules/cabinet/mails/forget', [
            'model' => $user,
            'link' => $link,
        ])
            ->setFrom($mail_from)
            ->setTo($user->email)
            ->setSubject('Восстановление пароля.' . ' ' . $_SERVER['SERVER_NAME'])
            ->send();

        return $user;
    }


    public function resetPassword(ResetForm $form, $token)
    {
        /** @var User $user */
        $user = $this->user_repository->getByPasswordResetToken($token);
        $user->setPassword($form->new_password);
        $user->removePasswordResetToken();
        $this->user_repository->save($user);

        return $user;
    }


    public function hasUserWithResetToken($token)
    {
        return !!$this->user_repository->findByPasswordResetToken($token);
    }


    /**
     * @param $network
     * @param $identity
     * @return User|mixed
     */
    public function authNetwork($network, $identity)
    {
        //если пользователь уже есть возвращаем его
        /** @var User $user */
        $user = $this->user_repository->findByNetworkIdentity($network, $identity);
        if ($user) {
            return $user;
        }

        //если пользователя нет, создаем  его, создаем соц.сеть, привязываем и возвращаем.
        $user = User::signUpByNetwork($network, $identity); // регистрация пользователя
        $this->user_repository->save($user);
        $this->applyRole($user);


        return $user;
    }

    public function attach($id, $network, $identity)
    {
        // если сеть уже присоединена к пользователю
        if ($this->user_repository->findByNetworkIdentity($network, $identity)) {
            throw new \DomainException('Network is already signed up.');
        }
        $user = $this->user_repository->get($id);

        $user->attachNetwork($network, $identity);
        $this->user_repository->save($user);
    }

    public function applyRole(User $user)
    {
        Yii::$app->authManager->revokeAll($user->id);
        $role = Yii::$app->authManager->getRole($user->role_auth);
        if ($role) {
            Yii::$app->authManager->assign($role, $user->id);
        }
    }

}