<?php

namespace app\modules\cabinet\repositories;

use app\modules\admin\models\backend\User;
use app\modules\cabinet\exceptions\NotFoundException;
use yii\db\ActiveRecord;


class UserRepository
{

    public function findByUsernameOrEmail($value)
    {
        return User::find()->andWhere(['or', ['username' => $value], ['email' => $value]])->one();
    }

    public function get($id)
    {
        return $this->getBy(['id' => $id]);
    }

    public function findByNetworkIdentity($network, $identity)
    {
        return User::find()
            ->joinWith('networks n')
            ->andWhere([
                'n.network' => $network,
                'n.identity' => $identity
            ])
            ->one();
    }

    public function getByEmail($email)
    {
        return $this->getBy(['email' => $email]);
    }

    public function getByPasswordResetToken($token)
    {
        if(!$user = $this->findByPasswordResetToken($token)){
            throw new NotFoundException();
        }
        return $user;
    }

    public function findByPasswordResetToken($token)
    {
        return User::findByPasswordResetToken($token);
    }

    public function findByEmailVerificationToken($token)
    {
        return User::findOne([
            'email_verification_token' => $token,
        ]);
    }

    public function existsByPasswordResetToken($token)
    {
        return (bool) User::findByPasswordResetToken($token);
    }

    /**
     * @param array $condition
     * @return array|null|User
     */
    private function getBy(array $condition)
    {
        if (!$user = User::find()->andWhere($condition)->limit(1)->one()) {
            throw new NotFoundException('User not found.');
        }
        return $user;
    }

    public function save(ActiveRecord $record)
    {
        if (!$record->save()) {
            $errors = implode(', ', $record->getFirstErrors());
            throw new \RuntimeException('Saving error. ' . $errors);
        }
    }

    public function remove(ActiveRecord $record)
    {
        if (!$record->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }
}