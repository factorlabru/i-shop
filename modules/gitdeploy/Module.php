<?php

namespace app\modules\gitdeploy;

/**
 * gitdeploy module definition class
 */
class Module extends \yii\base\Module
{
    public $module_name = 'Git deploy';

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\gitdeploy\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        \Yii::configure($this, require(__DIR__ . '/config.php'));
        // custom initialization code goes here
    }
}
