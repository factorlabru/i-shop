<?php

namespace app\modules\gitdeploy\controllers\frontend;

use app\components\Controller;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;

/**
 * Default controller for the `gitdeploy` module
 */
class DefaultController extends Controller
{
    public $repo_dir;

    public $web_root_dir;

    /**
     * Токен задается в gitlab, в разделе integrations
     * @var string
     */
    public $secret_token = 'secrettoken';

    public $git_bin_path = 'git';

    /**
     * @var bool
     */
    public $logs = true;

    /**
     * Место, куда пищутся логи.
     *
     * @var string
     */
    public $log_dir = '/web/content/';

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;

        return parent::beforeAction($action);
    }

    public function __construct($id, $module, $config = [])
    {
        $this->repo_dir = $_SERVER['DOCUMENT_ROOT'];
        $this->web_root_dir = $_SERVER['DOCUMENT_ROOT'];

        parent::__construct($id, $module, $config);
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        //Токен приходит с Gitlab
        if(isset($_SERVER['HTTP_X_GITLAB_TOKEN']) && $_SERVER['HTTP_X_GITLAB_TOKEN'] == $this->secret_token) {
            $repo_dir = $this->repo_dir;
            $web_root_dir = $this->web_root_dir;
            $git_bin_path = $this->git_bin_path;

            $branch = 'master';
            $payload = '';
            if(!empty($_POST['payload'])) {
                $payload = json_decode($_POST['payload']);
            }
            if (empty($payload->commits)){
                // When merging and pushing to bitbucket, the commits array will be empty.
                // In this case there is no way to know what branch was pushed to, so we will do an update.
                $update = true;
            } else {
                foreach ($payload->commits as $commit) {
                    $branch = $commit->branch;
                    if ($branch === 'production' || isset($commit->branches) && in_array('production', $commit->branches)) {
                        $update =	true;
                        break;
                    }
                }
            }
            if ($update) {
                // Do a git checkout to the web root
                exec('cd ' . $repo_dir . ' && ' . $git_bin_path . ' fetch');
                exec('cd ' . $repo_dir . ' && GIT_WORK_TREE=' . $web_root_dir . ' ' . $git_bin_path . ' checkout -f');
                exec('cd ' . $repo_dir . ' && ' . $git_bin_path . ' pull');
                // Log the deployment
                $commit_hash = shell_exec('cd ' . $repo_dir . ' && ' . $git_bin_path . ' rev-parse --short HEAD');

                if($this->logs) {
                    file_put_contents($_SERVER['DOCUMENT_ROOT'].$this->log_dir.'deploy.log', date('m/d/Y h:i:s a') . " Deployed branch: " . $branch  . " Commit: " . $commit_hash . "\n", FILE_APPEND);
                }
            }
        } else {
            die();
        }
    }
}
