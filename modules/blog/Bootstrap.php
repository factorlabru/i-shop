<?php
namespace app\modules\blog;
use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules(
            [
                '<path:([\w_\/-]+\/)?(blog)>/tag/<tag_url:[\w\-]+>' => 'blog/post/tag',
                '<path:([\w_\/-]+\/)?(blog)>' => 'blog/post/index',
                [
                    'pattern' => '<path:([\w_\/-]+\/)?(blog)>/<url_alias:[-a-zA-Z0-9_]+>',
                    'route' => 'blog/post/view',
                    'suffix' => '.html',
                ],
            ]
        );
    }
}