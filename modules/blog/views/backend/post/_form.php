<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
//use app\modules\user\models\User;
use app\modules\blog\models\Tag;
use app\modules\blog\models\Category;
use yii\widgets\ActiveForm;
use app\widgets\CKEditor as CKEditor;
use app\modules\admin\widgets\ImageRender;
use yii\bootstrap\Alert;
?>

<div class="post-form content">

    <?php $form = ActiveForm::begin(); ?>

    <?php if(Yii::$app->session->getFlash('save')) {
        echo Alert::widget([
            'options' => ['class' => 'alert-success',],
            'body' => Yii::$app->session->getFlash('save'),
        ]);
    }?>

    <?= $form->field($model, 'vis')->checkbox(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= \app\modules\admin\widgets\UrlTranslate::widget([
        'source_id' => Html::getInputId($model, 'name'),
        'target_id' => Html::getInputId($model, 'url_alias'),
    ]); ?>

    <?= $form->field($model, 'url_alias')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'annotation')->textarea(['rows' => 6]) ?>

    <?= \app\modules\admin\widgets\CKEditorWidget::widget([
        'form'=>$form,
        'model'=>$model,
        'attr'=>'content',
        'textarea_id'=>'post-content',
        'gallery_widget'=>false,
    ]); ?>

    <?= $form->field($model, 'tags')->checkboxList(ArrayHelper::map(Tag::find()->all(), 'id', 'name'))->label('Теги') ?>

    <hr>
    <?= $form->field($model, 'img')->fileInput(['accept' => 'image/*']) ?>

    <?php if($model->img) {
        echo ImageRender::widget([
            'id'=>$model->id,
            'attribute'=>'img',
            'img_url'=>$model->img(),
        ]);
    }?>

    <hr>

    <?= $this->render('@admin_layouts/chunks/_meta_form', [
        'model' => $model,
        'form' => $form,
    ]); ?>

    <?= $this->render('@admin_layouts/chunks/_save_buttons', [
        'model' => $model,
        'form' => $form,
    ]); ?>

    <?php ActiveForm::end(); ?>

</div>
