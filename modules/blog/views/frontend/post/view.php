<?php
use app\modules\blog\widgets\LastPosts;
use app\modules\blog\widgets\Tags;
?>

<div class="col-md-8">
    <h1><?=\app\helpers\Common::pageTitle($post)?></h1>

    <div>
        <?=$post->content;?>
    </div>

    <?=LastPosts::widget()?>
</div>

<div class="col-md-4">
    <?=Tags::widget()?>
</div>