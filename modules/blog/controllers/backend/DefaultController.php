<?php

namespace app\modules\blog\controllers\backend;

use app\modules\admin\components\AdminController;

/**
 * Default controller for the `blog` module
 */
class DefaultController extends AdminController
{

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $this->redirect('/admin/blog/post');
        return $this->render('index');
    }
}
