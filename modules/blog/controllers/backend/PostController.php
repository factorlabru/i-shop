<?php

namespace app\modules\blog\controllers\backend;

use app\modules\admin\actions\AjaxDeleteGridItemsAction;
use app\modules\admin\actions\AjaxEditableAction;
use app\modules\admin\actions\AjaxMainDeleteImageAction;
use app\modules\blog\models\PostSearch;
use app\modules\blog\models\Tag;
use app\modules\blog\models\PostTags;
use yii\web\Controller;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use app\modules\blog\models\Post;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use app\modules\admin\components\AdminController;


/**
 * Post controller for the `post` module
 */
class PostController extends AdminController
{
    public $layout = '@app/modules/admin/views/backend/default/main.php';


    public function actions()
    {
        $model_name = \app\modules\blog\models\Post::className();

        return [
            'ajax-editable' => [
                'class' => AjaxEditableAction::className(),
                'model' => $model_name,
            ],
            'ajax-main-delete-image' => [
                'class' => AjaxMainDeleteImageAction::className(),
                'model' => $model_name,
                'behavior_name' => 'image',
            ],
            'ajax-delete-grid-items' => [
                'class' => AjaxDeleteGridItemsAction::className(),
                'model' => $model_name,
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $this->setbcAction('Просмотр постов');
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new Post();
        $model->scenario = 'create';
        $model->vis = 1;

        if($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->getSession()->setFlash('save', 'Данные сохранены');

            return $this->moduleRedirect($model->id);
        } else {


            return $this->render('create', ['model'=>$model]);
        }
    }

    public function actionUpdate($id)
    {
        $this->setbcAction('Редактирование постов');

        $model = $this->findModel($id);
        $model->scenario = 'update';

        if($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->getSession()->setFlash('save', 'Данные сохранены');

            return $this->moduleRedirect($model->id);
        }

        return $this->render('update', ['model'=>$model]);
    }

    public function actionDelete($id)
    {
        $this->setbcAction('Удаление постов');

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
