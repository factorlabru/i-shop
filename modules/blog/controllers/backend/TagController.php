<?php

namespace app\modules\blog\controllers\backend;

use app\modules\admin\actions\AjaxDeleteGridItemsAction;
use app\modules\admin\actions\AjaxEditableAction;
use Yii;
use app\modules\blog\models\Tag;
use app\modules\blog\models\TagSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\admin\controllers\console\BackController;
use app\modules\admin\components\AdminController;

/**
 * TagController implements the CRUD actions for Tag model.
 */
class TagController extends AdminController
{
    public $layout = '@app/modules/admin/views/backend/default/main.php';

    public function actions()
    {
        $model_name = \app\modules\blog\models\Tag::className();

        return [
            'ajax-editable' => [
                'class' => AjaxEditableAction::className(),
                'model' => $model_name,
            ],
            'ajax-delete-grid-items' => [
                'class' => AjaxDeleteGridItemsAction::className(),
                'model' => $model_name,
            ],
        ];

    }

    /**
     * Lists all Tag models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TagSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tag model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tag model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->setbcAction('Создание тегов');

        $model = new Tag();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->getSession()->setFlash('save', 'Данные сохранены');

            return $this->moduleRedirect($model->id);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Tag model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->setbcAction('Редактирование тегов');

        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->getSession()->setFlash('save', 'Данные сохранены');

            return $this->moduleRedirect($model->id);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Tag model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->setbcAction('Удаление тегов');

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tag model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tag the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tag::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
