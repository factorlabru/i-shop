<?php

namespace app\modules\blog\models;

use app\behaviors\UploadImageBehavior;
use app\modules\section\models\Section;
use app\modules\admin\models\backend\User;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use Yii;

/**
 * This is the model class for table "tbl_posts".
 *
 * @mixin UploadImageBehavior
 *
 * @property integer $id
 * @property string $name
 * @property string $url_alias
 * @property string $annotation
 * @property string $content
 * @property string $author
 * @property integer $user_id
 * @property integer $vis
 * @property string $img
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $h1_tag
 * @property string $created_at
 * @property string $updated_at
 */
class Post extends \yii\db\ActiveRecord
{
    /** @var Section */
    public static $_blog_section;

    public function behaviors()
    {
        return [
            'log' => [
                'class' => 'app\behaviors\LogBehavior',
            ],
            'slug' => [
                'class' => 'app\behaviors\Slug',
                'in_attribute' => 'name',
                'out_attribute' => 'url_alias',
                'translit' => true
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'saveRelations' => [
                'class'     => SaveRelationsBehavior::className(),
                'relations' => ['tags']
            ],
            'image' => [
                'class' => UploadImageBehavior::className(),
                'attribute' => 'img',
                'scenarios' => ['create', 'update'],
                'extensions' => 'png, jpg, jpeg, gif',
                'path' => '@webroot/content/blog',
                'url' => '@web/content/blog',
                'generate_new_name'=>true,
                'thumbs' => [
                    'thumb' => [
                        'width' => 590,
                        'height'=>410,
                        'quality' => 90,
                        'mode'=>\Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND
                    ],
                ],
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_posts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'trim'],
            [['annotation', 'content'], 'string'],
            [['vis', 'user_id'], 'integer'],
            [['tags'], 'safe'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'url_alias', 'author', 'meta_title', 'meta_description', 'meta_keywords', 'h1_tag'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'url_alias' => 'Url Alias',
            'annotation' => 'Аннотация',
            'content' => 'Контент',
            'author' => 'Author',
            'vis' => 'Показывать',
            'img' => 'Картинка',
            'user_id' => 'Пользователь',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
            'h1_tag' => 'H1 Tag',
            'created_at' => 'Дата создания',
            'updated_at' => 'Updated At',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getTags()
    {
        return $this->hasMany(
            Tag::className(),
            ['id' => 'tag_id']
        )->via('selectedTagIds');
    }


    public function getSelectedTagIds()
    {
        return $this->hasMany(PostTags::className(), ['post_id' => 'id']);
    }

    public static function getPostTags($post_id=2)
    {
        $query = "SELECT
                        t.name,
                        t.url_alias
                  FROM
                        tbl_tags t
                  INNER JOIN
                        tbl_post_tags pt
                  ON
                       (tag_id=t.id)
                  WHERE
                      pt.post_id = :post_id
                  ";
        $tags = Yii::$app->db->createCommand($query)
            ->bindValue(':post_id', $post_id)
            ->queryAll();

        return $tags;
    }

    /**
     * @return null|Section
     */
    public static function getBlogSection()
    {
        if(!self::$_blog_section){
            self::$_blog_section = Section::find()->where([
                'url_alias' => 'blog'
            ])->one();
        }

        return self::$_blog_section;
    }

    public function getUrl()
    {
        $blog_section = self::getBlogSection();

        $url = \yii\helpers\Url::toRoute([
            '/blog/post/view',
            'path' => $blog_section->getUrlPath(),
            'url_alias'=>$this->url_alias
        ]);

        return $url;
    }
}
