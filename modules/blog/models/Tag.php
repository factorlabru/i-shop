<?php

namespace app\modules\blog\models;
use app\modules\blog\models\Post;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "tbl_tags".
 *
 * @property integer $id
 * @property string $name
 */
class Tag extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            'log' => [
                'class' => 'app\behaviors\LogBehavior',
            ],
            'slug' => [
                'class' => 'app\behaviors\Slug',
                'in_attribute' => 'name',
                'out_attribute' => 'url_alias',
                'translit' => true
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_tags';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'url_alias'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }

    public function getPosts()
    {
        /*return $this->hasMany(Tag::className(), ['id' => 'tag_id'])
            ->viaTable('tbl_post_tags', ['post_id' => 'id']);*/

        return $this->hasMany(
            Post::className(),
            ['id' => 'post_id']
        )->viaTable(
            'tbl_post_tags',
            ['tag_id' => 'id']
        );
    }

    public function getSelectedTagIds()
    {
        return $this->hasMany(PostTags::className(), ['tag_id' => 'id']);
    }


    public function getUrl()
    {
        $blog_section = Post::getBlogSection();

        return \yii\helpers\Url::to([
            '/blog/post/tag',
            'path' => $blog_section->getUrlPath(),
            'tag_url'=>$this->url_alias
        ]);
    }
}
