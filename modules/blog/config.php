<?php
return [
    'params' => [
        '_items' => [
            [
                'icon' => '',
                'label' => 'Посты',
                'url' => '/admin/blog/post',
            ],
            [
                'icon' => '',
                'label' => 'Теги',
                'url' => '/admin/blog/tag',
            ],
        ]
    ],
];