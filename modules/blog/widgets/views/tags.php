<?php
use yii\helpers\Html;
?>

<h2>Теги</h2>

<?php if($tags) {
    echo Html::beginTag('ul');
    foreach($tags as $tag) {
        echo Html::beginTag('li');
        echo Html::a($tag->name, $tag->getUrl());
        echo Html::endTag('li');
    }
    echo Html::endTag('ul');
}?>