<?php
namespace app\modules\blog\widgets;

use yii\base\Widget;
use app\modules\blog\models\Tag;

/**
 * Виджет выводит теги.
 *
 * Class LastPostsWidget
 * @package app\modules\blog\widgets
 */
class Tags extends Widget
{
    /**
     * @var string
     */
    public $order = 'name';

    public function run()
    {
        $tags = Tag::find()->orderBy($this->order)->all();

        return $this->render('tags', ['tags'=>$tags]);
    }
}