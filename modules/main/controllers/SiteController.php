<?php
namespace app\modules\main\controllers;

use app\components\Controller;
use DateTime;
use Yii;
use yii\web\View;

class SiteController extends Controller
{
    public function actions()
    {

        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $this->setAllMeta(['title'=>\Yii::$app->params['mainpage_title']]);

        return $this->render('index');
    }
}