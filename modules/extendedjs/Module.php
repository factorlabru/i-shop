<?php

namespace app\modules\extendedjs;

/**
 * extendedjs module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\extendedsjs\controllers';

    public $module_name = 'Расширенные скрипты';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        //\Yii::configure($this, require(__DIR__ . '/config.php'));
        // custom initialization code goes here
    }
}
