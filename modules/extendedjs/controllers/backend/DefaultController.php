<?php

namespace app\modules\extendedjs\controllers\backend;

use yii\web\Controller;
use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\admin\components\AdminController;

/**
 * Default controller for the `extendedjs` module
 */
class DefaultController extends AdminController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $this->setbcAction('Расширенные скрипты');

        $file_path = Yii::getAlias('@app/web/ext/extendedjs.txt');

        if (!file_exists($file_path)) {
            $fp = fopen($file_path, "w");
            fclose ($fp);
        }

        $file = file_get_contents($file_path);

        if(Yii::$app->request->post('extended_js')) {
            $fp = fopen($file_path, "w");
            $data = trim(Yii::$app->request->post('extended_js'));
            fwrite($fp, $data);
            fclose($fp);

            \Yii::$app->getSession()->setFlash('save', 'Данные сохранены.');

            return $this->redirect('/admin/extendedjs/');
        }

        return $this->render('index', ['file'=>$file]);
    }
}
