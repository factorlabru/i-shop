<?php
\app\modules\admin\assets\CodemirrorAsset::register($this);
use yii\helpers\Html;
use yii\bootstrap\Alert;

$this->title = 'Расширенные скрипты';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="content">
    <legend>
        <a href="/ext/extendedjs.txt" target="_blank">/ext/extendedjs.txt</a>
    </legend>

    <?php if(isset($file)) {?>

        <?php if(Yii::$app->session->getFlash('save')) {
            echo Alert::widget([
                'options' => ['class' => 'alert-success',],
                'body' => Yii::$app->session->getFlash('save'),
            ]);
        }?>

        <?= Html::beginForm() ?>
        <fieldset>
        <textarea id="code" style="height: 350px; width: 100%;" name="extended_js"><?=$file?></textarea>
        </fieldset>
        <hr/>
        <div class="control-group">
            <input name="apply" class="btn btn-primary" type="submit" value="Сохранить">
        </div>
        <?= Html::endForm() ?>

    <?php } else {
        echo Html::tag("p", 'Файл extended.css отсутствует.');
    }

    $script = <<< JS
     CodeMirror.commands.autocomplete = function(cm) {
        cm.showHint({hint: CodeMirror.hint.anyword});
    }
    var editor = CodeMirror.fromTextArea(document.getElementById("code"), {
        lineNumbers: true,
        extraKeys: {"Ctrl-Space": "autocomplete"}
    });
JS;
    $this->registerJs($script, yii\web\View::POS_READY);
    ?>

</div>
