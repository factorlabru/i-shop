<?php

namespace app\modules\param\models\backend;

use Yii;
use app\modules\param\models\backend\Param;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%param}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $value
 * @property string $title
 */
class SearchParam extends Param
{
    public function search($params)
    {
        $query = Param::find();
        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 25,
            ],
            'sort' => [
                'defaultOrder' => [
                    'priority' => SORT_ASC,
                ]
            ],
        ]);


        if($this->load($params)) {

            // adjust the query by adding the filters

            $query->andFilterWhere(['like', 'name', $this->name])
                ->andFilterWhere(['like', 'value', $this->value])
                ->andFilterWhere(['like', 'title', $this->title]);

        }

        return $provider;
    }
}
