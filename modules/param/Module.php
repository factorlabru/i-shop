<?php

namespace app\modules\param;

use Yii;
/**
 * param module definition class
 */
class Module extends \app\modules\admin\Module
{
    public $module_name = 'Настройки';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        \Yii::configure($this, require(__DIR__ . '/config.php'));

        // custom initialization code goes here
    }
}
