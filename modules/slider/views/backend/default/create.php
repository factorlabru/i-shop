<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\testimonials\models\Testimonial */

$this->title = 'Создание слайдера';
$this->params['breadcrumbs'][] = ['label' => 'Слайдер', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="testimonial-create content">
    <div class="box">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>