<?php

use app\modules\admin\helpers\XEditableHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;
use yii\widgets\Pjax;
use app\modules\admin\widgets\grid\LinkColumn;
use mcms\xeditable\XEditableColumn;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\Slider\models\SliderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//\Yii::$app->getModule('slider');
$this->title = 'Слайдер';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="Slider-index content">
    <div class="box">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <div>
            <?= Html::a('<i class="glyphicon glyphicon-plus"></i> Добавить запись', ['create'], ['class' => 'btn btn-success']) ?>
            <?php echo\app\modules\admin\widgets\GridMultiDelete::widget()?>
        </div>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'class' => \app\modules\admin\widgets\grid\sortable\SortableColumn::className(),
                    'url' => \yii\helpers\Url::toRoute(['ajax-sorting'])
                ],
                ['class' => 'yii\grid\CheckboxColumn'],
                //['class' => 'yii\grid\SerialColumn'],
                [
                    'class' => LinkColumn::className(),
                    'attribute' => 'name',
                ],

                //XEditableHelper::buildEditPriority(),

                XEditableHelper::buildYesNo('vis'),

                ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}{link}'],
            ],
        ]); ?>
    </div>
</div>