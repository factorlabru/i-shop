<?php

namespace app\modules\slider\controllers\backend;

use app\modules\admin\actions\AjaxDeleteGridItemsAction;
use app\modules\admin\actions\AjaxEditableAction;
use app\modules\admin\actions\AjaxMainDeleteImageAction;

use app\modules\admin\widgets\grid\sortable\AjaxGridSortableAction;
use Yii;
use app\modules\slider\models\Slider;
use app\modules\slider\models\SliderSearch;
use yii\web\NotFoundHttpException;
use app\modules\admin\components\AdminController;

/**
 * DefaultController implements the CRUD actions for Slider model.
 */
class DefaultController extends AdminController
{

    public function actions()
    {
        $model_name = Slider::className();

        return [
            'ajax-editable' => [
                'class' => AjaxEditableAction::className(),
                'model' => $model_name,
            ],
            'ajax-main-delete-image' => [
                'class' => AjaxMainDeleteImageAction::className(),
                'model' => $model_name,
                'behavior_name' => 'image',
            ],
            'ajax-delete-grid-items' => [
                'class' => AjaxDeleteGridItemsAction::className(),
                'model' => $model_name,
            ],
            'ajax-sorting' => [
                'class' =>  AjaxGridSortableAction::className(),
                'modelClass' => $model_name,
            ],
        ];
    }

    /**
     * Lists all Slider models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SliderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new Slider();
        $model->scenario = 'create';
        $model->vis = 1;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->moduleRedirect($model->id);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Slider model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'update';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->moduleRedirect($model->id);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Slider model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Slider model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Slider the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Slider::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
