<?php

namespace app\modules\slider\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "tbl_slider".
 *
 * @property integer $id
 * @property integer $vis
 * @property string $name
 * @property string $title
 * @property string $description
 * @property string $img
 * @property string $link
 * @property string $button_text
 * @property integer $priority
 * @property string $updated_at
 * @property string $created_at
 */
class Slider extends \yii\db\ActiveRecord
{
    /**
     * @inheritdock
     */
    public static function tableName()
    {
        return 'tbl_slider';
    }

    public function behaviors()
    {
        return [
            'log' => [
                'class' => 'app\behaviors\LogBehavior',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'PriorityBehavior' => [
                'class' => 'app\behaviors\PriorityBehavior',
            ],
            'image' => [
                'class' => \app\behaviors\UploadImageBehavior::className(),
                'attribute' => 'img',
                'scenarios' => ['create', 'update'],
                'extensions' => 'png, jpg, jpeg, gif',
                'path' => '@webroot/content/slider',
                'url' => '@web/content/slider',
                'generate_new_name'=>true,
                'thumbs' => [
                    'thumb' => [
                        'width' => 1290,
                        'height'=>670,
                        'quality' => 90,
                        'mode'=>\Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND
                    ],
                ],
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vis', 'priority'], 'integer'],
            [['name'], 'required'],
            [['description'], 'string'],
            [['updated_at', 'created_at'], 'safe'],
            [['name', 'title', 'link', 'button_text'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'vis' => 'Показывать',
            'name' => 'Название слайда',
            'title' => 'Заголовок',
            'link' => 'Ссылка',
            'button_text' => 'Текст на кнопке',
            'description' => 'Описание',
            'img' => 'Картинка слайда',
            'priority' => 'Приоритет',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }
}
