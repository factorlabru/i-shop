<?php

namespace app\modules\block\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "{{%block}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $content
 * @property string $page_url
 * @property integer $vis
 * @property integer $wysiwyg_vis
 * @property string $updated_at
 * @property string $created_at
 */
class Block extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            'log' => [
                'class' => 'app\behaviors\LogBehavior',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%block}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'content'], 'required'],
            [['name'], 'trim'],
            [['content'], 'string'],
            [['vis', 'wysiwyg_vis'], 'integer'],
            [['name', 'page_url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'content' => 'Текст',
            'page_url' => 'Адрес страницы',
            'vis' => 'Показывать',
            'wysiwyg_vis' => 'Показывать визуальный редактор',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

    public function getEditUrl()
    {
        return '/admin/block/' . $this->id . '/update/';
    }

    public static function getCreateUrl()
    {
        return '/admin/block/default/create/';
    }
}
