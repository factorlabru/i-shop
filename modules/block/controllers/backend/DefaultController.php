<?php

namespace app\modules\block\controllers\backend;

use app\modules\admin\actions\AjaxDeleteGridItemsAction;
use app\modules\admin\actions\AjaxEditableAction;
use Yii;
use app\modules\block\models\Block;
use app\modules\block\models\SearchBlock;
use app\modules\admin\controllers\console\BackController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\admin\components\AdminController;

/**
 * DefaultController implements the CRUD actions for Block model.
 */
class DefaultController extends AdminController
{

    public function actions()
    {
        $model_name = \app\modules\block\models\Block::className();

        return [
            'ajax-editable' => [
                'class' => AjaxEditableAction::className(),
                'model' => $model_name,
            ],
            'ajax-delete-grid-items' => [
                'class' => AjaxDeleteGridItemsAction::className(),
                'model' => $model_name,
            ],
        ];
    }

    /**
     * Lists all Block models.
     * @return mixed
     */
    public function actionIndex()
    {
        $this->setbcAction('Список блоков');
        $searchModel = new SearchBlock();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Block model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $this->setbcAction('Создание блока');
        $model = new Block();
        $model->vis = 1;
        $model->wysiwyg_vis = 1;
        if(Yii::$app->request->get('wysiwyg') && Yii::$app->request->get('wysiwyg') == 'off') {
            $model->wysiwyg_vis = 0;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->moduleRedirect($model->id);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Block model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $this->setbcAction('Изменение блока');
        $model = $this->findModel($id);
        if(Yii::$app->request->get('wysiwyg') && Yii::$app->request->get('wysiwyg') == 'off') {
            $model->wysiwyg_vis = 0;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->moduleRedirect($model->id);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Block model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->setbcAction('Удаление блока');
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Block model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Block the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Block::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
