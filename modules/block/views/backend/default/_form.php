<?php

use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\block\models\Block */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box-body">
    <div class="form-group">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'wysiwyg_vis')->checkbox(); ?>

        <?= $form->field($model, 'vis')->checkbox(); ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= \app\modules\admin\widgets\CKEditorWidget::widget([
            'form'=>$form,
            'model'=>$model,
            'attr'=>'content',
            'textarea_id'=>'block-content',
            'gallery_widget'=>false,
            'vis'=>$model->wysiwyg_vis,
        ]); ?>

        <?= $form->field($model, 'page_url')->textInput(['maxlength' => true]) ?>

        <?= $this->render('@admin_layouts/chunks/_save_buttons', [
            'model' => $model,
            'form' => $form,
        ]); ?>

    <?php ActiveForm::end(); ?>

    </div>
</div>
