<?php

namespace app\modules\admin\helpers;

use app\modules\admin\widgets\grid\XEditableColumn;
use yii\helpers\Url;

class XEditableHelper
{

    public static function buildYesNo($attribute)
    {
        return self::buildListColumn([
            1 => 'Да',
            0 => 'Нет',
        ], $attribute);
    }

    public static function formatEditableArray($array = [])
    {
        return collect($array)->map(function ($item, $key) {
            return [
                'value' => $key,
                'text' => $item,
            ];
        })->values()->all();
    }

    public static function buildListColumn($array, $attribute, $default = [])
    {
        $source = $array;

        if(!empty($default)){
            $source = collect($default)->merge($array)->all();
        }

        return [
            'value' => function($data) use ($array, $attribute) {
                return collect($array)->get($data->$attribute);
            },
            'class' => XEditableColumn::className(),
            'url' => Url::to(['ajax-editable']),
            'dataType'=>'select',
            'editable'=> [
                'source' => self::formatEditableArray($source),
                'placement' => 'right',
                'emptytext' => '(Не задано)',
            ],
            'attribute' => $attribute,
            'format' => 'raw',

            'filter' => $array,
            'filterInputOptions' => ['class'=>'form-control','prompt' => 'Все'],
        ];
    }

    public static function buildEditPriority()
    {
        return self::buildEditField('priority', 'number');
    }

    public static function buildEditField($attribute, $data_type = 'text')
    {
        return [
            'value'=>function($model) use ($attribute) {
                return e($model->$attribute);
            },
            'class' => XEditableColumn::className(),
            'url' => Url::to(['ajax-editable']),
            'dataType'=>$data_type,
            'pluginOptions' => [
                'toggle' => 'manual',
                'name' => 'content',
                'title' => 'Enter comments',
            ],
            'editable'=>[
                'placement' => 'bottom',
                'emptytext' => '(Не задано)'

            ],
            'attribute' => $attribute,
            'format' => 'raw',
        ];
    }

}