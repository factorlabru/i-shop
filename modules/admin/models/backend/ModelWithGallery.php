<?php 
namespace app\modules\admin\models\backend;


use app\modules\gallery\models\backend\DynamicImage;
use yii\db\ActiveRecord;
use Yii;


class ModelWithGallery extends ActiveRecord
{

    public $imageCounter = 0; // счетчик количества картинок в галерее
    public $images;

    public function beforeDelete()
    {
        $currTableFullName = self::getTableSchema()->fullName;
        $currImageTableFullName = self::getTableSchema()->fullName . '_image';
        $currModelShortName=strtolower(substr(strrchr(self::className(), '\\'), 1));
        $filePath=\Yii::getAlias(Yii::$app->modules['gallery']['commonImageDir']) . $currModelShortName . DIRECTORY_SEPARATOR;
        $thumbPath=\Yii::getAlias(Yii::$app->modules['gallery']['commonImageDir']) . $currModelShortName . DIRECTORY_SEPARATOR . Yii::$app->modules['gallery']['thumbFilesPrefix'];

        $imagesToDel = Yii::$app->db->createCommand('SELECT id, file FROM '.$currImageTableFullName.' WHERE rid='.$this->id)->queryAll();
        $idsArray=array();
        foreach ($imagesToDel as $key => $imageToDel)
        {
            if (is_file($filePath . $imageToDel['file']))
                unlink($filePath . $imageToDel['file']);
            if (is_file($thumbPath . $imageToDel['file']))
                unlink($thumbPath . $imageToDel['file']);
            $idsArray[]=$imageToDel['id'];
        }
        $dataToDel=implode(', ',$idsArray);

        Yii::$app->db->createCommand()->delete($currImageTableFullName, "id in({$dataToDel})")->execute();

        return $this;
    }

    public function beforeSave($update)
    {
        $currImageTableFullName = self::getTableSchema()->fullName . '_image';
        $images=\Yii::$app->request->post('Image');
        if(sizeof($images)>0)
        {
            foreach ($images as $key => $image)
            {
                Yii::$app->db->createCommand()->update($currImageTableFullName, ['name' => $image['name'], 'link' => $image['link'], 'priority' => $image['priority']], 'id = ' . $image['id'])->execute();
            }
        }
        return $this;
    }

}

