<?php

namespace app\modules\admin\models\backend;

use app\modules\cabinet\models\Network;
use app\modules\cabinet\models\Wishlist;
use app\modules\catalog\models\Product;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use Yii;
use developeruz\db_rbac\interfaces\UserRbacInterface;

/**
 * This is the model class for table "tbl_user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email_verification_token
 * @property string $email
 * @property array|Product $wishlistProducts
 * @property integer $status
 * @property string $role
 * @property string $shop_start_bonus_points
 * @property string $last_sign_in
 * @property string $current_sign_in_ip
 * @property string $last_sign_in_ip
 * @property integer $created_at
 * @property integer $updated_at
 */
class User extends ActiveRecord implements IdentityInterface, UserRbacInterface
{

    public $password_field;
    public $role_auth;

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;
    const STATUS_UNACTIVE = 20;

    const SCENARIO_NETWORK_SIGNUP = 'network_signup'; // регистрация

    const ROLE_CLIENT = 'client';

    public $invalidate_all_sessions = true;

    public static function tableName()
    {
        return '{{%user}}';
    }

    public function behaviors()
    {
        return [
            'log' => [
                'class' => 'app\behaviors\LogBehavior',
            ],
            'saveRelations' => [
                'class'     => SaveRelationsBehavior::className(),
                'relations' => ['networks', 'wishlist', 'wishlistProducts', ]
            ],

            TimestampBehavior::className(),
        ];
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    public function afterFind()
    {
        $roles = array_keys(Yii::$app->authManager->getRolesByUser($this->id));
        if (!empty($roles)) {
            $this->role_auth = $roles[0];
        }
        return parent::afterFind();
    }

    public static function primaryKey()
    {
        return array('id');
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'username' => 'Имя пользователя',
            'password_field' => 'Пароль',
            'email' => 'E-mail',
            'status' => 'Статус',
            'role_auth' => 'Роль',
            'last_sign_in' => 'Последний вход',
            'current_sign_in_ip' => 'Текущий IP',
            'last_sign_in_ip' => 'Последний вход был с IP',
            'invalidate_all_sessions' => 'Обнулить все сессии',

            'shop_start_bonus_points' => 'Стартовое количество поинтов, для магазина',
        );
    }

    public function rules()
    {
        return [
            [['username', 'email', 'status', 'password_hash'], 'required', 'except' => [self::SCENARIO_NETWORK_SIGNUP]],
            [['username', 'email', 'password_field', 'role_auth'], 'safe'],
            ['password_field', 'required', 'on' => 'create'],
            ['invalidate_all_sessions', 'integer'],
            ['email', 'email'],
            [['current_sign_in_ip', 'last_sign_in_ip'], 'string', 'max' => 20],
            ['email', 'unique'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED, self::STATUS_UNACTIVE]],
        ];
    }

    public function search($params)
    {
        $query = User::find();
        $provider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 25,
            ],
            'sort' => [
                'defaultOrder' => [
                    'username' => SORT_ASC,
                ]
            ],
        ]);


        if ($this->load($params)) {

            // adjust the query by adding the filters

            $query->andFilterWhere([
                'id' => $this->id,
                'status' => $this->status,
            ]);

            $query->andFilterWhere(['like', 'username', $this->username])
                ->andFilterWhere(['like', 'email', $this->email])
                ->andFilterWhere(['like', 'status', $this->status]);

        }

        return $provider;
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => [self::STATUS_ACTIVE,],]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    public function hasValidResetToken()
    {
        return static::isPasswordResetTokenValid($this->password_reset_token);
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function beforeValidate()
    {
        if (!empty($this->password_field)) {
            $this->setPassword($this->password_field);
            if($this->invalidate_all_sessions){
                $this->generateAuthKey();
            }
        }
        return parent::beforeValidate();
    }

    public function getStatuses()
    {
        return [self::STATUS_DELETED => 'Не активен', self::STATUS_ACTIVE => 'Активен'];
    }

    public function getUserName()
    {
        return $this->username;
    }

    public static function logUserAccess($username, $id = null)
    {
        $user = static::findByUsername($username);

        $user->last_sign_in = date("Y-m-d H:i:s");
        $user->last_sign_in_ip = $user->current_sign_in_ip;
        $user->current_sign_in_ip = Yii::$app->request->userIP;
        $user->save();
    }

    public function getNetworks()
    {
        return $this->hasMany(Network::className(), ['user_id' => 'id']);
    }


    public static function signUpByNetwork($network, $identity)
    {
        $user = new User();
        $user->scenario = self::SCENARIO_NETWORK_SIGNUP;
        $user->created_at = date('Y-m-d H:i:s');
        $user->status = self::STATUS_ACTIVE;
        $user->role = self::ROLE_CLIENT;
        $user->setPassword(Yii::$app->security->generateRandomString());
        $user->generateAuthKey();

        $user->networks = [Network::create($network, $identity)];

        return $user;
    }


    public function isUnfilled()
    {
        return empty($this->username) || empty($this->email);
    }

    public function attachNetwork($network, $identity)
    {
        /** @var Network[] $networks */
        $networks = $this->networks;
        foreach ($networks as $current) {
            if ($current->isFor($network, $identity)) {
                throw new \DomainException('Network is already attached.');
            }
        }
        $networks[] = Network::create($network, $identity);
        $this->networks = $networks;
    }


    /**
     * @return ActiveQuery
     */
    public function getWishlistProducts()
    {
        return $this->hasMany(Product::class, ['id' => 'product_id'])
            ->via('wishlist');
    }

    /**
     * @return ActiveQuery
     */
    public function getWishlist()
    {
        return $this->hasMany(Wishlist::class, ['user_id' => 'id']);
    }
}

