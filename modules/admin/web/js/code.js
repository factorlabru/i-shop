$(document).ready(function(){

    if($('a[data-type=number]').length) {
        $('a[data-type=number]').attr('data-min', '1');
    }

    //отвечает за запоминание состояния левого сайдбара
    $(document).on('click', '.left-sidebar__control', function(event) {
        if(!localStorage.getItem('left_sidebar')) {
            localStorage.setItem('left_sidebar', 1);
        } else {
            localStorage.removeItem('left_sidebar');
        }
    });

    $(window).scroll(function () {
        var bHeight = $(window).height();
        var offset = $(window).scrollTop();
    });

    if($('#stick-mark').length) {
        $(window).scroll(function(){

            var el_offset = $('#stick-mark').offset().top,
                outer_height = $('#stick-mark').outerHeight(),
                window_width = $(window).height(),
                scroll_top = $(this).scrollTop();

            if (scroll_top > (el_offset + outer_height - window_width)){
                $('.save-buttons').removeClass('sticky-element');
                $('.icons-save_buttons').hide();
                $('.simple-save_buttons').show();

            } else {
                $('.save-buttons').addClass('sticky-element');
                $('.icons-save_buttons').show();
                $('.simple-save_buttons').hide();
            }

        });//scroll

        setTimeout(function(){
            $(window).scroll();
        }, 500)

    }

    $(document).on('keyup', '#metaData input[type=text], #metaData textarea', function(event) {
        var id = $(this).data('id'),
            len = $(this).val().length;

        $('.count-'+id+' span').html(len);

        return false;
    });

    if($('#metaData').length) {
        $('#metaData input[type=text], #metaData textarea').each(function() {
            var id = $(this).data('id'),
                len = $(this).val().length;

            $('.count-'+id+' span').html(len);
        });
    }


    function translit(){
        // Символ, на который будут заменяться все спецсимволы
        var symbol = '_';
        // Берем значение из нужного поля и переводим в нижний регистр
        var text = $('.translit-from').val().toLowerCase();

        // Массив для транслитерации
        var transl = {
            'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'e', 'ж': 'j',
            'з': 'z', 'и': 'i', 'й': 'y', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n',
            'о': 'o', 'п': 'p', 'р': 'r','с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'h',
            'ц': 'ts', 'ч': 'ch', 'ш': 'sh', 'щ': 'sch','ъ': '', 'ы': 'y', 'ь': '', 'э': 'e', 'ю': 'yu', 'я': 'ya',
            ' ': symbol, '_': symbol, '`': symbol, '~': symbol, '!': symbol, '@': symbol,
            '#': symbol, '$': symbol, '%': symbol, '^': symbol, '&': symbol, '*': symbol,
            '(': symbol, ')': symbol,'-': '-', '\=': symbol, '+': symbol, '[': symbol,
            ']': symbol, '\\': symbol, '|': symbol, '/': symbol,'.': symbol, ',': symbol,
            '{': symbol, '}': symbol, '\'': symbol, '"': symbol, ';': symbol, ':': symbol,
            '?': symbol, '<': symbol, '>': symbol, '№':symbol, '«':'', '»':''
        }

        var result = '';
        var curent_sim = '';

        for(i=0; i < text.length; i++) {
            // Если символ найден в массиве то меняем его
            if(transl[text[i]] != undefined) {
                if(curent_sim != transl[text[i]] || curent_sim != symbol){
                    result += transl[text[i]];
                    curent_sim = transl[text[i]];
                }
            }
            // Если нет, то оставляем так как есть
            else {
                result += text[i];
                curent_sim = text[i];
            }
        }

        result = TrimStr(result);

        // Выводим результат
        $('.translit-to').val(result);

    }
    function TrimStr(s) {
        s = s.replace(/^-/, '');
        return s.replace(/-$/, '');
    }
    // Выполняем транслитерацию при вводе текста в поле
    $(function(){
        $('.translit-from').keyup(function(){

            translit();
            return false;
        });
    });

    function generatePassword() {
        var length = 6,
            charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789$%*&",
            password = "";
        for (var i = 0, n = charset.length; i < length; ++i) {
            password += charset.charAt(Math.floor(Math.random() * n));
        }
        return password;
    }

    $(document).on('click', '.generate-password__link', function(event) {
        var password = generatePassword();

        $('#user-password_field').val(password);
        $('#user-password_field').next().next().html('<b>Сгенерированный пароль:</b> '+ password);

        return false;
    });

});