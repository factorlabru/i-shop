<div class="accordion-group meta-accordion">
    <div class="accordion-heading">
        <a class="accordion-toggle collapsed" data-toggle="collapse" href="#metaData">
            Мета-данные
        </a>
    </div>
    <div id="metaData" class="accordion-body collapse" style="height: 0px;">
        <div class="accordion-inner">
            <div class="control-group">
                <?= $form->field($model, 'meta_title',
                    [
                        'template' => '{label}
                                <div class="col-sm-12 nopadding inline-input">
                                    {input}  <div class="count-meta-input">Символов: <span>0</span> шт.</div>
                                    {error}{hint}
                                </div>'
                    ])->textInput(['maxlength' => true, 'data-id'=>'meta-input']) ?>
            </div>

            <div class="control-group">
                <?= $form->field($model, 'meta_keywords',
                    [
                        'template' => '{label}
                                <div class="col-sm-12 nopadding inline-input">
                                    {input}  <div class="count-meta-keywords">Символов: <span>0</span> шт.</div>
                                    {error}{hint}
                                </div>'
                    ])->textInput(['maxlength' => true, 'data-id'=>'meta-keywords']) ?>
            </div>

            <div class="control-group">
                <?= $form->field($model, 'meta_description',
                    [
                        'template' => '{label}
                                <div class="col-sm-12 nopadding inline-input">
                                    {input}  <div class="count-meta-description">Символов: <span>0</span> шт.</div>
                                    {error}{hint}
                                </div>'
                    ])->textArea(['rows' => 6, 'data-id'=>'meta-description']) ?>
            </div>

            <div class="control-group">
                <?= $form->field($model, 'h1_tag',
                    [
                        'template' => '{label}
                                <div class="col-sm-12 nopadding inline-input">
                                    {input}  <div class="count-h1_tag">Символов: <span>0</span> шт.</div>
                                    {error}{hint}
                                </div>'
                    ])->textInput(['maxlength' => true, 'data-id'=>'h1_tag']) ?>
            </div>
        </div>
    </div>
</div>