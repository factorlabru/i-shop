<?php
use app\widgets\Alert;
use app\modules\admin\widgets\backend\DashboardWidgetFirst;
use app\modules\admin\widgets\backend\DashboardWidgetSecond;
use app\modules\admin\widgets\backend\DashboardWidgetThird;
/* @var $this yii\web\View */

$this->title = 'Administration tool';
?>
<div class="container">
    <?= Alert::widget() ?>
</div>

<section class="content">
    <div class="row">
        <?= DashboardWidgetFirst::widget(['modelName'=>'Section', 'color'=>'red', 'icon'=>'folder']); ?>
        <?= DashboardWidgetFirst::widget(['modelName'=>'Snippet', 'color'=>'green', 'icon'=>'code']); ?>
        <?= DashboardWidgetFirst::widget(['modelName'=>'User', 'color'=>'yellow', 'icon'=>'person']); ?>
        <?= DashboardWidgetFirst::widget(['modelName'=>'Param', 'color'=>'blue', 'icon'=>'wrench']); ?>
    </div>
    <div class="row">
        <?//= DashboardWidgetSecond::widget(); ?>
        <?//= DashboardWidgetThird::widget(); ?>
    </div>
</section>
