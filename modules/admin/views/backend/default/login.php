<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */

//$this->title = $this->context->getbcAction();
$this->title = 'Авторизация';
?>

    <!-- Main content -->
    <section class="content">
        <h1 style="text-align:center;"><?=$this->title?></h1>
        <div>
            <div class="row">
                <div style="width:30%; margin:0 auto;">
                        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
                            <?= $form->field($model, 'password')->passwordInput() ?>
                            <?= $form->field($model, 'rememberMe')->checkbox() ?>
                            <div class="form-group">
                                <?= Html::submitButton('Вход', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                            </div>
                        <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->