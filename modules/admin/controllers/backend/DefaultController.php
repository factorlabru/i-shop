<?php

namespace app\modules\admin\controllers\backend;

use app\modules\admin\models\backend\LoginForm;
use Yii;
use app\modules\admin\components\AdminController;
use app\modules\admin\models\backend\User;

class DefaultController extends AdminController
{

    public function actionLogin()
    {
        if (Yii::$app->user->can('admin/default/index') || Yii::$app->user->can('admin')) {
            return $this->redirect(Yii::getAlias('@admin'));
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $user = User::logUserAccess($model->username);

            return $this->redirect('/admin');
        } else {
            $this->layout = '@app/modules/admin/views/backend/default/blank';
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        if(!Yii::$app->user->isGuest) {
            Yii::$app->user->logout();
        }
        $this->redirect('/');
    }

    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            $this->layout = '@app/modules/admin/views/backend/default/main';
            return $this->render('@app/modules/admin/views/backend/default/index', [
                'content' => '',
            ]);
        } else {
            return $this->redirect('/admin/login');
        }
    }

    public function actionClearcache()
    {
        Yii::$app->cache->flush();

        return $this->redirect('/admin/');
    }
}