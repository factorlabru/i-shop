<?php
namespace app\modules\admin\controllers\console;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\HttpException;
use yii\helpers\Url;

class BackController extends Controller
{
    public $layout = '@app/modules/admin/views/backend/default/main.php';

    /**
    * Название контроллера - используется в хлебных крошках
    */
    public $_bcController = ''; 

    /**
    * Название экшена - используется в хлебных крошках
    */
    public $_bcAction = '';

    /**
     * Порядок отображения в левом меню - используется в виджете Menu
     */
    public $_order = '';

    /**
    * Отображение кнопки "Добавить" на странице списка элементов (обычно actionIndex)
    */
    public $_enableCreate = true;

    public function setbcAction($name)
    {
        $this->_bcAction = $name;
    }

    public function getbcAction()
    {
        return $this->_bcAction;
    }

    public function init()
    {
        parent::init();
    }
    /**
     * Редирект на экшен index текущего контроллера
     */
    public function goToIndex(){
        $path = ['/admin'];
        $module = $this->module->id;
        if ($module != 'admin') {
            $path[] = $module;
        }
        $path[] = $this->id;
        $path[] = 'index';
        $this->redirect(implode('/',$path));
    }

    public function actionDevmodeon()
    {
        Yii::$app->session->set('devmode',true);
        $this->goToIndex();
    }

    public function actionDevmodeoff()
    {
        Yii::$app->session->set('devmode',false);
        $this->goToIndex();
    }

    public function beforeAction($action)
    {
        if (Yii::$app->user->isGuest && (($action->id != 'login') && ($this->module->id != 'user'))) {
            $this->redirect(Url::toRoute('@login'));
        } else {
            $path = implode('/', [$this->module->id, $this->id, $action->id]);
            $role = Yii::$app->authManager->getRolesByUser(Yii::$app->user->id);
            $role = array_keys($role);
            if (!empty($role[0])) $role = $role[0];

            if (($role == 'admin') or (Yii::$app->user->can($path)) or ($action->id == 'error')) {
                return parent::beforeAction($action);
            } else {
               throw new HttpException(403, 'Доступ запрещен.');
            }
        }
        return parent::beforeAction($action);
    }
}
