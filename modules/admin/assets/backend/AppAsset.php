<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\admin\assets\backend;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/admin/web';
    public $baseUrl = '/web';
    public $css = [
        'css/site.css',
        'css/theme/font-awesome.min.css',
        'css/theme/css/AdminLTE.min.css',
        'css/theme/css/skins/skin-red.css',
    ];
    public $images = '/css/theme/images/';
    public $js = [
		'js/app.min.js',
        'js/backend.js',
        'js/code.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
