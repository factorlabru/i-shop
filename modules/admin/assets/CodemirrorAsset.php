<?php

namespace app\modules\admin\assets;

use yii\web\AssetBundle;

class CodemirrorAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/admin/web';
    public $baseUrl = '/web';
    public $css = [
        'css/codemirror.css',
    ];
    public $js = [
        'js/codemirror.js',
        'js/codemirror_modes/css/css.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}
