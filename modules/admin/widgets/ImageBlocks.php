<?php
namespace app\modules\admin\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Url;

class ImageBlocks extends Widget
{
    /**
     * Массив с картинками
     * @var array
     */
    public $data;

    /**
     * URL для сохранения записи.
     * @var string
     */
    public $request_save_url = ['ajax-save-image-info'];

    /**
     * URL для удаления записи.
     * @var string
     */
    public $request_del_url = ['ajax-related-delete-image'];

    public function run()
    {
        $request_del_url = Url::to($this->request_del_url);
        $request_save_url = Url::to($this->request_save_url);

        return $this->render('image_blocks', [
            'data'=>$this->data,
            'request_del_url'=>$request_del_url,
            'request_save_url'=>$request_save_url,
        ]);
    }
}