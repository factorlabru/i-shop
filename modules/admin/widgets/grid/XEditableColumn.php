<?php

/**
 * @inheritdoc
 */

namespace app\modules\admin\widgets\grid;

use yii\helpers\ArrayHelper;

use yii\helpers\Json;

class XEditableColumn extends \mcms\xeditable\XEditableColumn
{

    // исправил экранирование. т.к. кавычки в значении рушат верстку грида.
	protected function getDataCellContent($model, $key, $index)
	{
		if (empty($this->url)) {
			$this->url = \Yii::$app->urlManager->createUrl($_SERVER['REQUEST_URI']);
		}

		if (empty($this->value)) {
			$value = ArrayHelper::getValue($model, $this->attribute);
		} else {
			$value = call_user_func($this->value, $model, $index, $this);
		}

		$value = '<a href="#" data-name="'.$this->attribute.'" data-value="' . e($model->{$this->attribute}) . '"  class="editable" data-type="' . $this->dataType . '" data-pk="' . $model->{$this->pk} . '" data-url="' . $this->url . '" data-title="' . $this->dataTitle . '">' . $value . '</a>';

		return $value;
	}

} 
