function initSortableWidgets(url, grid_id) {

    var fixHelper = function(e, ui) {
        ui.children().each(function() {
            $(this).width($(this).width());
        });
        return ui;
    };

    $('#' + grid_id).sortable({
        handle: '.sortable-widget-handler',
        forceHelperSize: true,
        forcePlaceholderSize: true,
        axis: 'y',
        items: 'tr',
        stop: function (e) {
            var data = [];
            $("tbody [data-key]").each(function () {
                data.push($(this).data('key'));
            });
            $.post(url, {
                sorting: data
            });
        },
        helper: fixHelper
    });
}
