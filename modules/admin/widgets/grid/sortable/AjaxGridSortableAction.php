<?php

namespace app\modules\admin\widgets\grid\sortable;

use yii\db\ActiveRecord;

/**
 * Class Action
 */
class AjaxGridSortableAction extends \yii\rest\Action
{
    /** @var string */
    public $order_attribute = 'priority';

    public function run()
    {
        /** @var SortableBehaviour|ActiveRecord $model_inst */
        $model_inst = new $this->modelClass;
        $order_attribute = $this->order_attribute;

        $items = \Yii::$app->request->post('sorting', []);

        $errors = [];
        $result = true;
        if($items){
            $i = 1;
            $models = $model_inst::find()->where(['in', 'id', $items])->indexBy('id')->all();
            foreach ($items as $id) {
                if(!isset($models[$id])){
                    throw new \RuntimeException('Model not found');
                }

                /** @var ActiveRecord $model */
                $model = $models[$id];
                $model->$order_attribute = $i++;

                if(!$model->save()){
                    $errors = $model->getErrors();
                    $result = false;
                    break;
                }
            }
        }

        return \yii\helpers\Json::encode([
            'result' => $result,
            'errors' => $errors
        ]);
    }

    public function getPrimaryKeyList()
    {
        $list = [];
        /* @var $class \yii\db\ActiveRecord */
        $class = $this->modelClass;
        $pks = $class::primaryKey();
        foreach (\Yii::$app->request->post('sorting') as $pk) {
            if (count($pks) === 1) {
                $list[] = [$pks[0] => $pk];
            } else {
                $list[] = json_decode($pk, true);
            }
        }

        return $list;
    }
}
