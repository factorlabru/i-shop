<?php
namespace app\modules\admin\widgets;

use Yii;
use yii\base\Widget;
use app\modules\snippet\models\Snippet;
use app\modules\gallery\models\Gallery;

class CKEditorWidgets extends Widget
{
    public $textarea_id;

    /**
     * Параметр отвечает за показ виджета сниппетов.
     *
     * @var bool
     */
    public $snippet_widget = true;

    /**
     * Параметр отвечает за показ виджета галереи.
     * @var bool
     */
    public $gallery_widget = true;

    public function run()
    {
        $snippets = false;
        $galleries = false;

        if($this->snippet_widget) {
            $snippets = Snippet::find()
                ->orderBy('name')
                ->all();
        }

        if($this->gallery_widget) {
            $galleries = Gallery::find()
                ->orderBy('name')
                ->all();
        }

        return $this->render('ckeditor_widgets', [
            'textarea_id'=>$this->textarea_id,
            'snippets'=>$snippets,
            'galleries'=>$galleries,
        ]);
    }
}