<?php
namespace app\modules\admin\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Url;

class GridMultiDelete extends Widget
{
    /**
     * URL для удаления записей.
     * @var string
     */
    public $request_del_url = ['ajax-delete-grid-items'];


    public function run()
    {
        $request_del_url = Url::to($this->request_del_url);

        return $this->render('grid_multi_delete', [
            'request_del_url'=>$request_del_url,
        ]);
    }
}