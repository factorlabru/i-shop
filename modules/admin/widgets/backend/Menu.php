<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\admin\widgets\backend;

use app\modules\modulemanager\models\Module;
use app\modules\modulemanager\models\ModuleManager;
use Yii;
use yii\helpers\Html;
use yii\helpers\BaseArrayHelper;

class Menu extends \yii\bootstrap\Widget
{

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $module_manager = new ModuleManager();

        $modules_records = Module::find()->indexBy('module')->all();
        $modules = $modules_records;

        $items = [];

        $module_config = collect(Yii::$app->modules_config);
        foreach ($modules as $key => $module) {
            if ($module->active && $module->menu_vis) {


                $badge = '';
                $viewed = $module_config->get($key . '.params.viewed', false);
                $viewed_model = $module_config->get($key . '.params.model', false);
                if ($viewed && $viewed_model) {
                    $count = $viewed_model::find()->where(['viewed' => 0])->count();
                    if ($count) {
                        $badge = '&nbsp;&nbsp;<span class="badge">' . $count . '</span>';
                    }
                }

                $submenu = $module_config->get($key . '.params._items', false);

                $items[] =
                    [
                        'order' => $module->priority,
                        'icon' => $module->icon,
                        'label' => $module->name . $badge,
                        'url' => '/admin/' . $key . '/default/index',
                        'items' => $submenu
                    ];
            }

        }
        return $this->render('menu', ['items' => $items]);
    }

    public function activeItem($url)
    {
        return (strpos(Yii::$app->request->url, $url) !== false);
    }

    public function renderItems($items = [])
    {
        BaseArrayHelper::multisort($items, 'order', SORT_ASC, SORT_NUMERIC);
        foreach ($items as $item) {
            $class = [];
            $multilevel = false;
            if (!empty($item['items'])) {
                $class[] = 'treeview';
                $multilevel = true;
            }

            if ($this->activeItem($item['url']) || (stristr($item['url'], Yii::$app->controller->module->id) && Yii::$app->controller->module->id != 'admin')) {
                $class[] = 'active';
            }

            echo Html::beginTag('li', ['class' => implode(' ', $class)]);
            echo Html::a($item['icon'] . '<span>' . ($this->activeItem($item['url']) ? '<b>' . $item['label'] . '</b>' : $item['label']) . '</span>'
                . (($multilevel) ? '<i class="fa fa-angle-left pull-right drpdwn"></i>' : ''), $item['url']);

            if ($multilevel) {
                echo Html::beginTag('ul', ['class' => 'treeview-menu']);
                $this->renderItems($item['items']);
                echo Html::endTag('ul');
            }

            echo Html::endTag('li');
        }
    }
}
