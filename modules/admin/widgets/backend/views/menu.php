<?php
use app\modules\admin\widgets\backend\Menu;
/**
*	классы
*	li active - активный элемент
*	li header - заголовок - не ссылка
*	li treeview - есть вложенные элементы
*	ul treeview-menu - список вложенных элементов
*	i text-red - цвет текста красный
*	i pull-right - прижат к правому краю
*	i fa fa-circle-o - кружок
*/

?>

<ul class="sidebar-menu">
	<li class="header">ОСНОВНОЕ МЕНЮ</li>
	<li class="<?=(Yii::$app->controller->module->id == 'admin' ? ' active' : '')?>">
		<a href="/admin">
			<i class="fa fa-dashboard"></i> <span>Панель управления</span>
		</a>
	</li>
	<?php $this->context->renderItems($items); ?>
</ul>
<br>