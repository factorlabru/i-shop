<div class="col-md-6">
	<!-- USERS LIST -->
	<div class="box box-danger">
		<div class="box-header with-border">
			<h3 class="box-title">Галереи</h3>

			<div class="box-tools pull-right">
				<span class="label label-danger"><?= sizeof($gallery)?> шт.</span>
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
				</button>
			</div>
		</div>
		<!-- /.box-header -->
		<div class="box-body no-padding">
			<ul class="users-list clearfix">
				<?php foreach($gallery as $key => $gal) {?>
				<li>
					<img src="<?= Yii::$app->modules['gallery']['imagePath'] . Yii::$app->modules['gallery']['thumbFilesPrefix'] . $gal->cover ?>" alt="User Image">
					<a class="users-list-name" href="/admin/gallery/default/update?id=<?= $gal->id ?>"><?= $gal->name ?></a>
					<span class="users-list-date">картинок <?= $gal->imageCounter ?> шт.</span>
				</li>
				<?php } ?>
			</ul>
			<!-- /.users-list -->
		</div>
		<!-- /.box-body -->
		<div class="box-footer text-center">
			<a href="/admin/gallery/default/index" class="uppercase">Перейти к галереям</a>
		</div>
		<!-- /.box-footer -->
	</div>
	<!--/.box -->
</div>
<!-- /.col -->