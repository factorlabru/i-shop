<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\admin\widgets\backend;

use Yii;
use yii\widgets\Breadcrumbs as YiiBC;

class Breadcrumbs extends \yii\bootstrap\Widget
{

    public function init()
    {
        parent::init();
    }
	
	public function run()
	{
		$items = [['label'=>'Панель управления', 'url'=> '@admin']];
		$path = ['admin'];
		if ((!empty(Yii::$app->modules[Yii::$app->controller->module->id])) and (!empty(Yii::$app->modules[Yii::$app->controller->module->id]->_bcModule))) {
			$path[] = Yii::$app->controller->module->id;
			$items[] = ['label'=>Yii::$app->modules[Yii::$app->controller->module->id]->_bcIcon . Yii::$app->modules[Yii::$app->controller->module->id]->_bcModule, 'url'=>'/'.implode('/', $path).'/default/index'];
		}
		$path[] = Yii::$app->controller->id;
		if (!empty(Yii::$app->controller->_bcController)) {
			$items[] = ['label' => Yii::$app->controller->_bcController, 'url'=>'/'.implode('/', $path).'/index'];
		}
		$path[] = Yii::$app->controller->action->id;
		if (!empty(Yii::$app->controller->_bcAction)) {
			$items[] = ['label' => Yii::$app->controller->getbcAction()];
		}
		
		return YiiBC::widget([
			'tag'=> 'ol',
			'options'=> ['class'=>'breadcrumb'],
			'activeItemTemplate'=> '<li class="active">{link}</li>',
			'itemTemplate' => '<li>{link}</li>',
			'encodeLabels' => false,
			'homeLink' => false,
            'links' => $items
        ]);

	}
}
