<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\admin\widgets\backend;

use Yii;

class SaveButtons extends \yii\bootstrap\Widget
{
    public $model=array();

    public function init()
    {
        parent::init();
    }
	
	public function run()
	{
		return $this->render('save_buttons',['model' => $this->model,]);
	}

}
