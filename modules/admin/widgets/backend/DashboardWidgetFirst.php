<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\admin\widgets\backend;

use Yii;

use \app\modules\admin\models\backend\User;
use app\modules\gallery\models\Gallery;
use \app\modules\param\models\backend\Param;
use \app\modules\snippet\models\Snippet;
use \app\modules\section\models\Section;

class DashboardWidgetFirst extends \yii\bootstrap\Widget
{
	public $modelName;
	public $color;
	public $title;
	public $count;
	public $moduleName;
	public $icon;

    public function init()
    {
        parent::init();
    }
	
	public function run()
	{
		switch ($this->modelName) {
			case 'User':
				$this->count=User::find()->count();
				break;
			case 'Section':
				$this->count = Section::find()->count();
				break;
			case 'Gallery':
				$this->count=Gallery::find()->count();
				break;
			case 'Param':
				$this->count=Param::find()->count();
				break;
			case 'Snippet':
				$this->count=Snippet::find()->count();
				break;
		}

		$this->moduleName = strtolower($this->modelName);

		$this->title = Yii::$app->modules[$this->moduleName]['params']['_bcModule'];
		return $this->render('dashboard_widget_first',
			[
				'color' => $this->color,
				'title' => $this->title,
				'count' => $this->count,
				'icon' => $this->icon,
				'moduleName' => $this->moduleName
			]
		);

	}
	

	



	
}
