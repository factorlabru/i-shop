<?php
/**
 * Action удаляет связанные картинки. Вместе с моделью.
 */
namespace app\modules\admin\actions;

use Yii;
use yii\base\Action;
use yii\web\NotFoundHttpException;

class AjaxRelatedDeleteImageAction extends Action
{

    /**
     * Связанная модель. Например RoomImage.
     * @var
     */
    public $related_model;

    public function run()
    {
        if (Yii::$app->request->post('id')) {
            $id =  Yii::$app->request->post('id');
            $related_model = $this->related_model;
            $model = $related_model::findOne($id);

            if(!$model){
                throw new NotFoundHttpException();
            }

            $result = $model->delete();

            //\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\helpers\Json::encode([
                'result' => $result
            ]);
        }

        throw new NotFoundHttpException('Страница не найдена.');
    }


}