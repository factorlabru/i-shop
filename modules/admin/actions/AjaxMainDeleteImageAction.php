<?php
/**
 * Action удаляет картину.
 */
namespace app\modules\admin\actions;

use Imagine\Exception\RuntimeException;
use Yii;
use yii\base\Action;
use yii\db\ActiveRecord;
use yii\web\NotFoundHttpException;

class AjaxMainDeleteImageAction extends Action
{
    /**
     * Основная модель.
     * @var
     */
    public $model;
    public $id;
    public $behavior_name = 'image';

    public function run()
    {
        if (Yii::$app->request->post('id')) {

            $this->id =  Yii::$app->request->post('id');
            $attribute_from_post =  Yii::$app->request->post('attribute');

            $result = false;

            /** @var ActiveRecord $model */
            $model = $this->model;
            $model = $model::findOne($this->id);
            if ($model) {
                $behavior = $model->getBehavior($this->behavior_name);

                if(!$behavior){
                    // если не удаляется картинка, то возможно вы не указали имя для Behavior
                    throw new RuntimeException('Не указано имя behavior');
                }

                $attribute = $behavior->attribute;

                if($attribute_from_post != $attribute){
                    /*
                     * Передаете один атрибут, а в экшене указан другой?
                     * Эта проверка нужна, чтобы не удалить какой-то неправильный атрибут.
                     * И убедиться, что вы указали правильный экшен
                     */
                    throw new RuntimeException('Wrong attribute name.');
                }

                $model->$attribute = null;

                $result = $model->save();
            }

            //\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\helpers\Json::encode([
                'result' => $result,
                'errors' => $model->getErrors()
            ]);
        }

        throw new NotFoundHttpException('Страница не найдена.');
    }


}