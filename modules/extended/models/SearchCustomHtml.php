<?php

namespace app\modules\extended\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\block\models\Block;

/**
 * SearchBlock represents the model behind the search form about `app\modules\block\models\backend\Block`.
 */
class SearchCustomHtml extends CustomHtml
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'vis', 'priority'], 'integer'],
            [['position', 'html'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CustomHtml::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'vis' => $this->vis,
            'name' => $this->name,
            'position' => $this->position,
            'html' => $this->html,
        ]);
        $query->orderBy('priority, id');

        $query
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'position', $this->position])
            ->andFilterWhere(['like', 'vis', $this->vis])
            ->andFilterWhere(['like', 'html', $this->html]);

        return $dataProvider;
    }
}
