<?php
\app\modules\admin\assets\CodemirrorAsset::register($this);
use yii\helpers\Html;
use yii\bootstrap\Alert;

$this->title = 'Редактирование robots.txt';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="content">
    <legend>
        <a href="<?= Yii::getAlias('@web/robots.txt') ?>" target="_blank"><?= Yii::getAlias('@web/robots.txt') ?></a>
    </legend>

    <?php if(isset($file)) {?>

        <?php if(Yii::$app->session->getFlash('save')) {
            echo Alert::widget([
                'options' => ['class' => 'alert-success',],
                'body' => Yii::$app->session->getFlash('save'),
            ]);
        }?>

        <?= Html::beginForm() ?>
        <fieldset>
        <textarea id="code" style="height: 350px; width: 100%;" name="content"><?=$file?></textarea>
        </fieldset>
        <hr/>
        <div class="control-group">
            <input name="apply" class="btn btn-primary" type="submit" value="Сохранить">
        </div>
        <?= Html::endForm() ?>

    <?php } else {
        echo Html::tag("p", 'Файл robots.txt отсутствует.');
    }

    $script = <<< JS
     CodeMirror.commands.autocomplete = function(cm) {
        cm.showHint({hint: CodeMirror.hint.anyword});
    }
    var editor = CodeMirror.fromTextArea(document.getElementById("code"), {
        lineNumbers: true,
        extraKeys: {"Ctrl-Space": "autocomplete"}
    });
JS;
    $this->registerJs($script, yii\web\View::POS_READY);
    ?>

</div>
