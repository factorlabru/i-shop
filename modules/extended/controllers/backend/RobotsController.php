<?php

namespace app\modules\extended\controllers\backend;

use Yii;
use app\modules\admin\components\AdminController;

/**
 * Default controller for the `extendedstyle` module
 */
class RobotsController extends AdminController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $this->setbcAction('Редактирование robots.txt');

        $file_path = Yii::getAlias('@webroot/robots.txt');

        if (!file_exists($file_path)) {
            file_put_contents($file_path, "# /robots.txt");
        }

        $file = file_get_contents($file_path);

        $content = Yii::$app->request->post('content');
        if(Yii::$app->request->post('content')) {
            file_put_contents($file_path, $content);
            \Yii::$app->getSession()->setFlash('save', 'Данные сохранены.');

            return $this->redirect('/admin/extended/robots/');
        }

        return $this->render('index', ['file'=>$file]);
    }
}
