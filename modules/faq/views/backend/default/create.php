<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\faq\models\Faq */

$this->title = 'Создание записи FAQ';
$this->params['breadcrumbs'][] = ['label' => 'FAQ', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="faq-create content">
    <div class="box">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
