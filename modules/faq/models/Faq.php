<?php

namespace app\modules\faq\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "tbl_faq".
 *
 * @property integer $id
 * @property string $question
 * @property string $answer
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property integer $priority
 * @property integer $vis
 * @property string $updated_at
 * @property string $created_at
 */
class Faq extends \yii\db\ActiveRecord
{
    public $verifyCode;
    const PAGE_SIZE = 5;

    public function behaviors()
    {
        return [
            'log' => [
                'class' => 'app\behaviors\LogBehavior',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'PriorityBehavior' => [
                'class' => 'app\behaviors\PriorityBehavior',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_faq';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'question'], 'required'],
            [['question', 'answer'], 'string'],
            ['email', 'email'],
            [
                'verifyCode',
                'captcha',
                'captchaAction' => '/faq/faq/captcha',
                'skipOnEmpty'=>!Yii::$app->user->isGuest
            ],
            [['priority', 'vis'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
            [['name', 'email', 'phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'question' => 'Вопрос',
            'answer' => 'Ответ',
            'name' => 'ФИО',
            'email' => 'Email',
            'phone' => 'Телефон',
            'priority' => 'Приоритет',
            'vis' => 'Показывать',
            'updated_at' => 'Updated At',
            'created_at' => 'Дата создания',
        ];
    }

    public function sendMail($model, $mail_to, $mail_from='', $subject='Вопрос с сайта')
    {
        $mail_from = $mail_from ? $mail_from : 'noreply@'.$_SERVER['SERVER_NAME'];

        Yii::$app->mailer->compose('@modules/faq/mails/letter', ['model' => $model])
            ->setFrom($mail_from)
            ->setTo($mail_to)
            ->setSubject($subject.' '.$_SERVER['SERVER_NAME'])
            ->send();
    }
}
