<?php

namespace app\modules\subscription\controllers\backend;

use app\modules\admin\actions\AjaxDeleteGridItemsAction;
use Yii;
use app\modules\subscription\models\Subscription;
use app\modules\subscription\models\SubscriptionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\admin\components\AdminController;

/**
 * DefaultController implements the CRUD actions for Subscription model.
 */
class DefaultController extends AdminController
{
    public function actions()
    {
        $model_name = Subscription::className();

        return [
            'ajax-delete-grid-items' => [
                'class' => AjaxDeleteGridItemsAction::className(),
                'model' => $model_name,
            ],
        ];
    }

    /**
     * Lists all Subscription models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SubscriptionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Subscription model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Deletes an existing Subscription model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Subscription model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Subscription the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Subscription::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionUnload()
    {
        $model = Subscription::find()->orderBy('created_at DESC')->all();

        if($model) {
            $file_path = Yii::getAlias('@app/web/content/subscribers.txt');

            $data = '';
            foreach ($model as $item) {
                $data .= $item->email."\n";
            }

            $fp = fopen($file_path, "w");
            fwrite($fp, $data);
            fclose($fp);

            header("Content-Disposition: attachment; filename=subscribers.txt"); // use 'attachment' to force a download
            header("Content-type: text/plain"); // add here more headers for diff. extensions

            $fsize = filesize($file_path);
            if($fsize) {
                header("Content-length: $fsize");
            }
            readfile($file_path);
            exit;
        }
    }
}
