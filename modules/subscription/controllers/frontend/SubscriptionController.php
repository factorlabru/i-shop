<?php
namespace app\modules\subscription\controllers\frontend;

use app\components\Controller;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use app\modules\Subscription\models\Subscription;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use app\components\NumericCaptcha;

class SubscriptionController extends Controller
{
    public function actionIndex()
    {
        if(Yii::$app->request->post('Subscription')['name'] && Yii::$app->request->post('Subscription')['email']) {
            $model = new Subscription();
            $result = false;

            if($model->load(Yii::$app->request->post()) && $model->save()) {

                $mail = (new \app\helpers\Mail())->sendMail($model,
                    Yii::$app->params['admin_email'],
                    '@modules/subscription/mails/letter',
                    'Подписка');

                $result = true;
            }

            return \yii\helpers\Json::encode(['result'=>$result]);
        } else {
            throw new NotFoundHttpException('Страница не найдена.');
        }
    }

    public function actionComplete()
    {
        $this->setMeta(['title'=>'Сообщение отправлено']);

        return $this->render('complete');
    }
}