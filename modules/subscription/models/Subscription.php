<?php

namespace app\modules\subscription\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "tbl_subscriptions".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $ip
 * @property string $created_at
 */
class Subscription extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            'log' => [
                'class' => 'app\behaviors\LogBehavior',
            ],
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if($this->isNewRecord){
            $this->ip = Yii::$app->request->userIP;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_subscriptions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'email'], 'required'],
            [['updated_at', 'created_at'], 'safe'],
            ['email', 'email'],
            [['name', 'email', 'phone'], 'string', 'max' => 255],
            [['ip'], 'string', 'max' => 55],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ФИО',
            'email' => 'Email',
            'phone' => 'Телефон',
            'ip' => 'Ip',
            'created_at' => 'Дата создания',
        ];
    }

    public function sendMail($model, $mail_to, $mail_from='', $subject='Подписка')
    {
        $mail_from = $mail_from ? $mail_from : 'noreply@'.$_SERVER['SERVER_NAME'];

        Yii::$app->mailer->compose('@modules/subscription/mails/letter', ['model' => $model])
            ->setFrom($mail_from)
            ->setTo($mail_to)
            ->setSubject($subject.' '.$_SERVER['SERVER_NAME'])
            ->send();
    }
}
