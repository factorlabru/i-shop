<?php
namespace app\modules\subscription\widgets;

use yii\base\Widget;
use yii\helpers\Html;

/**
 * Виджет для вывода формы подписки.
 *
 * Class Subscription
 * @package app\modules\subscription\widgets
 */
class Subscription extends Widget
{
    public function run()
    {
        $model = new \app\modules\subscription\models\Subscription();

        return $this->render('subscription_form', ['model'=>$model]);
    }
}