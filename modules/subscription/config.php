<?php

namespace app\modules\subscription;

return [
    'params' => [
        'model'=>'\app\modules\subscription\models\Subscription',
    ],
];