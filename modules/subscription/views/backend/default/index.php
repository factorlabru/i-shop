<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\subscription\models\SubscriptionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Подписчики';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="subscription-index content">

    <div class="box">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a('<i class="fa fa-cloud-download"></i> Выгрузить подписчиков', ['unload'], ['class' => 'btn btn-success']) ?>

            <?=\app\modules\admin\widgets\GridMultiDelete::widget()?>
        </p>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\CheckboxColumn'],
                ['class' => 'yii\grid\SerialColumn'],
                'name',
                'email:email',
                //'phone',
                //'ip',
                [
                    'filter' => DatePicker::widget([
                        'model' => $searchModel,
                        'attribute' => 'created_at',
                        'pluginOptions' => ['format' => 'yyyy-mm-dd']
                    ]),
                    'attribute' => 'created_at',
                    'format' => ['date', 'php:Y-m-d'],
                ],

                ['class' => 'yii\grid\ActionColumn', 'template' => '{delete}{link}'],
            ],
        ]); ?>
    </div>
</div>
