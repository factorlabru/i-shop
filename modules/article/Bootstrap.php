<?php
namespace app\modules\article;
use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        $app->getUrlManager()->addRules(
            [
                '<path:([\w_\/-]+\/)?(articles|stati)>/page/<page:[0-9]+>'=>'article/article/index',
                [
                    'pattern' => '<path:([\w_\/-]+\/)?(articles|stati)>/<url_alias:[-a-zA-Z0-9_]+>',
                    'route' => 'article/article/view',
                    'suffix' => '.html',
                ],
                '<path:([\w_\/-]+\/)?(articles|stati)>' => 'article/article/index',
            ]
        );
    }
}