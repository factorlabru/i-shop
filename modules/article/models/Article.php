<?php

namespace app\modules\article\models;

use app\behaviors\UploadImageBehavior;
use app\modules\section\models\Section;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use Yii;

/**
 * This is the model class for table "tbl_articles".
 *
 * @mixin UploadImageBehavior
 *
 * @property integer $id
 * @property string $name
 * @property string $url_alias
 * @property string $annotation
 * @property string $content
 * @property string $img
 * @property integer $vis
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $h1_tag
 * @property string $updated_at
 * @property string $created_at
 *
 * @property ArticleImages[] $articleImages
 */
class Article extends \yii\db\ActiveRecord
{
    const PAGE_SIZE = 5;

    /** @var  Section */
    public static $_articles_section;

    public $file_loader;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            'slug' => [
                'class' => 'app\behaviors\Slug',
                'in_attribute' => 'name',
                'out_attribute' => 'url_alias',
                'translit' => true
            ],

            'image' => [
                'class' => UploadImageBehavior::className(),
                'attribute' => 'img',
                'scenarios' => ['create', 'update'],
                'extensions' => 'png, jpg, jpeg, gif',
                'path' => '@webroot/content/articles',
                'url' => '@web/content/articles',
                'generate_new_name' => true,
                'thumbs' => [
                    'thumb' => [
                        'width' => 335,
                        'height' => 210,
                        'quality' => 90,
                        'mode' => \Imagine\Image\ManipulatorInterface::THUMBNAIL_OUTBOUND
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_articles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'trim'],
            [['annotation', 'content'], 'string'],
            [['vis'], 'integer'],
            ['file_loader', 'image', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg, gif', 'maxFiles' => 10],
            [['updated_at', 'created_at'], 'safe'],
            [['name', 'url_alias', 'meta_title', 'meta_description', 'meta_keywords', 'h1_tag'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'url_alias' => 'Url Alias',
            'annotation' => 'Аннотация',
            'content' => 'Контент',
            'img' => 'Картинка',
            'file_loader' => 'Фото статьи',
            'vis' => 'Показывать',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keywords' => 'Meta Keywords',
            'h1_tag' => 'H1 Tag',
            'updated_at' => 'Дата обновления',
            'created_at' => 'Дата создания',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(\app\modules\article\models\ArticleImage::className(),
            ['article_id' => 'id'])->orderBy(['priority' => SORT_ASC]);
    }


    /**
     * @return null|Section
     */
    public static function getArticlesSection()
    {
        if (!self::$_articles_section) {
            self::$_articles_section = Section::find()->where([
                'url_alias' => 'articles'
            ])->one();
        }

        return self::$_articles_section;
    }

    public function getUrl()
    {
        $articles_section = self::getArticlesSection();

        $url = \yii\helpers\Url::toRoute([
            '/article/article/view',
            'path' => $articles_section->getUrlPath(),
            'url_alias' => $this->url_alias
        ]);

        return $url;
    }
}
