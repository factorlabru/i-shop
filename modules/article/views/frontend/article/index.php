<?php
use yii\helpers\Html;
use yii\widgets\ListView;
use yii\helpers\Url;
use app\helpers\Common;
?>

<section id="content" class="page-content page-cms page-cms-1">

<h1><?=\app\helpers\Common::pageTitle($section)?></h1>

<?php if($articles) { ?>
    <div class="col-md-8">
        <?php foreach($articles as $item) {?>

            <?php $url = $item->getUrl();?>

            <?php echo Html::beginTag('div');?>

            <h3>
                <a href="<?=$url;?>">
                    <?=$item->name;?>
                </a>
            </h3>
            <div><?=Common::formatDate($item->created_at);?></div>

            <?php if($item->img) {?>
                <div class="image-item">
                    <a href="<?=$url;?>">
                        <img src="<?= $item->img() ?>" alt="">
                    </a>
                </div>
            <?php }?>

            <br>
            <div>
                <?=$item->annotation;?>
            </div>
            <br>

            <div class="entry">

                <p><a href="<?=$item->getUrl()?>#more"
                      class="more-link"><em>Читать далее →</em></a></p>
            </div>
            <div class="clearfix"></div>

            <?php echo Html::endTag('div');?>

        <?php }?>
    </div>
    <div class="col-md-4">
        <?=\app\widgets\Block::widget(['id' => 7]);?>
    </div>
<?php }?>

<nav class="pagination">
    <div>
        <?php
        echo \app\widgets\LinkPager::widget([
            'pagination' => $pages,
            'nextPageLabel'=>'вперед →',
            'prevPageLabel'=>'← назад'
        ]); ?>
    </div>
</nav>
</section>