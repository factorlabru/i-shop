<section id="content" class="page-content page-cms page-cms-1">
    <div class="col-md-8">
        <header class="page-header">
            <h1><?=\app\helpers\Common::pageTitle($model)?></h1>
        </header>

        <?=$model->content?>
    </div>
    <div class="col-md-4">
        <?=\app\widgets\Block::widget(['id' => 7]);?>
    </div>
</section>