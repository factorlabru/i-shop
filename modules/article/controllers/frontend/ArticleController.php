<?php

namespace app\modules\article\controllers\frontend;

use app\modules\article\models\Article;
use app\modules\article\models\ArticleImage;
use app\modules\article\models\ArticleSearch;
use app\components\Controller;
use app\modules\section\models\Section;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;


/**
 * Article controller for the `news` module
 */
class ArticleController extends Controller
{
    public function actionIndex($path)
    {
        $section = Section::getByPath($path) or $this->fail();

        $query = Article::find()->where(['vis'=>1])->orderBy('created_at DESC');
        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(),
            'pageSize'=>Article::PAGE_SIZE, 'defaultPageSize'=>Article::PAGE_SIZE]);

        $articles = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        $this->setMetaByModel($section);
        $this->setBreadcrumbs($section->makeBreadcrumbs());

        return $this->render('index', [
            'articles' => $articles,
            'pages' => $pages,
        ]);
    }

    public function actionView($path, $url_alias)
    {
        $section = Section::getByPath($path) or $this->fail();

        $model = Article::find()->where('url_alias=:url_alias AND vis=1', [':url_alias'=> $url_alias])->one();
        $model or $this->fail();

        $this->current_item = $model->id;

        $this->setMetaByModel($model);
        $this->setBreadcrumbs($section->makeBreadcrumbs(), $model->name);

        return $this->render('view', [
            'model'=>$model,
        ]);
    }
}
