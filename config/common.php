<?php
use yii\helpers\ArrayHelper;

$params = ArrayHelper::merge(
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'basePath' => dirname(__DIR__),
    'vendorPath' => dirname(__DIR__) . '/vendor',
    'bootstrap' => ['log'],
    'language' => 'ru-RU',

    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'charset' => 'utf8',
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'suffix' => '/',
            'rules' => [
                [
                    'class' => 'yii\web\GroupUrlRule',
                    'prefix' => 'admin',
                    'routePrefix' => 'admin',
                    'rules' => [
                        '' => 'default/index',

                        //admin/login, admin/logout, etc.
                        '<_a:(login|logout)>' => 'default/<_a>',

                        '<_m:[\w\-]+>' => '<_m>/default/index',
                        '<_m:[\w\-]+>/<id:\d+>' => '<_m>/default/view',
                        '<_m:[\w\-]+>/<id:\d+>/<_a:[\w-]+>' => '<_m>/default/<_a>',
                        '<_m:[\w\-]+>/<_c:[\w\-]+>/<id:\d+>' => '<_m>/<_c>/view',
                        '<_m:[\w\-]+>/<_c:[\w\-]+>/<id:\d+>/<_a:[\w\-]+>' => '<_m>/<_c>/<_a>',
                        '<_m:[\w\-]+>/<_c:[\w\-]+>' => '<_m>/<_c>/index',
                    ],
                ],
                '' => 'site/index',
                'contact' => 'main/contact/index',

                'testimonials/page/<page:[0-9]+>'=>'testimonials/testimonials/index',
                'testimonials/<url_alias:[\w\-]+>'=>'testimonials/testimonials/view',
                'testimonials' => 'testimonials/testimonial/index',

                /*Blog*/
                'post/<url_alias:[\w\-]+>' => 'blog/post/view',
                'tag/<tag_url:[\w\-]+>' => 'blog/post/tag',
                'category/<category_lvl1:[\w\-]+>/<category_lvl2:[\w\-]+>' => 'blog/post/category',
                'category/<category_lvl1:[\w\-]+>' => 'blog/post/category',
                /*Blog*/

                '<_a:(login)>' => 'admin/default/<_a>',
                '<_a:error>' => 'main/default/<_a>',
                '<_a:(login|logout|signup|email-confirm|password-reset-request|password-reset)>' => 'user/default/<_a>',
                '<_m:[\w\-]+>' => '<_m>/default/index',
                '<_m:[\w\-]+>/<_c:[\w\-]+>' => '<_m>/<_c>/index',
                '<_m:[\w\-]+>/<_c:[\w\-]+>/<_a:[\w-]+>' => '<_m>/<_c>/<_a>',
                '<_m:[\w\-]+>/<_c:[\w\-]+>/<id:\d+>' => '<_m>/<_c>/view',
                '<_m:[\w\-]+>/<_c:[\w\-]+>/<id:\d+>/<_a:[\w\-]+>' => '<_m>/<_c>/<_a>',
            ],
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            //'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            //'useFileTransport' => true,
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'dateFormat' => 'd.M.Y',
            'datetimeFormat' => 'd.MM.Y H:i:s',
            'timeFormat' => 'H:i:s',
        ],
        'cache' => [
            //'class' => 'yii\caching\DummyCache',
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'user' => [
            'identityClass' => 'modules\admin\models\backend\User',
        ],
        'log' => [
            'class' => 'yii\log\Dispatcher',
        ],
    ],
    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\Module',
            'layout' => '@app/modules/admin/views/layouts/main',
            'modules' => [
                'user' => [
                    'class' => 'app\modules\user\Module',
                    'controllerNamespace' => 'app\modules\user\controllers\backend',
                    'viewPath' => '@app/modules/user/views/backend',
                ],
                'block' => [
                    'class' => 'app\modules\block\Module',
                    'controllerNamespace' => 'app\modules\block\controllers\backend',
                    'viewPath' => '@app/modules/block/views/backend',
                ],
                'testimonials' => [
                    'class' => 'app\modules\testimonials\Module',
                    'controllerNamespace' => 'app\modules\testimonials\controllers\backend',
                    'viewPath' => '@app/modules/testimonials/views/backend',
                ],
            ]
        ],
        'main' => [
            'class' => 'app\modules\main\Module',
        ],
        'user' => [
            'class' => 'app\modules\user\Module',
            'controllerNamespace' => 'app\modules\user\controllers\frontend',
            'viewPath' => '@app/modules/user/views/frontend',
        ],
        'block' => [
            'class' => 'app\modules\block\Module',
            'controllerNamespace' => 'app\modules\block\controllers\frontend',
            'viewPath' => '@app/modules/block/views/frontend',
        ],
        'testimonials' => [
            'class' => 'app\modules\testimonials\Module',
            'controllerNamespace' => 'app\modules\testimonials\controllers\frontend',
            'viewPath' => '@app/modules/testimonials/views/frontend',
        ],
    ],
    'params' => $params,
];