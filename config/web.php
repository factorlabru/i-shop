<?php
Yii::setAlias('@admin', '/admin/');
Yii::setAlias('@modules', '/admin/modules');
Yii::setAlias('@login', '/admin/login');
Yii::setAlias('@upload','/web/content/');

Yii::setAlias('@modules', dirname(__DIR__) . '/modules');
Yii::setAlias('@admin_layouts', dirname(__DIR__) . '/modules/admin/views/backend/default');

$params = require(__DIR__ . '/params.php');

$db_config = '/db-local.php';

$current_ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
if(YII_ENV != 'test') {
    if (!in_array($current_ip, ['127.0.0.1', "::1"])) {
        $db_config = '/db.php';
    }
}

//Проверка существования файлов.
if (YII_DEBUG) {
    if (!file_exists(__DIR__ . '/db-local.php')) {
        copy(__DIR__ . '/db.php', __DIR__ . '/db-local.php');
    }

    if (!file_exists($_SERVER['DOCUMENT_ROOT'] . '/web/robots.txt')) {
        $fp = fopen($_SERVER['DOCUMENT_ROOT'] . '/web/robots.txt', "w");
        fwrite($fp, "User-agent: *\nDisallow: /");
        fclose($fp);
    }
}

$modules = [];
$bootstrap = [];
$bootstrap[] = 'log';
$bootstrap[] = 'conf';

$config = [
    'id' => 'CMS',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'language' => 'ru-RU',
    'basePath' => dirname(__DIR__),
    'bootstrap' => $bootstrap,
    'components' => [
        'conf' => ['class' => 'app\components\Config'],
        'view' => ['class' => 'app\components\View'],
        'request' => [
            //убирает web из адреса
            'baseUrl'=> '',
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'lYLAqx86xJRN9lv1z-J-wpcYXTSgaQVN',
        ],
        'user' => [
            'class' => 'app\components\WebUser',
            'identityClass' => 'app\modules\admin\models\backend\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['admin/default/login'],
        ],
        'errorHandler' => [
            'errorAction' => 'main/site/error',
        ],

        'db' => require(__DIR__ . $db_config),

        'urlManager' => [
            //'class' => 'yii\web\UrlManager',
            'class' => 'app\components\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'suffix' => '/',
            'normalizer' => [
                'class' => 'yii\web\UrlNormalizer',
                //'action' => yii\web\UrlNormalizer::ACTION_REDIRECT_TEMPORARY, // use temporary redirection instead of permanent
            ],
            'rules' => [
                [
                    'class' => 'yii\web\GroupUrlRule',
                    'prefix' => 'admin',
                    'routePrefix' => 'admin',
                    'rules' => [
                        '' => 'default/index',
                        '<_a:(login|logout|clearcache|ajax-url-translate)>' => 'default/<_a>',

                        '<_m:[\w\-]+>' => '<_m>/default/index',
                        '<_m:[\w\-]+>/<id:\d+>' => '<_m>/default/view',
                        '<_m:[\w\-]+>/<id:\d+>/<_a:[\w-]+>' => '<_m>/default/<_a>',
                        '<_m:[\w\-]+>/<_c:[\w\-]+>/<id:\d+>' => '<_m>/<_c>/view',
                        '<_m:[\w\-]+>/<_c:[\w\-]+>/<id:\d+>/<_a:[\w\-]+>' => '<_m>/<_c>/<_a>',
                        '<_m:[\w\-]+>/<_c:[\w\-]+>' => '<_m>/<_c>/index',
                    ],
                ],
                'admin/<_m:[\w\-]+>/<_c:[\w\-]+>/<_a:[\w-]+>'=>'admin/<_m>/<_c>/<_a>',

                //Главная страница
                '' => 'main/site/index',
            ],
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
        ],
        'cache' => [
            //'class' => 'yii\caching\DummyCache',
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'class' => 'yii\log\Dispatcher',
            // уведомления на почту, если хотите понять работает ли ваш сайт.
            /*'targets' => [
                [
                    'class' => 'yii\log\EmailTarget',
                    'levels' => ['error', 'warning'],
                    //'categories' => ['yii\db\*'],
                    'message' => [
                        'from' => 'noreply@'.$_SERVER['SERVER_NAME'],
                        'to' => ['devmail@gmail.com'], // адрес разработчика
                        'subject' => 'Лог с сайта: ' . $_SERVER['SERVER_NAME'],
                    ],
                ],
            ],*/
        ],

        'i18n' => [
            'translations' => [
                'app' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'forceTranslation' => true,
                ],
            ],
        ],

        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'assetManager' => [
            'class' => 'yii\web\AssetManager',
            'appendTimestamp' => true,
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js' => [
                        YII_DEBUG ? 'jquery.js' : 'jquery.min.js'
                    ]
                ],
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [
                        YII_DEBUG ? 'css/bootstrap.css' : 'css/bootstrap.min.css',
                    ]
                ],
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'js' => [
                        YII_DEBUG ? 'js/bootstrap.js' : 'js/bootstrap.min.js',
                    ]
                ]
            ],
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'vk' => [
                    'class' => 'yii\authclient\clients\VKontakte',
                    'clientId' => '6159783',
                    'clientSecret' => 'vZ0cgoC7ecQiqfAKtGks',
                ],
            ],
        ],
        'cart' => [
            'class' => 'app\components\Cart'
        ],
    ],
    'params' => $params,
];

if (file_exists(__DIR__ . '/config-local.php')) {
    $config = \yii\helpers\ArrayHelper::merge($config,  require __DIR__ . '/config-local.php');
}

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;