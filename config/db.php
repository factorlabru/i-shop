<?php

return [
    'class' => 'yii\db\Connection',
    //'dsn' => 'mysql:host=localhost;dbname=yii2project_db',
    'dsn' => 'mysql:host=localhost;dbname=webelementshopd_db',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
    'tablePrefix' => 'tbl_',

    /* Кеширование, включить на продакшене */

    /*'enableSchemaCache' => true,
    // Duration of schema cache.
    'schemaCacheDuration' => 3600,
    // Name of the cache component used to store schema information
    'schemaCache' => 'cache',*/

    /* Кеширование, включить на продакшене */
];