<?php
use yii\helpers\ArrayHelper;

Yii::setAlias('@modules', dirname(__DIR__) . '/modules');

//Перебор всех модулей для дальнейшего подключения
$dir = dirname(__DIR__) . '/modules';
$dh  = opendir($dir);
$modules = [];

while (false !== ($moduleName = readdir($dh))) {
    if ($moduleName != '.' && $moduleName != '..' && $moduleName != 'admin') {
        if (is_dir($dir.'/'.$moduleName)){
            if (is_file($dir.'/'.$moduleName.'/config.php')) {
                $modules[$moduleName] = require($dir.'/'.$moduleName.'/config.php');
            }
        }
    }
}

return ['modules' => $modules];