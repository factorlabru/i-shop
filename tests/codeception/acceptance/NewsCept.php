<?php

/* @var $scenario Codeception\Scenario */

$I = new AcceptanceTester($scenario);
$I->wantTo('ensure that news works');
$I->amOnPage('/news');
$I->see('Новости');
$I->seeLink('Эмпирический ряд Тейлора: гипотеза и теории');
$I->click('Эмпирический ряд Тейлора: гипотеза и теории');
$I->see('Математическая статистика упорядочивает');
