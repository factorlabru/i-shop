<?php

/* @var $scenario Codeception\Scenario */

$I = new AcceptanceTester($scenario);
$I->wantTo('ensure that testimonials works');
$I->amOnPage('/testimonials');
$I->see('Отзывы');
$I->seeLink('Добавить отзыв');
$I->click('Добавить отзыв');
$I->see('Оставить отзыв');

$I->amGoingTo('submit testimonial form with no data');
$I->click('Отправить');

if (method_exists($I, 'wait')) {
    $I->wait(3); // only for selenium
}
$I->expectTo('see validations errors');
$I->see('Оставить отзыв', 'h1');
$I->see('Необходимо заполнить «ФИО».');
$I->see('Необходимо заполнить «Email».');
$I->see('Необходимо заполнить «Отзыв».');


$I->amGoingTo('submit testimonial form with correct data');
$I->fillField('Testimonial[name]','Test Name');
$I->fillField('Testimonial[email]','test@test.ru');
$I->fillField('Testimonial[message]','Test Message');
$I->fillField('Testimonial[verifyCode]','testme');

if (method_exists($I, 'wait')) {
    $I->wait(3); // only for selenium
}
$I->click('Отправить');
$I->dontSeeElement('#testimonial-form');
$I->see('Сообщение отправлено');
