<?php

namespace app\behaviors;

use yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\db\BaseActiveRecord;

class PriorityBehavior extends Behavior
{
    public $class;

    public $temp_sortable_val;

    public $order_attribute = 'priority';

    public function events()
    {
        return [
            BaseActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
            BaseActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
            BaseActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
        ];
    }

    public function beforeValidate()
    {
        $model = $this->owner;
        $this->temp_sortable_val = $model->{$this->owner->order_attribute};
    }

    public function beforeSave()
    {
        $model = $this->owner;
        if(!$model->{$this->owner->order_attribute}) {

            //Из-за AdjacencyListBehavior атрибут priority ставится = 0
            //temp_sortable_val исправляет это.
            if($model->isNewRecord && $this->temp_sortable_val) {
                $val = $this->temp_sortable_val;
            } else {
                $val = $model::find()->max($this->order_attribute);

                if(!$val) {
                    $val = 1;
                } else {
                    $val += 1;
                }
            }

            $model->{$this->order_attribute} = $val;
        }
    }
}