<?php
namespace app\behaviors;

use yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;
use yii\db\BaseActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;
use yii\validators\Validator;
use app\helpers\Common;

class UploadBehavior extends Behavior
{
    public $attribute='img';
    private $_file;
    protected $_files_arr=[];
    protected $_files;
    public $instance_by_name = false;
    public $related_model;
    const EVENT_AFTER_UPLOAD = 'afterUpload';
    /**
     * @var boolean If `true` current attribute file will be deleted
     */
    public $unlink_on_save = true;
    /**
     * @var boolean If `true` current attribute file will be deleted after model deletion.
     */
    public $unlink_on_delete = true;

    public $generate_new_name;

    /**
     * @var boolean $delete_temp_file whether to delete the temporary file after saving.
     */
    public $delete_temp_file = true;

    /**
     * @var string the base path or path alias to the directory in which to save files.
     */
    public $path;
    /**
     * @var string the base URL or path alias for this file
     */
    public $url;

    /**
     * @var array the scenarios in which the behavior will be triggered
     */
    public $scenarios = [];

    public $extensions = 'png, jpg, jpeg, gif';

    public function init()
    {
        parent::init();

        if ($this->attribute === null) {
            throw new InvalidConfigException('The "attribute" property must be set.');
        }
    }

    public function events()
    {
        return [
            BaseActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
            BaseActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
            BaseActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
            BaseActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            BaseActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
            BaseActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete',
        ];
    }

    /**
     * This method is invoked before validation starts.
     */
    public function beforeValidate()
    {
        /** @var BaseActiveRecord $model */
        $model = $this->owner;

        /*
         * ругаемся, если кто-то пытается загрузить файл, но передал неправильный сценарий
         */
        if (!in_array($model->scenario, $this->scenarios)) {
            $file = $model->getAttribute($this->attribute);
            if (!($file instanceof UploadedFile)) {
                if ($this->instance_by_name === true) {
                    $file = UploadedFile::getInstanceByName($this->attribute);
                } else {
                    $file = UploadedFile::getInstance($model, $this->attribute);
                }
            }

            if ($file instanceof UploadedFile) {
                $message = "Wrong scenario '{$model->scenario}', for ".get_class($model)."! (" . implode(', ', $this->scenarios) . ") needed.";
                throw new \RuntimeException($message);
            }
        }

        if (in_array($model->scenario, $this->scenarios)) {
            if (($file = $model->getAttribute($this->attribute)) instanceof UploadedFile) {
                $this->_file = $file;
            } else {
                if ($this->instance_by_name === true) {
                    $this->_file = UploadedFile::getInstanceByName($this->attribute);
                } else {
                    $this->_file = UploadedFile::getInstance($model, $this->attribute);

                    if(!$this->_file) {
                        $this->_files_arr = UploadedFile::getInstances($model, $this->attribute);
                    }
                }
            }

            if ($this->_file instanceof UploadedFile) {
                $this->_file->name = $this->getFileName($this->_file);
                $model->setAttribute($this->attribute, $this->_file);
            }

            if($this->_files_arr) {
                $model->{$this->attribute} = $this->_files_arr;
            }
        }
    }

    public function beforeSave()
    {
        $model = $this->owner;
        if (in_array($model->scenario, $this->scenarios)) {

            if ($this->_file instanceof UploadedFile) {
                if (!$model->getIsNewRecord() && $model->isAttributeChanged($this->attribute)) {
                    if ($this->unlink_on_save === true) {
                        $this->delete($this->attribute, true);
                    }
                }
                $model->setAttribute($this->attribute, $this->_file->name);
            } else {
                // Protect attribute
                unset($model->{$this->attribute});
            }
        } else {
            if (!$model->getIsNewRecord() && $model->isAttributeChanged($this->attribute)) {
                if ($this->unlink_on_save === true) {
                    $this->delete($this->attribute, true);
                }
            }
        }
    }

    /**
     * This method is called at the end of inserting or updating a record.
     * @throws \yii\base\InvalidParamException
     */
    public function afterSave()
    {
        if ($this->_file instanceof UploadedFile) {
            $path = $this->getUploadPath($this->attribute);
            if (is_string($path) && FileHelper::createDirectory(dirname($path))) {
                $this->save($this->_file, $path);
                $this->afterUpload();
            } else {
                throw new InvalidParamException("Directory specified in 'path' attribute doesn't exist or cannot be created.");
            }
        } elseif($this->_files_arr) {

            $path = $this->resolvePath($this->path);

            $related_model = $this->related_model;
            $max_val = $this->getMaxValue($related_model);

            $new_names = [];
            foreach($this->_files_arr as $file) {
                $file_name = $this->getFileName($file);

                if($this->related_model) {
                    $model = new $related_model['model_name'];
                    $model->{$related_model['related_attribute']} = $this->owner->id;
                    $model->{$related_model['attribute']} = $file_name;

                    if(is_numeric($max_val)) {
                        $model->priority =$max_val;
                        $max_val++;
                    }
                    $model->save();
                }

                $new_names[] = $file_name;
                $this->save($file, Yii::getAlias($path."/".$file_name));
            }

            $this->_files_arr = $new_names;

            $this->afterUpload();
        }
    }

    /**
     * This method is invoked before deleting a record.
     */
    public function beforeDelete()
    {
        $attribute = $this->attribute;
        if ($this->unlink_on_delete && $attribute) {
            $this->delete($attribute);
        }
    }

    /**
     * Returns file path for the attribute.
     * @param string $attribute
     * @param boolean $old
     * @return string|null the file path.
     */
    public function getUploadPath($attribute, $old = false)
    {
        /** @var BaseActiveRecord $model */
        $model = $this->owner;
        $path = $this->resolvePath($this->path);
        $fileName = ($old === true) ? $model->getOldAttribute($attribute) : $model->$attribute;
        return $fileName ? Yii::getAlias($path . '/' . $fileName) : null;
    }

    /**
     * Returns file url for the attribute.
     * @param string $attribute
     * @return string|null
     */
    public function getUploadUrl($attribute)
    {
        /** @var BaseActiveRecord $model */
        $model = $this->owner;
        $url = $this->resolvePath($this->url);
        $fileName = $model->getOldAttribute($attribute);
        return $fileName ? Yii::getAlias($url . '/' . $fileName) : null;
    }

    /**
     * Replaces all placeholders in path variable with corresponding values.
     */
    protected function resolvePath($path)
    {
        /** @var BaseActiveRecord $model */
        $model = $this->owner;
        return preg_replace_callback('/{([^}]+)}/', function ($matches) use ($model) {
            $name = $matches[1];
            $attribute = ArrayHelper::getValue($model, $name);
            if (is_string($attribute) || is_numeric($attribute)) {
                return $attribute;
            } else {
                return $matches[0];
            }
        }, $path);
    }

    /**
     * Saves the uploaded file.
     * @param UploadedFile $file the uploaded file instance
     * @param string $path the file path used to save the uploaded file
     * @return boolean true whether the file is saved successfully
     */
    protected function save($file, $path)
    {
        return $file->saveAs($path, $this->delete_temp_file);
    }

    /**
     * Deletes old file.
     * @param string $attribute
     * @param boolean $old
     */
    protected function delete($attribute, $old = false)
    {
        $path = $this->getUploadPath($attribute, $old);
        if (is_file($path)) {
            unlink($path);
        }
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    protected function getFileName($file)
    {
        if ($this->generate_new_name) {

            return $this->generateFileName($file);
        } else {
            return $this->sanitize($file->name);
        }
    }

    /**
     * Generates random filename.
     * @param UploadedFile $file
     * @return string
     */
    protected function generateFileName($file)
    {
        return md5($file->name.time()) . '.' . $file->extension;
    }

    /**
     * Replaces characters in strings that are illegal/unsafe for filename.
     *
     * #my*  unsaf<e>&file:name?".png
     *
     * @param string $filename the source filename to be "sanitized"
     * @return boolean string the sanitized filename
     */
    public static function sanitize($filename)
    {
        return str_replace([' ', '"', '%', '\'', '&', '/', '\\', '?', '#'], '-', $filename);
    }

    protected function afterUpload()
    {
        $this->owner->trigger(self::EVENT_AFTER_UPLOAD);
    }

    protected function getMaxValue($related_model)
    {
        if(isset($related_model['priority'])) {
            $max_val = $related_model['model_name']::find()->where("{$related_model['attribute']}={$this->owner->id}")->select('priority')->max('priority');
            $max_val = $max_val ? $max_val : 1;

            return $max_val;
        }

        return false;
    }

    public function getPath()
    {
        return Yii::getAlias($this->path);
    }
}