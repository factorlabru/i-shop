<?php

namespace app\behaviors;

use Yii;
use yii\db\ActiveRecord;
use yii\db\BaseActiveRecord;
use yii\web\UploadedFile;

class UploadManyFilesBehavior extends \yii\base\Behavior
{

    public $attribute = 'file_loader';
    public $behavior_name = 'file';
    public $relation = 'files';

    // список сценариев, по которым файлы будут загружаться
    public $scenarios = ['default'];

    // сценарий, по которому будут создаваться файлы
    public $related_scenario = 'default';

    public function events()
    {
        return [
            BaseActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
            BaseActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate',
            BaseActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
            BaseActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
        ];
    }

    /**
     * This method is invoked before validation starts.
     */
    public function beforeValidate()
    {
        /** @var BaseActiveRecord $model */
        $model = $this->owner;
        $attr = $this->attribute;
        $model->$attr = UploadedFile::getInstances($model, $this->attribute);
    }

    public function afterDelete()
    {
        $model = $this->owner;

        /** @var ActiveRecord $related_model */
        foreach($model->{$this->relation} as $related_model){
            $related_model->delete();
        }
    }

    public function afterInsert()
    {
        $this->processFiles();
    }

    public function afterUpdate()
    {
        $this->processFiles();
    }

    public function processFiles()
    {
        /** @var ActiveRecord $model */
        $model = $this->owner;

        // получаем параметры из указанной связи
        $related_model = $this->getRelatedModel();
        $parent_attribute = $this->getParentAttribute();
        $related_model_name = $this->getRelatedModelName();

        // получаем параметры из поведения
        $related_behavior = $related_model->getBehavior($this->behavior_name);
        $attribute = $related_behavior->attribute;

        $files = UploadedFile::getInstances($model, $this->attribute);

        // если файлы передаются, а сценарий неправильный - ругаемся
        if(!in_array($model->scenario, $this->scenarios)){
            if(!empty($files)){
                $message = "Wrong scenario '{$model->scenario}', for ".get_class($model)."! (" . implode(', ', $this->scenarios) . ") needed.";
                throw new \RuntimeException($message);
            }
        }

        if (in_array($model->scenario, $this->scenarios)) {
            if ($files) {
                $max_val = null;
                if($related_model->hasAttribute('priority')){
                    $max_val = $related_model::find()->where([
                        $parent_attribute => $model->id
                    ])->max('priority');
                }

                foreach($files as $file){
                    /** @var ActiveRecord $new_model */
                    $new_model = (new $related_model_name);
                    $new_model->scenario = $this->related_scenario;
                    $new_model->$parent_attribute = $this->owner->id;
                    $new_model->{$attribute} = $file;

                    if(is_numeric($max_val)) {
                        $max_val++;
                        $new_model->priority = $max_val;
                    }
                    if(!$new_model->save()){
                        $errors = implode(', ', $new_model->getFirstErrors());
                        throw new \RuntimeException('Saving error. ' . $errors);
                    }
                }
            }
        }
    }

    /**
     * @return ActiveRecord|mixed
     */
    protected function getRelatedModel()
    {
        $related_model_name = $this->getRelatedModelName();
        /** @var ActiveRecord $related_model */
        $related_model = (new $related_model_name);
        return $related_model;
    }

    /**
     * @return string
     */
    protected function getParentAttribute()
    {
        $link = $this->getRelationLink();
        $parent_attribute = collect($link->link)->keys()->first();
        return $parent_attribute;
    }

    /**
     * @return mixed
     */
    protected function getRelationLink()
    {
        $model = $this->owner;
        $relation_name = 'get' . ucfirst($this->relation);
        $link = $model->$relation_name();
        return $link;
    }

    /**
     * @return mixed
     */
    protected function getRelatedModelName()
    {
        $link = $this->getRelationLink();
        $related_model_name = $link->modelClass;
        return $related_model_name;
    }


}