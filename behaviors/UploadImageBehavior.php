<?php
namespace app\behaviors;

use Imagine\Image\ManipulatorInterface;
use RuntimeException;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;
use yii\db\BaseActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\imagine\Image;
use yii\validators\Validator;
use yii\web\UploadedFile;

class UploadImageBehavior extends UploadBehavior
{
    /**
     * @var string
     */
    public $placeholder;
    /**
     * @var boolean
     */
    public $createThumbsOnSave = true;
    /**
     * @var boolean
     */
    public $createThumbsOnRequest = false;
    /**
     * @var array the thumbnail profiles
     * - `width`
     * - `height`
     * - `quality`
     */
    public $thumbs = [
        'thumb' => ['width' => 200, 'height' => 200, 'quality' => 90],
    ];
    /**
     * @var string|null
     */
    public $thumbPath;
    /**
     * @var string|null
     */
    public $thumbUrl;

    /**
     * @var string
     */
    public $extensions = 'png, jpg, jpeg, gif';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        if ($this->createThumbsOnSave) {
            if ($this->thumbPath === null) {
                $this->thumbPath = $this->path;
            }
            if ($this->thumbUrl === null) {
                $this->thumbUrl = $this->url;
            }
            foreach ($this->thumbs as $config) {
                $width = ArrayHelper::getValue($config, 'width');
                $height = ArrayHelper::getValue($config, 'height');
                if ($height < 1 && $width < 1) {
                    throw new InvalidConfigException(sprintf(
                        'Length of either side of thumb cannot be 0 or negative, current size ' .
                        'is %sx%s', $width, $height
                    ));
                }
            }
        }
    }

    public function attach($owner)
    {
        /** @var BaseActiveRecord $owner */
        parent::attach($owner);
        $validator = Validator::createValidator('image', $owner, [$this->attribute], [
            'extensions' => $this->extensions,
            'on' => $this->scenarios,
        ]);
        $owner->validators[] = $validator;
    }

    public function img($profile = 'thumb')
    {
        return $this->getThumbUploadUrl($this->attribute, $profile);
    }

    /**
     * @inheritdoc
     */
    protected function afterUpload()
    {
        parent::afterUpload();

        if ($this->createThumbsOnSave) {
            $this->_files_arr ? $this->createMultiThumbs() : $this->createThumbs();
        }
    }

    /**
     * @throws \yii\base\InvalidParamException
     */
    protected function createThumbs()
    {
        $path = $this->getUploadPath($this->attribute);
        foreach ($this->thumbs as $profile => $config) {
            $thumbPath = $this->getThumbUploadPath($this->attribute, $profile);
            if ($thumbPath !== null) {
                if (!FileHelper::createDirectory(dirname($thumbPath))) {
                    throw new InvalidParamException("Directory specified in 'thumbPath' attribute doesn't exist or cannot be created.");
                }
                if (!is_file($thumbPath)) {
                    $this->generateImageThumb($config, $path, $thumbPath);
                }
            }
        }
    }

    protected function createMultiThumbs()
    {
        $path = $this->resolvePath($this->path);

        foreach($this->_files_arr as $file) {
            $file_name = $file;
            $file_path = Yii::getAlias($path.'/'.$file_name);

            foreach($this->thumbs as $profile => $config) {
                $thumb_path = $this->getThumbUploadPath($file_name, $profile, $old=false, $multiupload=true);
                $this->generateImageThumb($config, $file_path, $thumb_path);
            }
        }
    }

    /**
     * @param string $attribute
     * @param string $profile
     * @param boolean $old
     * @param boolean $multiupload
     * @return string
     */
    public function getThumbUploadPath($attribute, $profile = 'thumb', $old = false)
    {
        /** @var BaseActiveRecord $model */
        $model = $this->owner;
        $path = $this->resolvePath($this->thumbPath);
        $attribute = ($old === true) ? $model->getOldAttribute($attribute) : $model->$attribute;
        $filename = $this->getThumbFileName($attribute, $profile);

        return $filename ? Yii::getAlias($path . '/' . $filename) : null;
    }

    /**
     * @param string $attribute
     * @param string $profile
     * @return string|null
     */
    public function getThumbUploadUrl($attribute, $profile = 'thumb')
    {
        /** @var BaseActiveRecord $model */
        $model = $this->owner;
        $path = $this->getUploadPath($attribute, true);
        if (is_file($path)) {
            if ($this->createThumbsOnRequest) {
                $this->createThumbs();
            }
            $url = $this->resolvePath($this->thumbUrl);
            $fileName = $model->getOldAttribute($attribute);
            $thumbName = $this->getThumbFileName($fileName, $profile);
            return Yii::getAlias($url . '/' . $thumbName);
        } elseif ($this->placeholder) {
            return $this->getPlaceholderUrl($profile);
        } else {
            return null;
        }
    }

    /**
     * @param $filename
     * @param string $profile
     * @return string
     */
    protected function getThumbFileName($filename, $profile = 'thumb')
    {
        $profile = $profile ? $profile . '_' : '';
        return  $profile . $filename;
    }

    /**
     * @inheritdoc
     */
    protected function delete($attribute, $old = false)
    {
        parent::delete($attribute, $old);
        $profiles = array_keys($this->thumbs);
        foreach ($profiles as $profile) {
            $path = $this->getThumbUploadPath($attribute, $profile, $old);
            if (is_file($path)) {
                unlink($path);
            }
        }
    }

    /**
     * @param $config
     * @param $path
     * @param $thumbPath
     */
    protected function generateImageThumb($config, $path, $thumbPath)
    {
        $width = ArrayHelper::getValue($config, 'width');
        $height = ArrayHelper::getValue($config, 'height');
        $quality = ArrayHelper::getValue($config, 'quality', 100);
        $mode = ArrayHelper::getValue($config, 'mode', ManipulatorInterface::THUMBNAIL_INSET);

        if (!$width || !$height) {
            $image = Image::getImagine()->open($path);
            $ratio = $image->getSize()->getWidth() / $image->getSize()->getHeight();
            if ($width) {
                $height = ceil($width / $ratio);
            } else {
                $width = ceil($height * $ratio);
            }
        }
        // Fix error "PHP GD Allowed memory size exhausted".
        ini_set('memory_limit', '512M');
        Image::thumbnail($path, $width, $height, $mode)->save($thumbPath, ['quality' => $quality]);
    }

    public function getThumbs()
    {
        return $this->thumbs;
    }

    /**
     *
     * Возвращаяет адрес первой картинки.
     * Метод нужен для работы со связанными моделями.
     *
     * @param string $relation_attribute
     * @param string $profile
     * @return string - возвращается путь вида /content/folder/filename.jpg
     */
    public function getMainImg($relation_attribute = 'images', $profile = 'thumb')
    {
        $model = $this->owner;
        if(isset($model->{$relation_attribute}[0])) {
            $filename = $this->getThumbFileName($model->{$relation_attribute}[0]->img, $profile);

            return Yii::getAlias($this->url.'/'.$filename);
        }

        return false;
    }

    /**
     * Получение массива картинок.
     *
     * @param string $relation_attribute
     * @param string $model_attribute
     * @param string $profile
     * @return bool
     */
    public function getImagesArr($relation_attribute = 'images', $model_attribute='img', $profile = 'thumb')
    {
        $model = $this->owner;
        if(isset($model->{$relation_attribute})) {
            $arr = [];
            foreach($model->{$relation_attribute} as $item) {
                $filename = $this->getThumbFileName($item->{$model_attribute}, $profile);
                $arr[$item->id] = Yii::getAlias($this->url.'/'.$filename);
            }

            return $arr;
        }

        return false;
    }

    public function deleteFile($filename)
    {
        $profiles = array_keys($this->thumbs);
        array_push($profiles, '');

        foreach ($profiles as $profile) {
            $path = Yii::getAlias($this->path.'/'.$this->getThumbFileName($filename, $profile));

            if (is_file($path)) {
                unlink($path);
            }
        }
    }

    public function getImagePath()
    {
        return Yii::getAlias($this->url).'/';
    }
}