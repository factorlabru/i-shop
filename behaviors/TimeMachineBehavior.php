<?php
namespace app\behaviors;

use app\modules\section\models\Section;
use yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\db\BaseActiveRecord;

/**
 * Поведение для вдения истории записей (разделов, постов, etc.).
 *
 * Class TimeMachineBehavior
 * @package app\behaviors
 */
class TimeMachineBehavior extends Behavior
{
    public $history_model;

    public function events()
    {
        return [
            BaseActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
            BaseActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
            BaseActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
            //BaseActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
        ];
    }

    public function beforeValidate()
    {
/*        $model = $this->owner;
        $history_model = $this->history_model;

        $count = $history_model::find()
            ->where(['model_id' => $this->owner])
            ->count();

        $delete = $history_model::find()
            ->orderBy('updated_at DESC')
            ->one()
            ->delete();

        print_r($count);*/
    }

    public function beforeSave()
    {
        $model = $this->owner;
        $history_model = $this->history_model;

        $count = $history_model::find()
            ->where(['model_id' => $this->owner])
            ->count();

        //удаялем самую старую запись
        if($count > 2) {
            $delete_record = $history_model::find()
                ->orderBy('updated_at ASC')
                ->one()
                ->delete();
        }

        $history_model->setAttributes($model->attributes);
        $history_model->model_id = $this->owner->id;
        $history_model->model_name = $this->owner->className();
        $history_model->save();
    }
}