<?php

namespace app\behaviors;

use yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;

class SaveButtons extends Behavior
{
    public function redirectPath($param='')
    {

        if(Yii::$app->request->post()) {

            if(Yii::$app->request->post('save_close')) {
                return '/admin/'.Yii::$app->controller->module->id.'/'.$param;
            } else {
                return ['update', 'id' => $this->owner->id];
            }
        }
    }
}