<?php
namespace app\behaviors;

use Yii;
use yii\base\NotSupportedException;
use yii\caching\TagDependency;
use yii\helpers\ArrayHelper;

class AdjacencyListBehavior extends \paulzi\adjacencyList\AdjacencyListBehavior
{
    public $alias_attribute = 'url_alias';

    public function getChildrenAll()
    {
        return $this->owner->getDb()->cache(function() {
            return $this->getChildren()->all();
        }, null, $this->cacheDependency());
    }

    public function getParentsAll($depth = null)
    {
        return $this->owner->getDb()->cache(function() use ($depth) {
            return $this->getParents($depth = null);
        }, null, $this->cacheDependency());
    }

    public function getParentsOrderedAll()
    {
        $key = [
            'parents_ordered',
            $this->owner->className(),
            $this->owner->id,
        ];
        return Yii::$app->cache->getOrSet($key, function() {
            return $this->getParentsOrdered();
        }, null, $this->cacheDependency());
    }

    public function makeBreadcrumbs($with_last = false)
    {
        $breadcrumbs = array_map(function ($item) {
            return [
                'label' => $item->name,
                'url' => $item->getUrl(),
            ];
        }, array_merge($this->owner->getParentsOrderedAll(), [$this->owner]));
        if(!$with_last){
            $last_item = array_pop($breadcrumbs);
            $breadcrumbs[] = $last_item['label'];
        }
        return $breadcrumbs;
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws Exception
     */
    public function getParent()
    {
        $result = $this->owner->getDb()->cache(function ($db){
            return $this->owner->hasOne($this->owner->className(), [$this->getPrimaryKey() => $this->parentAttribute]);
        }, null, $this->cacheDependency());
        return $result;
    }

    /**
     * Формирует путь из родительских категорий + текущий урл.
     *
     * @return string
     */
    public function getUrlPath()
    {
        $arr = ArrayHelper::getColumn($this->owner->getParentsOrderedAll(), 'url_alias');
        $arr[] = $this->owner->url_alias;
        return implode('/', $arr);
    }

    /**
     * Найти запись по пути.
     *
     * @param $path
     * @param bool $strict
     * @return array|null|\yii\db\ActiveRecord
     */
    public function findByPath($path, $strict = true)
    {
        $path = trim($path, '/');
        $pieces = explode('/', $path);
        $model = $this->owner->find()->where([
            $this->alias_attribute => end($pieces)
        ])->one();
        if(!$model){
            return null;
        }
        if($strict && $model->getUrlPath() != $path){
            return null;
        }
        return $model;
    }

    protected function getChildByAlias($alias, $parent_id)
    {
        $model = $this->owner->className();
        $result =  $model::find()->where([
            $this->alias_attribute=>$alias,
            'parent_id'=>$parent_id,
        ])->one();
        return $result ? $result : null;
    }

    protected function cacheDependency()
    {
        return new TagDependency([
            'tags' => $this->owner->className(),
        ]);
    }

    public function refreshCache()
    {
        TagDependency::invalidate(Yii::$app->cache, [
            $this->owner->className(),
        ]);
        //прогреваем кеш
        $this->owner->getParentsOrderedAll();
    }


    public function afterSave()
    {
        $this->operation = null;
        $this->node      = null;
        $this->refreshCache();
    }
    public function beforeSave()
    {
        //Проверка, на родительский элемент.
        $exclude_ids = $this->getDescendantsIds(null, true);
        $exclude_ids[] = $this->owner->id;
        if($this->owner->parent_id && in_array($this->owner->parent_id, $exclude_ids)) {
            throw new NotSupportedException('Этот раздел нельзя выбрать в качестве родительского');
        }
        if ($this->node !== null && !$this->node->getIsNewRecord()) {
            $this->node->refresh();
        }
        switch ($this->operation) {
            case self::OPERATION_MAKE_ROOT:
                $this->owner->setAttribute($this->parentAttribute, null);
                if ($this->sortable !== false) {
                    $this->owner->setAttribute($this->behavior->sortAttribute, 0);
                }
                break;
            case self::OPERATION_PREPEND_TO:
                $this->insertIntoInternal(false);
                break;
            case self::OPERATION_APPEND_TO:
                $this->insertIntoInternal(true);
                break;
            case self::OPERATION_INSERT_BEFORE:
                $this->insertNearInternal(false);
                break;
            case self::OPERATION_INSERT_AFTER:
                $this->insertNearInternal(true);
                break;
            default:
                if ($this->owner->getIsNewRecord()) {
                    //throw new NotSupportedException('Method "' . $this->owner->className() . '::insert" is not supported for inserting new nodes.');
                }
        }
    }
    /**
     * Получиь ссылку на страницу (используется в админки в разделах).
     *
     * @return string
     */
    public function getPathLink($prefix='', $postfix='')
    {
        return '<a title="Смотреть на сайте" href="/'.$prefix.$this->getUrlPath().$postfix.'" target="_blank"><i class="glyphicon glyphicon-globe"></i></a>';
    }
}
